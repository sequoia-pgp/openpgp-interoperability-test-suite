#+TITLE: Interop Testing v6
#+AUTHOR: Justus Winter <justus@sequoia-pgp.org>
#+DATE: IETF 118, 2023-11-09
#+OPTIONS: H:1 toc:nil num:t
#+OPTIONS: tex:t
#+startup: beamer
#+LATEX_CLASS: beamer
#+LATEX_CLASS_OPTIONS: [presentation, aspectratio=169]
#+LATEX_HEADER: \usepackage[normalem]{ulem}
#+LATEX_HEADER: \usepackage{pifont}
#+LATEX_HEADER: \usepackage[export]{adjustbox}
#+LATEX_HEADER: \newcommand{\cmark}{\ding{51}}
#+LATEX_HEADER: \newcommand{\xmark}{\ding{55}}
#+LaTeX_HEADER: \hypersetup{linktoc = all, colorlinks = true}
#+BEAMER_HEADER: \titlegraphic{\url{https://tests.sequoia-pgp.org}\\ \vspace{2mm} \url{https://tests.sequoia-pgp.org/v6.html}\\ \vspace{2mm} \url{https://sequoia-pgp.org/talks/2023-11-ietf/interop-testing-v6.pdf}}
#+BEAMER_THEME: Madrid
#+COLUMNS: %45ITEM %10BEAMER_ENV(Env) %10BEAMER_ACT(Act) %4BEAMER_COL(Col) %8BEAMER_OPT(Opt)

* Testing the Crypto Refresh!

  - interop testing OpenPGP since 2019
  - circa 131 tests
  - around 1510 test vectors
  - found loads of bugs across many different implementations
  - includes some v6 tests
    - test vectors from the draft
    - asks implementations to generate v6 keys
  - https://tests.sequoia-pgp.org
    - runs all tests
    - enter "v6" in the search bar
  - https://tests.sequoia-pgp.org/v6.html
    - happy to test your preview releases
    - runs only v6 tests

* Results around IETF116

\center\includegraphics[width=.5\linewidth]{./bleak.png}

* Results now around IETF118

\center\includegraphics[width=.6\linewidth]{./nais.png}

* What can you do?

  - suggest tests, write tests, review tests
  - help with test suite development and maintenance
  - implementers:

    - consider implementing your own SOP frontend
      - if you do, clue me how to build it and keep it up-to-date
      - preferably, get it into Debian
    - do you have a development version? a v6 branch?
    - implement SOP's `--profile`
  - get in touch
    https://gitlab.com/sequoia-pgp/openpgp-interoperability-test-suite
