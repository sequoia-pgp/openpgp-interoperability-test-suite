# OpenPGP interoperability test suite

This is a test suite designed to chart and improve interoperability of
OpenPGP implementations.  It uses the [Stateless OpenPGP Command Line
Interface] (*SOP* for short) that can be implemented by all OpenPGP
implementations.

  [Stateless OpenPGP Command Line Interface]: https://datatracker.ietf.org/doc/draft-dkg-openpgp-stateless-cli/

The result of running the test suite can be seen here:
https://tests.sequoia-pgp.org/

The result of running the test suite running in CI can be seen here
(note that currently, there are still some implementations missing in
these results):
https://sequoia-pgp.gitlab.io/openpgp-interoperability-test-suite/

# How to run the test suite

## With Docker

The test suite can be run using Docker.  This has the advantage of
being able to easily use all the SOP implementations that we test, as
this is the method that we use in CI to produce our test results.  It
has the disadvantage of having a slightly higher latency when hacking
on the test suite itself.  For that, consider using the method
described below to run the test suite on your development machine.

First, you need to build a container image with all the SOP
implementations.  This container description uses multi-stage builds,
so that adding new SOP implementations or updating existing ones will
only require (re)building the added or changed implementations.

```sh
$ docker build -f Dockerfile.sops -t sops
```

To force a full rebuild of the SOP implementations, the cache can be
disabled:

```sh
$ docker build -f Dockerfile.sops -t sops --no-cache true
```

Next, you need to build the test suite.  This will incorporate the
container image containing the SOP implementations to be tested.
Therefore, you need to redo this step both when you changed the SOPs
image and when you changed the test suite itself:

```sh
$ docker build --tag test-suite .
```

Finally, you can run the test suite.  In order to exfiltrate the
results from the container, you need to bind mount a directory from
the host for the test suite to write the results into:

```sh
$ mkdir results
$ docker run \
  --mount type=bind,source=\$(pwd)/results,destination=/test-suite/output \
  test-suite
```

## On the host

The test suite can be run on your development machine.  In contrast to
the Docker method described above, this has a lower latency when
hacking on the test suite itself.  The downside is that you will have
to supply all the implementations that you want to test yourself.
However, when developing new tests, it is advantageous to use a
smaller set of SOP implementations, as this will make the test suite
run more quickly.

To run the test suite, you first need to install some build
dependencies.  On Debian-derived systems, this can be done using:

    $ sudo apt install git rustc cargo clang llvm pkg-config nettle-dev

Next, you need some implementations to test.  For starters, you can
install Sequoia's SOP frontend:

    $ sudo apt install sqop

Finally, you can clone the repository, copy the stock configuration
file (which points to `/usr/bin/sop` as the sole implementation), and
run the test suite:

    $ git clone https://gitlab.com/sequoia-pgp/openpgp-interoperability-test-suite
    $ cd openpgp-interoperability-test-suite
    $ cp config.json.dist config.json
    $ cargo run -- --html-out results.html

## Running a subset of the tests

To run a subset of tests, use either `--retain-tests` which matches on
title and description using regular expressions, or `--retain-tag`
which selects only those tests with a given tag:

    $ cargo run -- --html-out sha1.html --retain-tests sha1
    $ cargo run -- --html-out verify-only.html --retain-tag verify-only

# Configuration

The backends are configured using a JSON file.  It contains a list of
drivers.  Every driver needs a `path` that points to the SOP
executable, and may have an `env` map to set environment variables. A driver may have an optional `id`, which will be used to refer to it in the test outputs (if not set the output of `sop version` is used).
Additionally, you can configure process limits (see `man 2
setrlimit`).  You should at least limit the size of the processes'
data segments to avoid trashing when runaway allocations occur in an
implementation.

This is an example configuration that showcases all the fields:

```json
{
  "drivers": [
    {
      "path": "/usr/bin/sqop"
    },
    {
      "path": "glue/sopgpy",
      "env": { "PYTHONPATH": "..." }
    },
    {
      "id": "GoSOP",
      "path": "/usr/bin/gosop"
    },
    {
      "path": "glue/docker-sop",
      "env": { "IMAGE": "arch-sqop" },
      "id": "sqop from Arch Linux"
    }
  ],
  "rlimits": {
    "DATA": 2147483648
  }
}
```

# Adding a backend

To add an backend, your implementation must implement the [Stateless
OpenPGP Command Line Interface].  The glue code will be called with a
series of arguments, and it is expected to signal success by exiting
with status zero, and failure using any non-zero status code.  Any
artifact produced must be written to stdout, error and diagnostic
messages must be written to stderr, and input is fed to stdin.

If you have written a SOP frontend for your implementation and would
like to include it in https://tests.sequoia-pgp.org/, please [open an
issue].

[open an issue]: https://gitlab.com/sequoia-pgp/openpgp-interoperability-test-suite/-/issues

# Adding a test

If you have an idea for a test, please [open an issue].  You can also
take a stab at implementing a test, of course.  The tests are in the
`src/tests` directory.

# Hall of Fame

The OpenPGP interoperability test suite found the following bugs:

## Sequoia

  - sq: Impossible to generate key with multiple
    userids: https://gitlab.com/sequoia-pgp/sequoia/issues/347
  - attempt to subtract with overflow if signature is larger than
    expected: https://gitlab.com/sequoia-pgp/sequoia/issues/348
  - Blowfish encryption is
    broken: https://gitlab.com/sequoia-pgp/sequoia/issues/350
  - Marker packet not ignored in certificate
    parser: https://gitlab.com/sequoia-pgp/sequoia/issues/372
  - Expiration of primary key binding signatures is not
    honored: https://gitlab.com/sequoia-pgp/sequoia/-/issues/539
  - Sequoia does not honor certificate expiration on primary userid
    when iterating over
    subkeys: https://gitlab.com/sequoia-pgp/sequoia/-/issues/564
  - Sequoia does not correctly consume excess bytes in a container
    packet: https://gitlab.com/sequoia-pgp/sequoia/-/issues/675
  - Regression: Marker packets should be ignored when verifying
    detached
    signatures: https://gitlab.com/sequoia-pgp/sequoia/-/issues/686
  - Sequoia refuses to decrypt ESK-less SKESK4 using
    S2K::Simple: https://gitlab.com/sequoia-pgp/sequoia/-/issues/796
  - stream::Signer does not properly bracket OPS/Sig packets when using
    more than one
    signer: https://gitlab.com/sequoia-pgp/sequoia/-/issues/816

## DKGPG

  - Lowercase hex digits are not recognized as
    such: https://savannah.nongnu.org/bugs/index.php?57097
  - Keyrings must be
    armored: https://savannah.nongnu.org/bugs/index.php?57098
  - dkg-encrypt adds spurious
    newline: https://savannah.nongnu.org/bugs/index.php?57099
  - Decryption fails if the size of the session key is not 256
    bits: https://savannah.nongnu.org/bugs/index.php?57108
  - Generating a key with multiple userids silently ignores all but
    first userid: https://savannah.nongnu.org/bugs/index.php?57111
  - dkg-encrypt prefers OCB over the recipients preference for
    EAX: https://savannah.nongnu.org/bugs/index.php?57131
  - dkg-sign adds spurious newline to armored
    output: https://savannah.nongnu.org/bugs/index.php?57134
  - dkg-verify fails to verify signatures with leading
    zeros: https://savannah.nongnu.org/bugs/index.php?57135
  - Verification fails with "key was not intended for signing" though
    it is: https://savannah.nongnu.org/bugs/index.php?58776
  - dkgpg excludes trailing whitespace from hashing when verifying text
    signatures: https://savannah.nongnu.org/bugs/index.php?58887
  - Unknown notations with the "critical" bit set SHOULD invalidate
    the signature: https://savannah.nongnu.org/bugs/index.php?58895
  - dkgpg fails to enforce packet
    boundaries: https://savannah.nongnu.org/bugs/index.php?58906
  - dkgpg fails to decrypt message with unknown packet
    versions: https://savannah.nongnu.org/bugs/index.php?58937
  - dkgpg fails to verify detached signatures with unknown packet
    versions: https://savannah.nongnu.org/bugs/index.php?58939
  - dkgpg should ignore marker packets when verifying detached
    signatures: https://savannah.nongnu.org/bugs/index.php?58981
  - dkgpg throws std::bad_alloc on duplicated
    subkeys: https://savannah.nongnu.org/bugs/index.php?58983
  - AEAD encrypted messages still contain the MDC
    packet: https://savannah.nongnu.org/bugs/index.php?62012

## GopenPGP

  - Expiration of primary key binding signatures is not
    honored: https://github.com/ProtonMail/gopenpgp/issues/68
  - GopenPGP fails to decrypt message with unknown packet
    versions: https://github.com/ProtonMail/gopenpgp/issues/69
  - GopenPGP fails to verify detached signatures with unknown packet
    versions: https://github.com/ProtonMail/gopenpgp/issues/70
  - GopenPGP should ignore unbound or unknown certificate
    components: https://github.com/ProtonMail/gopenpgp/issues/72
  - GopenPGP does not honor certificate
    expiration: https://github.com/ProtonMail/gopenpgp/issues/80
  - GopenPGP successfully verifies signatures using
    SHA-1: https://github.com/ProtonMail/gopenpgp/issues/101
  - gosop doesn't handle 8-bit data correctly:
    https://github.com/ProtonMail/gosop/issues/15

## OpenPGP.js

  - Unknown notations with the "critical" bit set SHOULD invalidate
    the signature: https://github.com/openpgpjs/openpgpjs/issues/1136
  - Signatures without signature creation time subpackets are
    invalid: https://github.com/openpgpjs/openpgpjs/issues/1137
  - Expiration of primary key binding signatures is not
    honored: https://github.com/openpgpjs/openpgpjs/issues/1138
  - OpenPGPjs should ignore marker packets when verifying detached
    signatures: https://github.com/openpgpjs/openpgpjs/issues/1145
  - OpenPGPjs does not honor certificate expiration on direct key
    signatures: https://github.com/openpgpjs/openpgpjs/issues/1159
  - sop-openpgpjs doesn't handle 8-bit data correctly:
    https://github.com/openpgpjs/sop-openpgpjs/issues/4

## RNP

  - Generating a key with multiple userids silently ignores all but
    last userid: https://github.com/rnpgp/rnp/issues/929
  - Unbounded recursion when decrypting messages leading to unbounded
    allocation: https://github.com/rnpgp/rnp/issues/1211
  - Unknown notations with the "critical" bit set SHOULD invalidate
    the signature: https://github.com/rnpgp/rnp/issues/1234
  - Expiration of primary key binding signatures is not
    honored: https://github.com/rnpgp/rnp/issues/1247
  - RNP fails to decrypt message with unknown packet
    versions: https://github.com/rnpgp/rnp/issues/1255
  - RNP fails to verify detached signatures with unknown packet
    versions: https://github.com/rnpgp/rnp/issues/1257
  - rnp `--encrypt` ignores
    `--output`: https://github.com/rnpgp/rnp/issues/1314
  - RNP does not honor certificate expiration on direct key
    signatures: https://github.com/rnpgp/rnp/issues/1315
  - RNP takes certificate expiration information from non-primary
    userid bindings: https://github.com/rnpgp/rnp/issues/1316
  - RNP successfully verifies signatures using
    SHA-1: https://github.com/rnpgp/rnp/issues/1343

## PGPainless

  - PGPainless should not emit an uncompressed compressed data
    packet: https://github.com/pgpainless/pgpainless/issues/74
  - PGPainless should support wildcard recipient
    ids: https://github.com/pgpainless/pgpainless/issues/76
  - PGPainless successfully "decrypts" unencrypted SEIP
    packet: https://github.com/pgpainless/pgpainless/issues/77
  - PGPainless does not check the integrity of encrypted
    data: https://github.com/pgpainless/pgpainless/issues/78
  - Insecure handling of signature creation time
    subpackets: https://github.com/pgpainless/pgpainless/issues/79
  - Critical unknown subpackets and notations must invalidate
    signatures: https://github.com/pgpainless/pgpainless/issues/80
  - PGPainless should treat signatures using insecure hash algorithms
    as invalid: https://github.com/pgpainless/pgpainless/issues/81
  - PGPainless emits a 'Version' header in the ASCII
    armor: https://github.com/pgpainless/pgpainless/issues/82
  - Marker packets must be
    ignored: https://github.com/pgpainless/pgpainless/issues/84
  - PGPainless ignores certificate
    expiration: https://github.com/pgpainless/pgpainless/issues/85
  - PGPainless ignores
    revocations: https://github.com/pgpainless/pgpainless/issues/86
  - Potential denial-of-service problem related to recursion
    depth: https://github.com/pgpainless/pgpainless/issues/87

## PGPy

  - Primary key binding signature is not
    checked: https://github.com/SecurityInnovation/PGPy/issues/331
  - Critical, unknown subpackets and notations SHOULD invalidate
    signatures: https://github.com/SecurityInnovation/PGPy/issues/332
  - PGPy seems to ignore key
    flags: https://github.com/SecurityInnovation/PGPy/issues/333
  - PGPy fails to decrypt message with unknown packet
    versions: https://github.com/SecurityInnovation/PGPy/issues/334
  - PGPy fails to verify detached signatures with unknown packet
    versions: https://github.com/SecurityInnovation/PGPy/issues/335
  - PGPy should ignore marker packets when verifying detached
    signatures: https://github.com/SecurityInnovation/PGPy/issues/337
  - PGPy should ignore marker packets when parsing
    certificates: https://github.com/SecurityInnovation/PGPy/issues/338
  - PGPy should ignore unbound certificate
    components: https://github.com/SecurityInnovation/PGPy/issues/339
  - PGPy does not honor certificate
    expiration: https://github.com/SecurityInnovation/PGPy/issues/348

## GPGME

  - Dubious filename in literal data packet when encrypting with
    GPGME/GnuPGv1.4.23: https://dev.gnupg.org/T4725
  - GPGME gets FDs confused in high concurrency settings, deadlocks on
    select
  - GPGME reports an incorrect engine version if the path is not
    absolute
  - GPGME from debian unstable uses `--with-keygrip` unconditionally,
    breaking GnuPG 1.4: https://bugs.debian.org/984594

## GnuPG

  - GnuPG creates keys that it cannot use, likely related to
    AEAD: https://dev.gnupg.org/T4727
  - GnuPG 1.4.23 ignores key flags
  - All versions of GnuPG fail to enforce packet boundaries
  - Expiration of primary key binding signatures is not honored
  - All versions of GnuPG fail to decrypt message with unknown packet
    versions
  - All versions of GnuPG fail to verify detached signatures with
    unknown packet versions
  - All versions of GnuPG fail to ignore signatures and keys with
    unknown packet versions in certificates
  - All versions of GnuPG successfully verify signatures using SHA-1
  - All versions of GnuPG fail to correctly consume excess bytes in a
    container packet
