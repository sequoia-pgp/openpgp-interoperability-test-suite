function escape_for_shell(input) {
    return JSON.stringify(input)
        .replaceAll('\\\\', '\\\\\\\\')
        .replaceAll(/[`$]/g, '\\$&')
        .replaceAll('!', '"\'!\'"');
}
function insert_artifact_button(command, artifact) {
    const button = document.createElement('button');
    button.type = 'submit';
    button.title = 'Inspect with packet dumper';
    button.textContent = '🔎';
    button.dataset.value = artifact;
    command.before(button);
}
function replace_artifacts(ev) {
    ev.target.querySelectorAll('.command').forEach(command => {
        const match = command.textContent.match(/<\(echo -ne (?!"\$)(.*)\)/);
        if (match) {
            insert_artifact_button(command, JSON.parse(match[1].replaceAll('"\'!\'"', '!')));
        } else if (command.textContent.includes('$artifact')) {
            const artifact = command
                .closest('tr')
                .querySelector('td:nth-child(2) .details .data');
            if (artifact) {
                command.textContent = command.textContent.replaceAll(
                    '"$artifact"',
                    escape_for_shell(artifact.textContent)
                );
                insert_artifact_button(command, artifact.textContent);
            } else {
                // Artifact was omitted due to size
            }
        } else if (command.textContent.includes('$additional-artifact')) {
            command.closest('.test-result').querySelectorAll('ul button').forEach((button, i) => {
                if (command.textContent.includes(`"$additional-artifact-${i}"`)) {
                    const artifact = button.dataset.value;
                    command.textContent = command.textContent.replaceAll(
                        `"$additional-artifact-${i}"`,
                        escape_for_shell(artifact)
                    );
                    insert_artifact_button(command, artifact);
                }
            });
        }
    });
    ev.target.addEventListener('copy', ev => {
        // Firefox copies double newlines between pres, let's fix that.
        const selection = document.getSelection();
        const pres = Array.from({ length: selection.rangeCount }, (_n, i) => {
            const range = selection.getRangeAt(i);
            const contents = range.cloneContents();
            return Array.from(contents.querySelectorAll('pre'), pre => pre.textContent);
        }).flat();
        if (selection.toString().includes(pres.join('\n\n'))) {
            const text = selection.toString().replace(pres.join('\n\n'), pres.join('\n'));
            ev.clipboardData.setData('text/plain', text);
            ev.preventDefault();
        }
    });
    ev.target.removeEventListener(ev.type === 'mouseenter' ? 'focus' : 'mouseenter', replace_artifacts);
}
document.querySelectorAll('td').forEach(td => {
    // XXX: Querying for 'td:has(.details)' is more elegant, but
    // support for the :has selector is quite new, see
    // https://developer.mozilla.org/en-US/docs/Web/CSS/:has
    if (td.getElementsByClassName("details").length == 0) {
        return;
    }

    td.addEventListener('mouseenter', replace_artifacts, {
        once: true,
        passive: true
    });
    td.addEventListener('focus', replace_artifacts, {
        once: true,
        passive: true
    });
});
const input = document.getElementById('dump-input');
document.addEventListener('click', function(ev) {
    if (ev.target.tagName === 'BUTTON') {
        if (ev.target.dataset.value) {
            input.value = ev.target.dataset.value;
        } else {
            input.value = ev.target.closest('td').querySelector('.details .data').textContent;
        }
    }
});
const trs = document.querySelectorAll('tr:has(td[tabindex])');
document.addEventListener('keydown', function(ev) {
    const td = document.activeElement.closest('td');
    if (!td) return;
    let newTd;
    switch (ev.key) {
        case 'ArrowLeft': newTd = td.previousElementSibling; break;
        case 'ArrowRight': newTd = td.nextElementSibling; break;
        case 'ArrowUp':
        case 'ArrowDown':
            let trIndex = Array.prototype.indexOf.call(trs, td.parentElement);
            const tdIndex = Array.prototype.indexOf.call(td.parentElement.children, td);
            do {
                trIndex += ev.key === 'ArrowDown' ? 1 : -1;
                newTd = trs[trIndex]?.children[tdIndex];
            } while (newTd?.tabIndex !== 0 && trs[trIndex]);
            break;
    }
    if (newTd?.tabIndex === 0) {
        newTd.focus();
        const pos = newTd.getBoundingClientRect().top;
        if (pos < 400) {
          window.scrollBy(0, pos - 400);
        }
        ev.preventDefault();
    }
});
