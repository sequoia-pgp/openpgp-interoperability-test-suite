// Regular-expression-based filter for the results.

/* Courtesy of https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions#escaping */
function escapeRegExp(string) {
    return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
}

const term = document.getElementById("search-input");
const note = document.getElementById("search-note");
const results = document.getElementById("search-results");
const index = {};

// Initialize on demand.
let initialized = false;
function initialize() {
    if (initialized) {
        return;
    }

    // Fix the current layout to prevent it from changing.

    // First, fix the width of the introduction.  This
    // takes care of the main column.
    const intro = document.getElementById("introduction");
    intro.style.width = `${intro.clientWidth}px`;

    // Second, fix the width of the navigation element.
    const nav = document.querySelector("nav");
    nav.style.width = `${nav.clientWidth}px`;

    // Now build the search index.
    document.querySelectorAll(".test-result").forEach((e) => {
        const test_title = e.querySelector("h3 a");
        const description = e.querySelector(".description");
        const tags = e.querySelector(".tags");
        const title = test_title.textContent;
        const slug = test_title.getAttribute("name");

        const new_terms = new Set();
        const segments = [
            normalize_text(title, new_terms),
        ];
        const walk = (e) => {
            if (e.data) {
                segments.push(normalize_text(e.data, new_terms));
            } else {
                e.childNodes.forEach(walk)
            }
        };
        description.childNodes.forEach(walk);
        tags.childNodes.forEach(walk);
        new_terms.forEach((t) => segments.push(t));

        const full_text = segments.join(" ");
        index[slug] = full_text;

        if (false) {
            const d = document.createElement("p");
            d.textContent = full_text;
            e.appendChild(d);
        }
    });

    initialized = true;
}

const normalize_punctuation = /[:,*+?^${}()<>|[\]\\'"`]/g;
const normalize_whitespace = /\s+/g;
function normalize_text(t, new_terms) {
    const normalized = t
          .replace(normalize_punctuation, " ")
          .replace(normalize_whitespace, " ")
          .trim();

    for (const m of normalized.matchAll(/\b.*[-].*\b/g)) {
        new_terms.add(m[0].replace(/[-]/g, " "));
        new_terms.add(m[0].replace(/[-]/g, ""));
    }

    return normalized;
}

function search_on() {
    document.querySelectorAll(".hide-on-search")
        .forEach((e) => e.hidden = true);
    document.querySelectorAll(".show-on-search")
        .forEach((e) => e.hidden = false);
}

function search_off() {
    document.querySelectorAll(".hide-on-search, .test-result, .section-header")
        .forEach((e) => e.hidden = false);
    document.querySelectorAll(".show-on-search")
        .forEach((e) => e.hidden = true);
}

function filter_results() {
    initialize();

    if (term.value == "") {
        search_off();

        url.searchParams.delete('q');
        window.history.replaceState(null, '', url);
        return;
    }

    let re;
    try {
        re = new RegExp(term.value, 'i');
        note.textContent = "";
    } catch (e) {
        const escaped = escapeRegExp(term.value);
        note.textContent =
            `Invalid regular expression, searching for "${escaped}" instead.`;
        re = new RegExp(escaped, 'i');
    }

    let current_section = null;
    let current_section_matched = false;
    while (results.firstChild) {
        results.removeChild(results.firstChild);
    }

    document.querySelectorAll(".test-result, .section-header").forEach((e) => {
        const test_title = e.querySelector("h3 a");
        if (test_title) {
            /* Found a test result.  */

            const title = test_title.textContent;
            const slug = test_title.getAttribute("name");

            const matches = re.test(index[slug]);
            e.hidden = ! matches;
            current_section_matched |= matches;

            if (matches) {
                const a = document.createElement("a");
                a.setAttribute("href", test_title.getAttribute("href"));
                a.textContent = title;
                const li = document.createElement("li");
                li.appendChild(a);
                results.appendChild(li);
            }
        } else {
            /* Found a new section header.  */

            if (current_section) {
                current_section.hidden = ! current_section_matched;
            }
            current_section = e;
            current_section_matched = false;
        }
    });
    if (current_section) {
        current_section.hidden = ! current_section_matched;
    }

    search_on();

    url.searchParams.set('q', term.value);
    window.history.replaceState(null, '', url);
}

const url = new URL(window.location);

// If there is a query in the URL, populate the search field with it.
const query_param = url.searchParams.get('q');
if (query_param) {
    term.value = query_param;
}

// Filter the results once all test results have loaded.
document.addEventListener('DOMContentLoaded', function() {
    if (term.value) {
        filter_results();
    }

    term.addEventListener('input', filter_results);
    term.addEventListener('search', filter_results);
});

if (! window.location.hash) {
    term.focus();
}


// Filter implementations.

const select_impls = document.querySelectorAll('.filter-implementations input.select-imp');
const select_all = document.querySelector('.filter-implementations input#select-all');
const select_default = document.querySelector('.filter-implementations input#select-default');
const select_prereleases = document.querySelector('.filter-implementations input#select-prereleases');
const style = document.querySelector('.filter-implementations style');

const impls_param = url.searchParams.get('impls');

const impl_names = new Set();
let default_impls_bitset = 0;
select_impls.forEach((input, i) => {
    const impl_name = input.nextSibling.textContent.split(' ')[1];
    const new_impl_name = !impl_names.has(impl_name);
    default_impls_bitset |= new_impl_name * (1 << i);
    impl_names.add(impl_name);

    if (impls_param) {
        input.checked = impls_param & (1 << i);
    } else {
        input.checked = new_impl_name;
    }

    input.addEventListener('input', filter_implementations);
});

select_all.addEventListener('input', () => {
    select_impls.forEach(input => {
        input.checked = select_all.checked;
    });
    filter_implementations();
});

select_default.addEventListener('click', () => {
    select_impls.forEach((input, i) => {
        input.checked = default_impls_bitset & (1 << i);
    });
    filter_implementations();
});

select_prereleases.addEventListener('click', () => {
    select_impls.forEach(input => {
        const impl_summary = input.nextSibling.textContent;
        input.checked = impl_summary.includes('-') ||
            (impl_summary.includes('+') && !impl_summary.endsWith('+'));
    });
    filter_implementations();
});

function filter_implementations() {
    let active_impls_bitset = 0;
    style.textContent = Array.from(select_impls, (input, i) => {
        active_impls_bitset |= input.checked * (1 << i);
        return input.checked ? '' : `
            table.matrix:not(.example) thead th:nth-child(${ i + 3 }),
            table.matrix:not(.example) tbody td:nth-child(${ i + 3 }),
            table.matrix:not(.example) tbody tr[data-producer="${ i }"],
            div.chart li[data-implementation="${ i }"] {
                display: none;
            }
        `;
    }).join('');

    select_all.checked = active_impls_bitset === (1 << select_impls.length) - 1;

    if (active_impls_bitset !== default_impls_bitset) {
        url.searchParams.set('impls', active_impls_bitset);
    } else {
        url.searchParams.delete('impls');
    }
    window.history.replaceState(null, '', url);
}

filter_implementations();
