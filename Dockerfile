# syntax=docker/dockerfile:1

ARG base=sops

# We start from the image with the SOP implementations.
FROM ${base}
RUN apt update -y -qq && apt install -y -qq --no-install-recommends git build-essential pkg-config ca-certificates rustc cargo clang nettle-dev libssl-dev

WORKDIR /scratch

# AMAZING FUCKING COPY SEMANTICS!!!
#
# THAT DOESN'T WORK:
#
# COPY .git data src Cargo.toml Cargo.lock /scratch/
#
# INSTEAD WE NEED TO DUMB IT DOWN:
COPY .git /scratch/.git
COPY data /scratch/data
COPY src /scratch/src
COPY build.rs Cargo.toml Cargo.lock /scratch/
COPY templates /test-suite/templates
RUN cargo build

WORKDIR /test-suite
ENTRYPOINT ["/scratch/target/debug/openpgp-interoperability-test-suite", \
            "--json-out=output/results.json", \
            "--html-out=output/results.html"]
