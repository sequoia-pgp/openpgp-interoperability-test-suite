use std::io::Write;

use anyhow::ensure;

use sequoia_openpgp as openpgp;
use openpgp::{
    parse::Parse,
    types::SymmetricAlgorithm,
};
use crate::{
    Data,
    Result,
    data,
    sop::{Sop, SopResult, Verification},
    tests::{
        Expectation,
        TestMatrix,
        ConsumptionTest,
        CheckError,
    },
};

/// Tests how recipient ids are handled.
pub struct RecipientIDs {
}

impl RecipientIDs {
    pub fn new() -> Result<RecipientIDs> {
        Ok(RecipientIDs {
        })
    }
}

impl crate::plan::Runnable<TestMatrix> for RecipientIDs {
    fn title(&self) -> String {
        "Recipient IDs".into()
    }

    fn description(&self) -> String {
        "<p>Tests variations of recipient ids.</p>".into()
    }

    fn artifacts(&self) -> Vec<(String, Data)> {
        vec![("TSK".into(), data::certificate("bob-secret.pgp").into())]
    }

    fn run(&self, matrix: TestMatrix, implementations: &[crate::Sop])
           -> Result<TestMatrix> {
        ConsumptionTest::run(self, matrix, implementations)
    }
}

impl ConsumptionTest<Data, (Data, Vec<Verification>)> for RecipientIDs {
    fn produce(&self) -> Result<Vec<(String, Data, Option<Expectation>)>> {
        use openpgp::serialize::stream::Encryptor2 as Encryptor;
        use openpgp::serialize::stream::*;

        // The tests.
        let mut t = Vec::new();

        // Use the RSA key to increase compatibility.
        let cert =
            openpgp::Cert::from_bytes(data::certificate("bob.pgp"))?;

        // The base case.
        let mut buf = Vec::new();
        let message = Message::new(&mut buf);
        let message = Armorer::new(message).build()?;
        let recipients =
            cert.keys().with_policy(crate::tests::P, None)
            .for_transport_encryption();
        let message = Encryptor::for_recipients(message, recipients)
            .symmetric_algo(SymmetricAlgorithm::AES256)
            .build()?;
        let mut message = LiteralWriter::new(message).build()?;
        message.write_all(crate::tests::MESSAGE)?;
        message.finalize()?;
        t.push(("Encryption subkey's KeyID".into(),
                buf.into(),
                Some(Ok("Base case".into()))));

        // Wildcard.
        let mut buf = Vec::new();
        let message = Message::new(&mut buf);
        let message = Armorer::new(message).build()?;
        let recipients =
            cert.keys().with_policy(crate::tests::P, None)
            .for_transport_encryption()
            .map(|key| Recipient::from(key)
                 .set_keyid(openpgp::KeyID::wildcard()));
        let message = Encryptor::for_recipients(message, recipients)
            .symmetric_algo(SymmetricAlgorithm::AES256)
            .build()?;
        let mut message = LiteralWriter::new(message).build()?;
        message.write_all(crate::tests::MESSAGE)?;
        message.finalize()?;
        t.push(("Wildcard KeyID".into(),
                buf.into(),
                Some(Ok("Interoperability concern".into()))));

        // Certificate.
        let mut buf = Vec::new();
        let message = Message::new(&mut buf);
        let message = Armorer::new(message).build()?;
        let recipients =
            cert.keys().with_policy(crate::tests::P, None)
            .for_transport_encryption()
            .map(|key| Recipient::from(key)
                 .set_keyid(cert.keyid()));
        let message = Encryptor::for_recipients(message, recipients)
            .symmetric_algo(SymmetricAlgorithm::AES256)
            .build()?;
        let mut message = LiteralWriter::new(message).build()?;
        message.write_all(crate::tests::MESSAGE)?;
        message.finalize()?;
        t.push(("Certificate KeyID".into(),
                buf.into(),
                None));

        // Fictitious encrypted keyid.
        let mut buf = Vec::new();
        let message = Message::new(&mut buf);
        let message = Armorer::new(message).build()?;
        let recipients =
            cert.keys().with_policy(crate::tests::P, None)
            .for_transport_encryption()
            .map(|key| Recipient::from(key)
                 .set_keyid("AAAA BBBB CCCC DDDD".parse().unwrap()));
        let message = Encryptor::for_recipients(message, recipients)
            .symmetric_algo(SymmetricAlgorithm::AES256)
            .build()?;
        let mut message = LiteralWriter::new(message).build()?;
        message.write_all(crate::tests::MESSAGE)?;
        message.finalize()?;
        t.push(("Fictitious KeyID".into(),
                buf.into(),
                None));

        Ok(t)
    }

    fn consume(&self, pgp: &Sop, artifact: &Data)
               -> SopResult<(Data, Vec<Verification>)> {
        pgp.sop()
            .decrypt()
            .key(data::certificate("bob-secret.pgp"))
            .ciphertext(artifact)
    }

    fn check_consumer(&self, _artifact: &Data,
                      (plaintext, _sigs): &(Data, Vec<Verification>),
                      _expectation: &Option<Expectation>)
                      -> Result<()> {
        ensure!(&plaintext[..] == crate::tests::MESSAGE,
            CheckError::HardFailure(format!(
                "Expected {:?}, got {:?}",
                crate::tests::MESSAGE, plaintext)));
        Ok(())
    }

}
