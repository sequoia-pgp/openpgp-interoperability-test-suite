use crate::{
    Data,
    Result,
    sop::{Sop, SopResult, Verification},
    tests::{
        Expectation,
        TestMatrix,
        ConsumptionTest,
    },
};

/// Tests support for PQC encrypted messages.
pub struct PQC {
}

impl PQC {
    pub fn new() -> Result<PQC> {
        Ok(PQC {
        })
    }
}

impl crate::plan::Runnable<TestMatrix> for PQC {
    fn title(&self) -> String {
        "PQC encrypted message".into()
    }

    fn description(&self) -> String {
        "Tests decryption support for experimental algorithm ID 105 \
         (ML-KEM-768+X25519).".into()
    }

    fn artifacts(&self) -> Vec<(String, Data)> {
        vec![("Key".into(), crate::data::file("pqc/pqc-secret.pgp").unwrap().into())]
    }

    fn tags(&self) -> std::collections::BTreeSet<&'static str> {
        ["v6", "pqc", "draft"].iter().cloned().collect()
    }

    fn run(&self, matrix: TestMatrix, implementations: &[crate::Sop])
           -> Result<TestMatrix> {
        ConsumptionTest::run(self, matrix, implementations)
    }
}

impl ConsumptionTest<Data, (Data, Vec<Verification>)> for PQC {
    fn produce(&self) -> Result<Vec<(String, Data, Option<Expectation>)>> {
        Ok(vec![
            ("PQC encrypted message".into(),
             crate::data::file("pqc/pqc-encrypted.pgp").unwrap().into(),
             Some(Ok("Interoperability concern.".into()))),
        ])
    }

    fn consume(&self, pgp: &Sop, artifact: &Data)
               -> SopResult<(Data, Vec<Verification>)>
    {
        pgp.sop()
            .decrypt()
            .key(crate::data::file("pqc/pqc-secret.pgp").unwrap())
            .ciphertext(artifact)
    }
}
