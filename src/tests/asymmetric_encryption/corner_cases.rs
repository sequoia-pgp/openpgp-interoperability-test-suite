use std::io::Write;

use anyhow::ensure;

use sequoia_openpgp as openpgp;
use openpgp::{
    crypto::mpi::*,
    packet::prelude::*,
    parse::Parse,
    types::SymmetricAlgorithm,
};
use crate::{
    Data,
    Result,
    data,
    sop::{Sop, SopResult, Verification},
    tests::{
        Expectation,
        TestMatrix,
        ConsumptionTest,
        CheckError,
    },
};

/// Tests asymmetric encryption corner cases.
pub struct RSAEncryption {}

impl RSAEncryption {
    pub fn new() -> RSAEncryption {
        RSAEncryption {}
    }
}

impl crate::plan::Runnable<TestMatrix> for RSAEncryption {
    fn title(&self) -> String {
        "RSA encryption corner cases".into()
    }

    fn description(&self) -> String {
        "<p>
RSA ciphertext can vary in size.  This creates an opportunity for
mishandling of buffers, e.g. <a
href=\"https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-3580\">CVE-2021-3580</a>.
</p>".into()
    }

    fn artifacts(&self) -> Vec<(String, Data)> {
        vec![("TSK".into(), data::certificate("bob-secret.pgp").into())]
    }

    fn run(&self, matrix: TestMatrix, implementations: &[crate::Sop])
           -> Result<TestMatrix> {
        ConsumptionTest::run(self, matrix, implementations)
    }
}

impl ConsumptionTest<Data, (Data, Vec<Verification>)> for RSAEncryption
{
    fn produce(&self) -> Result<Vec<(String, Data, Option<Expectation>)>> {
        use openpgp::serialize::stream::Encryptor2 as Encryptor;
        use openpgp::serialize::stream::*;

        // RSA.
        let cert =
            openpgp::Cert::from_bytes(data::certificate("bob.pgp"))?;

        // The base case.
        let mut buf = Vec::new();
        let message = Message::new(&mut buf);
        let recipients =
            cert.keys().with_policy(crate::tests::P, None)
            .for_transport_encryption();
        let message = Encryptor::for_recipients(message, recipients)
            .symmetric_algo(SymmetricAlgorithm::AES256)
            .build()?;
        let mut message = LiteralWriter::new(message).build()?;
        message.write_all(crate::tests::MESSAGE)?;
        message.finalize()?;

        let pp = openpgp::PacketPile::from_bytes(&buf)?;
        assert_eq!(pp.children().count(), 2);
        let pkesk = pp.path_ref(&[0]).unwrap();
        let pkesk_s =
            if let Packet::PKESK(p) =
                pkesk { p } else { panic!() };
        let pkesk_s =
            if let PKESK::V3(p) =
                pkesk_s { p } else { panic!() };
        let rsa_c =
            if let Ciphertext::RSA { c } =
                pkesk_s.esk() { c } else { panic!() };
        let seip = pp.path_ref(&[1]).unwrap();

        let make_test = |test, packets, expectation| {
            crate::tests::make_test(test, packets,
                                    openpgp::armor::Kind::Message,
                                    expectation)
                .map(|(description, data, expectation)|
                     (description, data, expectation))
        };
        Ok(vec![
            make_test("Base case", vec![pkesk.clone(), seip.clone()],
                      Some(Ok("Interoperability concern".into())))?,
            make_test("zero ciphertext", vec![
                {
                    let mut p = pkesk_s.clone();
                    p.set_esk(Ciphertext::RSA { c: vec![].into() });
                    p.into()
                },
                seip.clone(),
            ], Some(Err("Must fail (gracefully!)".into())))?,

            make_test("ciphertext - 1 bit", vec![
                {
                    let mut p = pkesk_s.clone();
                    let mut c = rsa_c.value().to_vec();
                    // Clear one bit in the MSB, and set the next one.
                    match c[0].leading_zeros() {
                        7 => {
                            // Underflow.
                            c[0] &= !1;
                            assert_eq!(c[0], 0);
                            // Set bit in the next byte.
                            c[1] |= 1 << 7;
                        },
                        8 =>
                            panic!("leading zero byte in MPI"),
                        n => {
                            assert!(n < 7);
                            c[0] &= !(1 << (7 - n));
                            // Set the next bit.
                            c[0] |= 1 << (7 - n - 1);
                        },
                    }
                    p.set_esk(Ciphertext::RSA { c: c.into() });
                    p.into()
                },
                seip.clone(),
            ], Some(Err("Must fail (gracefully!)".into())))?,

            // XXX: This has a chance to succeed due to the
            // modulo-arithmetic in RSA.
            //make_test("ciphertext + 1 bit", vec![
            //    {
            //        let mut p = pkesk_s.clone();
            //        let mut c = rsa_c.value().to_vec();
            //        // Set one more bit in the MSB.
            //        match c[0].leading_zeros() {
            //            0 => // Overflow.  Need a new MSB.
            //                c.insert(0, 0x01),
            //            8 =>
            //                panic!("leading zero byte in MPI"),
            //            n => {
            //                // Set the bit.
            //                assert!(n < 8);
            //                c[0] |= 1 << (7 - n);
            //            },
            //        }
            //        p.set_esk(Ciphertext::RSA { c: c.into() });
            //        p.into()
            //    },
            //    seip.clone(),
            //], Some(Err("Must fail (gracefully!)".into())))?,

            make_test("ciphertext - 8 bit", vec![
                {
                    let mut p = pkesk_s.clone();
                    let mut c = rsa_c.value().to_vec();
                    c.pop();
                    p.set_esk(Ciphertext::RSA { c: c.into() });
                    p.into()
                },
                seip.clone(),
            ], Some(Err("Must fail (gracefully!)".into())))?,

            make_test("ciphertext + 8 bit", vec![
                {
                    let mut p = pkesk_s.clone();
                    let mut c = rsa_c.value().to_vec();
                    c.push(255);
                    p.set_esk(Ciphertext::RSA { c: c.into() });
                    p.into()
                },
                seip.clone(),
            ], Some(Err("Must fail (gracefully!)".into())))?,

            make_test("ciphertext + 1 MSB", vec![
                {
                    let mut p = pkesk_s.clone();
                    let mut c = rsa_c.value().to_vec();
                    c.insert(0, 255);
                    p.set_esk(Ciphertext::RSA { c: c.into() });
                    p.into()
                },
                seip.clone(),
            ], Some(Err("Must fail (gracefully!)".into())))?,
        ])
    }

    fn consume(&self, pgp: &Sop,
               ciphertext: &Data)
               -> SopResult<(Data, Vec<Verification>)> {
        pgp.sop()
            .decrypt()
            .key(data::certificate("bob-secret.pgp"))
            .ciphertext(ciphertext)
    }

    fn check_consumer(&self, _: &Data,
                      (plaintext, _sigs): &(Data, Vec<Verification>),
                      expectation: &Option<Expectation>)
                      -> Result<()> {
        if let &Some(Ok(_)) = expectation {
            // Base case.
            ensure!(&plaintext[..] == crate::tests::MESSAGE,
                CheckError::HardFailure(format!(
                    "Expected {:?}, got {:?}",
                    crate::tests::MESSAGE, plaintext)));
        }

        // No checks for the other cases.
        Ok(())
    }
}
