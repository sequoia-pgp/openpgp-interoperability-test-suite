use anyhow::{
    ensure,
    bail,
    Context,
};

use sequoia_openpgp as openpgp;
use openpgp::cert::prelude::*;
use openpgp::types::*;
use openpgp::parse::Parse;
use openpgp::serialize::SerializeInto;

use crate::{
    Data,
    Result,
    data,
    plan::Runnable,
    sop::{Sop, SopWithProfile, SopResult, Verification},
    tests::TestPlan,
    tests::{
        Expectation,
        TestMatrix,
        RoundtripTest,
        CheckError,
        ListOf,
    },
};

mod pqc;
mod recipient_ids;
mod corner_cases;
mod rsa_key_sizes;

/// Roundtrip tests check whether consume(produce(x)) yields x.
pub struct EncryptDecryptRoundtrip {
    title: String,
    description: String,
    certs: Vec<Vec<u8>>,
    key: Vec<u8>,
    cipher: Option<openpgp::types::SymmetricAlgorithm>,
    aead: Option<openpgp::types::AEADAlgorithm>,
    features: Features,
    message: Data,
    default_expectation: Option<std::result::Result<String, String>>,
    producer_expectation: Option<std::result::Result<String, String>>,
}

impl EncryptDecryptRoundtrip {
    pub fn new(title: &str, description: &str, cert: openpgp::Cert,
               message: Data) -> Result<EncryptDecryptRoundtrip> {
        Self::with_certs_and_key(title, description,
                                vec![cert.armored().to_vec()?],
                                cert.as_tsk().armored().to_vec()?,
                                message)
    }

    pub fn with_certs_and_key(title: &str, description: &str,
                             certs: Vec<Vec<u8>>, key: Vec<u8>,
                             message: Data) -> Result<EncryptDecryptRoundtrip> {
        Ok(EncryptDecryptRoundtrip {
            title: title.into(),
            description: description.into(),
            certs,
            key,
            cipher: None,
            aead: None,
            features: Features::empty(),
            message,
            default_expectation: Some(Ok("Interoperability concern.".into())),
            producer_expectation: None,
        })
    }

    pub fn with_cipher(title: &str, description: &str, cert: openpgp::Cert,
                       message: Data,
                       cipher: openpgp::types::SymmetricAlgorithm,
                       aead: Option<openpgp::types::AEADAlgorithm>,
                       features: Features)
                       -> Result<EncryptDecryptRoundtrip>
    {
        // Change the cipher preferences of CERT.
        let uid = cert.with_policy(super::P, None).unwrap()
            .primary_userid().unwrap();
        let mut builder = openpgp::packet::signature::SignatureBuilder::from(
            uid.binding_signature().clone())
            .set_signature_creation_time(Timestamp::now())?
            .set_features(features.clone())?;
        if features.supports_seipdv2() {
            if let Some(algo) = aead {
                builder = builder.set_preferred_aead_ciphersuites(vec![(cipher, algo)])?;
            }
        } else {
            builder = builder.set_preferred_symmetric_algorithms(vec![cipher])?;
            #[allow(deprecated)]
            if let Some(algo) = aead {
                builder = builder.set_preferred_aead_algorithms(vec![algo])?;
            }
        }
        let mut primary_keypair =
            cert.primary_key()
            .key().clone().parts_into_secret()?.into_keypair()?;
        let new_sig = uid.bind(&mut primary_keypair, &cert, builder)?;
        let cert = cert.insert_packets(Some(new_sig))?;
        let key = cert.as_tsk().armored().to_vec()?;
        let cert = cert.armored().to_vec()?;

        Ok(EncryptDecryptRoundtrip {
            title: title.into(),
            description: description.into(),
            certs: vec![cert],
            key,
            cipher: Some(cipher),
            aead,
            features: features,
            message,
            default_expectation: None,
            producer_expectation: None,
        })
    }
}

impl crate::plan::Runnable<TestMatrix> for EncryptDecryptRoundtrip {
    fn title(&self) -> String {
        self.title.clone()
    }

    fn description(&self) -> String {
        self.description.clone()
    }

    fn artifacts(&self) -> Vec<(String, Data)> {
        let mut artifacts: Vec<(String, Data)> = self.certs.iter().map(|cert| {
            ("Certificate".into(), cert.clone().into())
        }).collect();
        artifacts.push(
            ("Key".into(), self.key.clone().into())
        );
        artifacts
    }

    fn run(&self, matrix: TestMatrix, implementations: &[crate::Sop]) -> Result<TestMatrix> {
        RoundtripTest::run(self, matrix, implementations)
    }
}

impl RoundtripTest<Data, (Data, Vec<Verification>)> for EncryptDecryptRoundtrip
{
    fn produce(&self, sop: &SopWithProfile)
               -> SopResult<Data> {
        let mut encrypt = sop.encrypt();
        for cert in &self.certs {
            encrypt = encrypt.cert(&cert);
        }
        encrypt.plaintext(&self.message)
    }

    fn check_producer(&self, artifact: &Data) -> Result<()> {
        let pp = openpgp::PacketPile::from_bytes(&artifact)
            .context("Produced data is malformed")?;

        // Check that the number of PKESK packets matches the number of
        // certificates.
        let pkesk_count = pp.children()
            .filter(|p| p.tag() == openpgp::packet::Tag::PKESK)
            .count();
        ensure!(pkesk_count == self.certs.len(),
            CheckError::HardFailure(format!(
                "Expected {:?} PKESK packets, found {:?}",
                self.certs.len(), pkesk_count)));

        if self.features != Features::empty() {
            // Check that the PKESK packets are of the expected version.
            for pkesk in pp.children()
                .filter(|p| p.tag() == openpgp::packet::Tag::PKESK) {
                let version = pkesk.version().unwrap();
                let expected_version = if self.features.supports_seipdv2() { 6 } else { 3 };
                ensure!(version == expected_version,
                    CheckError::SoftFailure(format!(
                        "Expected version {:?} PKESK packet, found version {:?}",
                        expected_version, version)));
            }
        }

        // Check whether the implementation used the algorithm(s) that
        // were requested, if any. Note that using a different algorithm
        // is only a "soft failure", as the artifact may still be valid
        // (but using a different algorithm), and the test expectations
        // indicate whether the algorithm is expected to be implemented.

        if let Some(aead_algo) = self.aead {
            match pp.children().last() {
                Some(openpgp::Packet::SEIP(openpgp::packet::SEIP::V2(s))) => {
                    ensure!(self.features.supports_seipdv2(),
                        CheckError::SoftFailure(
                            "Producer used SEIPDv2 packet when support was not signalled".into()));

                    let cipher = self.cipher.unwrap();
                    ensure!((s.symmetric_algo(), s.aead()) == (cipher, aead_algo),
                        CheckError::SoftFailure(format!(
                            "Producer did not use {:?}-{:?}, but {:?}-{:?}",
                            cipher, aead_algo, s.symmetric_algo(), s.aead())));
                },
                #[allow(deprecated)]
                Some(openpgp::Packet::AED(a)) => {
                    ensure!(self.features.supports_aead(),
                        CheckError::SoftFailure(
                            "Producer used AEAD Encrypted Data packet when support was not signalled".into()));

                    let sym_algo = self.cipher.unwrap();
                    ensure!((a.symmetric_algo(), a.aead()) == (sym_algo, aead_algo),
                        CheckError::SoftFailure(format!(
                            "Producer did not use {:?}-{:?}, but {:?}-{:?}",
                            sym_algo, aead_algo, a.symmetric_algo(), a.aead())));
                },
                Some(p) => bail!(CheckError::SoftFailure(format!(
                    "Producer did not use AEAD, found {} packet", p.tag()))),
                None => bail!(CheckError::HardFailure("No packet emitted".into())),
            }
        } else if let Some(cipher) = self.cipher {
            // Check that the producer used CIPHER.
            let cert = openpgp::Cert::from_bytes(&self.key)?;
            let mode = KeyFlags::empty()
                .set_storage_encryption().set_transport_encryption();

            let mut ok = false;
            let mut algos = Vec::new();
            'search: for p in pp.children() {
                match p {
                    openpgp::Packet::PKESK(p) => {
                        for ka in cert.keys().with_policy(super::P, None).secret()
                            .key_flags(mode.clone())
                        {
                            let mut keypair = ka.key().clone().into_keypair()?;
                            if let Some((Some(a), _)) = p.decrypt(&mut keypair, None) {
                                if a == cipher {
                                    ok = true;
                                    break 'search;
                                }
                                algos.push(a);
                            }
                        }
                    },
                    openpgp::Packet::SEIP(openpgp::packet::SEIP::V2(_)) => {
                        bail!(CheckError::SoftFailure(
                            "Producer used SEIPDv2 packet when support was not signalled".into()));
                    },
                    openpgp::Packet::AED(_) => {
                        bail!(CheckError::SoftFailure(
                            "Producer used AEAD Encrypted Data packet when support was not signalled".into()));
                    },
                    _ => ()
                }
            }

            ensure!(ok,
                CheckError::SoftFailure(format!(
                    "Producer did not use {:?}, but {}",
                    cipher, ListOf(&algos))));
        }

        Ok(())
    }

    fn consume(&self,
               _producer: &Sop,
               consumer: &Sop,
               artifact: &Data)
               -> SopResult<(Data, Vec<Verification>)> {
        consumer.sop()
            .decrypt()
            .key(&self.key)
            .ciphertext(artifact)
    }

    fn check_consumer(&self, _ciphertext: &Data,
                      (plaintext, _sigs): &(Data, Vec<Verification>))
                      -> Result<()> {
        ensure!(&plaintext[..] == &self.message[..],
            CheckError::HardFailure(format!("Expected {:?}, got {:?}",
                                            self.message, plaintext)));
        Ok(())
    }

    fn producer_expectation(&self, profile: &String) -> Option<Expectation> {
        use SymmetricAlgorithm::*;

        match (self.cipher, self.aead, self.features.supports_seipdv2()) {
            (Some(IDEA) | Some(TripleDES) | Some(CAST5), _, _) => // Deprecated in the crypto refresh.
                Some(Err("Implementations MUST NOT encrypt data with this algorithm.".into())),
            _ => self.producer_expectation.clone().or_else(|| self.expectation(profile)),
        }
    }

    fn expectation(&self, _profile: &String) -> Option<Expectation> {
        use SymmetricAlgorithm::*;
        use AEADAlgorithm::*;

        match (self.cipher, self.aead, self.features.supports_seipdv2()) {
            (Some(AES128), None, _) =>
                Some(Ok("Implementations MUST implement AES-128.".into())),
            (Some(AES128), Some(OCB), true) =>
                Some(Ok("Implementations MUST implement AES-128 and OCB.".into())),
            (Some(AES256), None, _) =>
                Some(Ok("Implementations SHOULD implement AES-256.".into())),
            (Some(AES256), Some(OCB), true) =>
                Some(Ok("Implementations SHOULD implement AES-256 and MUST implement OCB.".into())),
            _ => self.default_expectation.clone(),
        }
    }
}

pub fn schedule(plan: &mut TestPlan) -> Result<()> {
    plan.add_section("Asymmetric Encryption");
    plan.add(
        EncryptDecryptRoundtrip::with_certs_and_key(
            "Encrypt-Decrypt roundtrip with minimal key from RFC9580",
            "Encrypt-Decrypt roundtrip with minimal key from \
             Appendix A.3 of RFC9580.",
            vec![data::file("v6/test-vectors/v6-minimal-cert.key").unwrap().into()],
            data::file("v6/test-vectors/v6-minimal-secret.key").unwrap().into(),
            crate::tests::MESSAGE.to_vec().into())?
            .with_tags(&["v6"]));
    plan.add(
        EncryptDecryptRoundtrip::with_certs_and_key(
            "Encrypt with key from RFC9580 and key 'Alice', decrypt with key from RFC9580",
            "Encrypt-Decrypt roundtrip with multiple keys: \
                the message is encrypted with the minimal key from \
                Appendix A.3 of RFC9580 and the 'Alice' key from \
                draft-bre-openpgp-samples-00; and decrypted using \
                the key from RFC9580 only.",
            vec![data::file("v6/test-vectors/v6-minimal-cert.key").unwrap().into(), data::certificate("alice.pgp").into()],
            data::file("v6/test-vectors/v6-minimal-secret.key").unwrap().into(),
            crate::tests::MESSAGE.to_vec().into())?
            .with_tags(&["v6"]));
    plan.add(
        EncryptDecryptRoundtrip::with_certs_and_key(
            "Encrypt with key from RFC9580 and key 'Alice', decrypt with key 'Alice'",
            "Encrypt-Decrypt roundtrip with multiple keys: \
                the message is encrypted with the minimal key from \
                Appendix A.3 of RFC9580 and the 'Alice' key from \
                draft-bre-openpgp-samples-00; and decrypted using \
                the 'Alice' key only.",
            vec![data::file("v6/test-vectors/v6-minimal-cert.key").unwrap().into(), data::certificate("alice.pgp").into()],
            data::certificate("alice-secret.pgp").into(),
            crate::tests::MESSAGE.to_vec().into())?
            .with_tags(&["v6"]));
    plan.add(Box::new(
        EncryptDecryptRoundtrip::new(
            "Encrypt-Decrypt roundtrip with key 'Alice'",
            "Encrypt-Decrypt roundtrip using the 'Alice' key from \
             draft-bre-openpgp-samples-00.",
            openpgp::Cert::from_bytes(data::certificate("alice-secret.pgp"))?,
            crate::tests::MESSAGE.to_vec().into())?));
    plan.add(Box::new(
        EncryptDecryptRoundtrip::new(
            "Encrypt-Decrypt roundtrip with key 'Bob'",
            "Encrypt-Decrypt roundtrip using the 'Bob' key from \
             draft-bre-openpgp-samples-00.",
            openpgp::Cert::from_bytes(data::certificate("bob-secret.pgp"))?,
            crate::tests::MESSAGE.to_vec().into())?));
    plan.add(Box::new(
        EncryptDecryptRoundtrip {
            title: "Encrypt-Decrypt roundtrip with key 'Carol'".into(),
            description: "Encrypt-Decrypt roundtrip using the 'Carol' key from \
             draft-bre-openpgp-samples-00.".into(),
            certs: vec![data::certificate("carol.pgp").into()],
            key: data::certificate("carol-secret.pgp").into(),
            cipher: None,
            aead: None,
            features: Features::empty(),
            message: crate::tests::MESSAGE.to_vec().into(),
            default_expectation: None,
            producer_expectation: Some(Err("Implementations MUST NOT encrypt using Elgamal keys according to RFC9580.".into())),
        }));
    plan.add(
        EncryptDecryptRoundtrip {
            title: "Encrypt-Decrypt roundtrip with key 'John'".into(),
            description:
            "Encrypt-Decrypt roundtrip using the v3 'John' key.".into(),
            certs: vec![data::certificate("john.pgp").into()],
            key: data::certificate("john-secret.pgp").into(),
            cipher: None,
            aead: None,
            features: Features::empty(),
            message: crate::tests::MESSAGE.to_vec().into(),
            default_expectation: None,
            producer_expectation: None,
        }
        .with_tags(&["v3"]));
    plan.add(
        EncryptDecryptRoundtrip {
            title: "Encrypt-Decrypt roundtrip with key 'Emma'".into(),
            description:
            "Encrypt-Decrypt roundtrip using the v5 'Emma' key.".into(),
            certs: vec![data::file("v5/v5-sample-1-pub.asc").unwrap().into()],
            key: data::file("v5/v5-sample-1-sec.asc").unwrap().into(),
            cipher: None,
            aead: None,
            features: Features::empty(),
            message: crate::tests::MESSAGE.to_vec().into(),
            default_expectation: None,
            producer_expectation: None,
        }
        .with_tags(&["v5"]));
    plan.add(
        EncryptDecryptRoundtrip {
            title: "Encrypt-Decrypt roundtrip with a PQC key".into(),
            description: "Encrypt-Decrypt roundtrip with a PQC key from \
             Appendix A.1 of draft-ietf-openpgp-pqc.".into(),
            certs: vec![data::file("pqc/pqc-public.pgp").unwrap().into()],
            key: data::file("pqc/pqc-secret.pgp").unwrap().into(),
            cipher: None,
            aead: None,
            features: Features::empty(),
            message: crate::tests::MESSAGE.to_vec().into(),
            default_expectation: Some(Ok("Interoperability concern.".into())),
            producer_expectation: None,
        }
        .with_tags(&["v6", "pqc", "draft"]));
    plan.add(
        EncryptDecryptRoundtrip {
            title: "Encrypt-Decrypt roundtrip with a persistent symmetric key".into(),
            description:
            "Encrypt-Decrypt roundtrip using the persistent symmetric key from \
             Appendix A.1 of draft-ietf-openpgp-persistent-symmetric-keys.".into(),
            certs: vec![data::certificate("symmetric.pgp").into()],
            key: data::certificate("symmetric.pgp").into(),
            cipher: None,
            aead: None,
            features: Features::empty(),
            message: crate::tests::MESSAGE.to_vec().into(),
            default_expectation: Some(Ok("Interoperability concern.".into())),
            producer_expectation: None,
        }
        .with_tags(&["draft"]));

    plan.add(Box::new(pqc::PQC::new()?));
    plan.add(Box::new(recipient_ids::RecipientIDs::new()?));
    plan.add(Box::new(corner_cases::RSAEncryption::new()));
    plan.add(Box::new(rsa_key_sizes::RSAKeySizes::new()?));
    Ok(())
}
