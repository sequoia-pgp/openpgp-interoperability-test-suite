use crate::{
    Data,
    Result,
    sop::{Sop, SopResult, Verification},
    tests::{
        Expectation,
        TestMatrix,
        ConsumptionTest,
    },
};

/// Tests support for SED encrypted messages.
pub struct SED {
}

impl SED {
    pub fn new() -> Result<SED> {
        Ok(SED {
        })
    }
}

impl crate::plan::Runnable<TestMatrix> for SED {
    fn title(&self) -> String {
        "SED encrypted message".into()
    }

    fn description(&self) -> String {
        "Tests support for SED (i.e. packet type 9), \
         not integrity protected encrypted messages.  \
         To protect SEIPDv1 messages, which may be \
         downgraded to SED messages, these MUST NOT \
         be decrypted.".into()
    }

    fn artifacts(&self) -> Vec<(String, Data)> {
        vec![("Key".into(), crate::data::certificate("bob-secret.pgp").into())]
    }

    fn run(&self, matrix: TestMatrix, implementations: &[crate::Sop])
           -> Result<TestMatrix> {
        ConsumptionTest::run(self, matrix, implementations)
    }
}

impl ConsumptionTest<Data, (Data, Vec<Verification>)> for SED {
    fn produce(&self) -> Result<Vec<(String, Data, Option<Expectation>)>> {
        Ok(vec![
            ("SED encrypted message".into(),
             crate::data::message("7C2FAA4DF93C37B2.SED.pgp").into(),
             Some(Err("MUST NOT be decrypted".into()))),
        ])
    }

    fn consume(&self, pgp: &Sop, artifact: &Data)
               -> SopResult<(Data, Vec<Verification>)>
    {
        pgp.sop()
            .decrypt()
            .key(crate::data::certificate("bob-secret.pgp"))
            .ciphertext(artifact)
    }
}
