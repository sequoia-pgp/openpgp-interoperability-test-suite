use anyhow::ensure;

use crate::{
    Data,
    Result,
    sop::{Sop, SopWithProfile, SopResult, Verification},
    tests::{
        Expectation,
        TestMatrix,
        RoundtripTest,
        CheckError,
    },
};

/// Roundtrip tests check whether consume(produce(x)) yields x.
pub struct PasswordEncryptionInterop {
}

impl PasswordEncryptionInterop {
    pub fn new() -> Result<PasswordEncryptionInterop> {
        Ok(PasswordEncryptionInterop {})
    }
}

impl crate::plan::Runnable<TestMatrix> for PasswordEncryptionInterop {
    fn title(&self) -> String {
        "Password-based encryption".into()
    }

    fn description(&self) -> String {
        format!("Encrypts a message using the password {:?}, \
                 and tries to decrypt it.", crate::tests::PASSWORD)
    }

    fn run(&self, matrix: TestMatrix, implementations: &[crate::Sop]) -> Result<TestMatrix> {
        RoundtripTest::run(self, matrix, implementations)
    }
}

impl RoundtripTest<Data, (Data, Vec<Verification>)> for PasswordEncryptionInterop
{
    fn producer_profiles(&self, sop: &Sop) -> Vec<String> {
        sop.list_profiles("encrypt")
    }

    fn produce(&self, pgp: &SopWithProfile) -> SopResult<Data> {
        pgp.sop()
            .encrypt()
            .with_password(crate::tests::PASSWORD)
            .plaintext(crate::tests::MESSAGE)
    }

    fn consume(&self,
               _producer: &Sop,
               consumer: &Sop,
               artifact: &Data)
               -> SopResult<(Data, Vec<Verification>)> {
        consumer.sop()
            .decrypt()
            .with_password(crate::tests::PASSWORD)
            .ciphertext(artifact)
    }

    fn check_consumer(&self, _artifact: &Data,
                      (plaintext, _sigs): &(Data, Vec<Verification>))
                      -> Result<()> {
        ensure!(&plaintext[..] == crate::tests::MESSAGE,
            CheckError::HardFailure(format!("Expected {:?}, got {:?}",
                                            crate::tests::MESSAGE, plaintext)));
        Ok(())
    }

    fn expectation(&self, _profile: &String) -> Option<Expectation> {
        Some(Ok("Interoperability concern.".into()))
    }
}
