use anyhow::ensure;

use crate::{
    Data,
    Result,
    sop::{Sop, SopResult, Verification},
    tests::{
        Expectation,
        TestMatrix,
        ConsumptionTest,
        CheckError,
    },
};

/// Tests support for Argon2 in the context of symmetrically
/// encrypted messages.
pub struct Argon2 {
}

impl Argon2 {
    pub fn new() -> Result<Argon2> {
        Ok(Argon2 {
        })
    }

    /// Message used in the RFC9580 test vectors.
    const MESSAGE: &'static [u8] = b"Hello, world!";

    /// Password used in the RFC9580 test vectors.
    const PASSWORD: &'static str = "password";
}

impl crate::plan::Runnable<TestMatrix> for Argon2 {
    fn title(&self) -> String {
        "Argon2 encrypted SEIPDv1 message".into()
    }

    fn description(&self) -> String {
        format!(
            "Tests support for Argon2 in the context of symmetrically encrypted messages.  \
             These are the test vectors from Appendix A.8 of RFC9580.  \
             The plaintext is {:?}.  \
             The password is {:?}.",
            std::str::from_utf8(Self::MESSAGE).unwrap(),
            Self::PASSWORD)
    }

    fn tags(&self) -> std::collections::BTreeSet<&'static str> {
        ["v6"].iter().cloned().collect()
    }

    fn run(&self, matrix: TestMatrix, implementations: &[crate::Sop])
           -> Result<TestMatrix> {
        ConsumptionTest::run(self, matrix, implementations)
    }
}

impl ConsumptionTest<Data, (Data, Vec<Verification>)> for Argon2 {
    fn produce(&self) -> Result<Vec<(String, Data, Option<Expectation>)>> {
        Ok(vec![
            ("AES-128".into(),
             crate::data::file("v6/test-vectors/v4skesk-argon2-aes128.pgp").unwrap().into(),
             Some(Ok("Interoperability concern".into()))),
            ("AES-192".into(),
             crate::data::file("v6/test-vectors/v4skesk-argon2-aes192.pgp").unwrap().into(),
             Some(Ok("Interoperability concern".into()))),
            ("AES-256".into(),
             crate::data::file("v6/test-vectors/v4skesk-argon2-aes256.pgp").unwrap().into(),
             Some(Ok("Interoperability concern".into()))),
        ])
    }

    fn consume(&self, pgp: &Sop, artifact: &Data)
               -> SopResult<(Data, Vec<Verification>)>
    {
        pgp.sop()
            .decrypt()
            .with_password(Self::PASSWORD)
            .ciphertext(artifact)
    }

    fn check_consumer(&self, _: &Data,
                      (artifact, _verifications): &(Data, Vec<Verification>),
                      _expextation: &Option<Expectation>)
                      -> Result<()>
    {
        ensure!(&artifact[..] == Self::MESSAGE,
            CheckError::HardFailure(format!("Expected {:?}, got {:?}",
                                            Self::MESSAGE, artifact)));
        Ok(())
    }
}
