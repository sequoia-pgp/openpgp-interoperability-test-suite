use anyhow::{bail, ensure, Context};

use sequoia_openpgp::{
    self as openpgp,
    cert::amalgamation::ValidAmalgamation,
    parse::Parse,
    serialize::SerializeInto,
    types::{
        AEADAlgorithm,
        Features,
        KeyFlags,
        SymmetricAlgorithm,
        Timestamp,
    },
};

use crate::{
    Data,
    Result,
    data,
    sop::{Sop, SopResult},
    tests::{
        CheckError,
        ConsumptionTest,
        Expectation,
        ListOf,
        MESSAGE,
        TestMatrix,
    },
};


/// Tests support for the production (i.e. encryption) of symmetric
/// encryption algorithms.
pub struct SymmetricEncryptionSupport {
    title: String,
    description: String,
    aead: Option<AEADAlgorithm>,
    features: Features,
    additional_certs: Vec<Data>,
}

impl SymmetricEncryptionSupport {
    pub fn new(title: &str, description: &str) -> Result<SymmetricEncryptionSupport> {
        Ok(SymmetricEncryptionSupport {
            title: title.into(),
            description: description.into(),
            aead: None,
            features: Features::empty().set_seipdv1(),
            additional_certs: Vec::new(),
        })
    }

    pub fn with_aead(title: &str, description: &str,
                       aead: openpgp::types::AEADAlgorithm,
                       features: Features)
                       -> Result<SymmetricEncryptionSupport>
    {
        Ok(SymmetricEncryptionSupport {
            title: title.into(),
            description: description.into(),
            aead: Some(aead),
            features: features,
            additional_certs: Vec::new(),
        })
    }

}

impl crate::plan::Runnable<TestMatrix> for SymmetricEncryptionSupport {
    fn title(&self) -> String {
        self.title.clone()
    }

    fn description(&self) -> String {
        self.description.clone()
    }

    fn run(&self, matrix: TestMatrix, implementations: &[crate::Sop])
           -> Result<TestMatrix> {
        ConsumptionTest::run(self, matrix, implementations)
    }
}

impl ConsumptionTest<(Data, SymmetricAlgorithm), Data>
    for SymmetricEncryptionSupport
{
    fn produce(&self)
               -> Result<Vec<(String,
                              (Data, SymmetricAlgorithm),
                              Option<Expectation>)>>
    {
        use SymmetricAlgorithm::*;
        use AEADAlgorithm::*;

        let cert =
            openpgp::Cert::from_bytes(data::certificate("bob-secret.pgp"))?;
        let mut t = Vec::new();

        for cipher in SymmetricAlgorithm::variants()
            .chain(std::iter::once(SymmetricAlgorithm::Unencrypted))
        {
            // Change the cipher preferences of CERT.
            let uid = cert.with_policy(crate::tests::P, None).unwrap()
                .primary_userid().unwrap();
            let mut builder = openpgp::packet::signature::SignatureBuilder::from(
                uid.binding_signature().clone())
                .set_signature_creation_time(Timestamp::now())?
                .set_features(self.features.clone())?;
            if self.features.supports_seipdv2() {
                if let Some(algo) = self.aead {
                    builder = builder.set_preferred_aead_ciphersuites(vec![(cipher, algo)])?;
                }
            } else {
                builder = builder.set_preferred_symmetric_algorithms(vec![cipher])?;
                #[allow(deprecated)]
                if let Some(algo) = self.aead {
                    builder = builder.set_preferred_aead_algorithms(vec![algo])?;
                }
            }
            let mut primary_keypair =
                cert.primary_key()
                .key().clone().parts_into_secret()?.into_keypair()?;
            let new_sig = uid.bind(&mut primary_keypair, &cert, builder)?;
            let cert = cert.clone().insert_packets(Some(new_sig))?;
            let cert = cert.armored().to_vec()?;

            let expectation = match (cipher, self.aead, self.features.supports_seipdv2()) {
                (IDEA | TripleDES | CAST5 | Blowfish, Some(_), _) => // EAX, OCB and GCM can only use block ciphers with 16-octet blocks in OpenPGP.
                    Some(Err("Algorithm can't be used with AEAD.".into())),
                (IDEA | TripleDES | CAST5, _, _) => // Deprecated in the crypto refresh.
                    Some(Err("Implementations MUST NOT encrypt data with this algorithm.".into())),
                (AES128, None, _) =>
                    Some(Ok("Implementations MUST implement AES-128.".into())),
                (AES128, Some(OCB), true) =>
                    Some(Ok("Implementations MUST implement AES-128 and OCB.".into())),
                (AES256, None, _) =>
                    Some(Ok("Implementations SHOULD implement AES-256.".into())),
                (AES256, Some(OCB), true) =>
                    Some(Ok("Implementations SHOULD implement AES-256 and MUST implement OCB.".into())),
                (Unencrypted, _, _) =>
                    Some(Err("\"Unencrypted cipher\" MUST NOT be used.".into())),
                _ => None,
            };

            t.push((
                format!("{:?}", cipher),
                (cert.into(), cipher),
                expectation,
            ));
        }

        Ok(t)
    }

    fn consume(&self,
               pgp: &Sop,
               (cert, _cipher):
                   &(Data, SymmetricAlgorithm))
               -> SopResult<Data>
    {
        pgp.sop()
            .encrypt()
            .cert(cert)
            .plaintext(MESSAGE)
    }

    fn check_consumer(&self,
                      (_cert, cipher):
                          &(Data, SymmetricAlgorithm),
                      ciphertext: &Data,
                      _expectation: &Option<Expectation>)
                      -> Result<()>
    {
        let pp = openpgp::PacketPile::from_bytes(&ciphertext)
            .context("Produced data is malformed")?;

        // Check that the number of PKESK packets matches the number of
        // certificates.
        let pkesk_count = pp.children()
            .filter(|p| p.tag() == openpgp::packet::Tag::PKESK)
            .count();
        ensure!(pkesk_count == 1 + self.additional_certs.len(),
            CheckError::HardFailure(format!(
                "Expected {:?} PKESK packets, found {:?}",
                1 + self.additional_certs.len(), pkesk_count)));

        // Check that the PKESK packets are of the correct version.
        for pkesk in pp.children()
            .filter(|p| p.tag() == openpgp::packet::Tag::PKESK) {
            let version = pkesk.version().unwrap();
            let expected_version = if self.features.supports_seipdv2() { 6 } else { 3 };
            ensure!(version == expected_version,
                CheckError::SoftFailure(format!(
                    "Expected version {:?} PKESK packet, found version {:?}",
                    expected_version, version)));
        }

        // Check whether the implementation used the algorithm(s) that
        // were requested, if any. Note that using a different algorithm
        // is only a "soft failure", as the artifact may still be valid
        // (but using a different algorithm), and the test expectations
        // indicate whether the algorithm is expected to be implemented.

        if let Some(aead_algo) = self.aead {
            match pp.children().last() {
                Some(openpgp::Packet::SEIP(openpgp::packet::SEIP::V2(s))) => {
                    ensure!(self.features.supports_seipdv2(),
                        CheckError::SoftFailure(
                            "Producer used SEIPDv2 packet when support was not signalled".into()));

                    ensure!((s.symmetric_algo(), s.aead()) == (*cipher, aead_algo),
                        CheckError::SoftFailure(format!(
                            "Producer did not use {:?}-{:?}, but {:?}-{:?}",
                            cipher, aead_algo, s.symmetric_algo(), s.aead())));
                },
                #[allow(deprecated)]
                Some(openpgp::Packet::AED(a)) => {
                    ensure!(self.features.supports_aead(),
                        CheckError::SoftFailure(
                            "Producer used AEAD Encrypted Data packet when support was not signalled".into()));

                    ensure!((a.symmetric_algo(), a.aead()) == (*cipher, aead_algo),
                        CheckError::SoftFailure(format!(
                            "Producer did not use {:?}-{:?}, but {:?}-{:?}",
                            cipher, aead_algo, a.symmetric_algo(), a.aead())));
                },
                Some(p) => bail!(CheckError::SoftFailure(format!(
                    "Producer did not use AEAD, found {} packet", p.tag()))),
                None => bail!(CheckError::HardFailure("No packet emitted".into())),
            }
        } else {
            // Check that the producer used CIPHER.
            let cert =
                openpgp::Cert::from_bytes(data::certificate("bob-secret.pgp"))?;
            let mode = KeyFlags::empty()
                .set_storage_encryption().set_transport_encryption();

            let mut ok = false;
            let mut algos = Vec::new();
            'search: for p in pp.children() {
                match p {
                    openpgp::Packet::PKESK(p) => {
                        for ka in cert.keys().with_policy(crate::tests::P, None).secret()
                            .key_flags(mode.clone())
                        {
                            let mut keypair = ka.key().clone().into_keypair()?;
                            if let Some((Some(a), _)) = p.decrypt(&mut keypair, None) {
                                if a == *cipher {
                                    ok = true;
                                    break 'search;
                                }
                                algos.push(a);
                            }
                        }
                    },
                    openpgp::Packet::SEIP(openpgp::packet::SEIP::V2(_)) => {
                        bail!(CheckError::SoftFailure(
                                "Producer used SEIPDv2 packet when support was not signalled".into()));
                    },
                    openpgp::Packet::AED(_) => {
                        bail!(CheckError::SoftFailure(
                                "Producer used AEAD Encrypted Data packet when support was not signalled".into()));
                    },
                    _ => ()
                }
            }

            ensure!(ok,
                CheckError::SoftFailure(format!(
                    "Producer did not use {:?}, but {}", cipher, ListOf(&algos))));
        }

        Ok(())
    }

}
