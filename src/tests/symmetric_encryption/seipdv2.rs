use anyhow::ensure;

use crate::{
    Data,
    Result,
    sop::{Sop, SopResult, Verification},
    tests::{
        Expectation,
        TestMatrix,
        ConsumptionTest,
        CheckError,
    },
};

/// Tests support for SKESKv6 & SEIPDv2 encrypted messages.
pub struct SEIPDv2 {
}

impl SEIPDv2 {
    pub fn new() -> Result<SEIPDv2> {
        Ok(SEIPDv2 {
        })
    }

    /// Message used in the RFC9580 test vectors.
    const MESSAGE: &'static [u8] = b"Hello, world!";

    /// Password used in the RFC9580 test vectors.
    const PASSWORD: &'static str = "password";
}

impl crate::plan::Runnable<TestMatrix> for SEIPDv2 {
    fn title(&self) -> String {
        "SKESKv6 & SEIPDv2 encrypted message".into()
    }

    fn description(&self) -> String {
        format!(
            "Tests support for SKESKv6 and SEIPDv2.  \
             These are the test vectors from Appendix A.9 through A.11 of RFC9580.  \
             The plaintext is {:?}.  \
             The password is {:?}.",
            std::str::from_utf8(Self::MESSAGE).unwrap(),
            Self::PASSWORD)
    }

    fn tags(&self) -> std::collections::BTreeSet<&'static str> {
        ["v6"].iter().cloned().collect()
    }

    fn run(&self, matrix: TestMatrix, implementations: &[crate::Sop])
           -> Result<TestMatrix> {
        ConsumptionTest::run(self, matrix, implementations)
    }
}

impl ConsumptionTest<Data, (Data, Vec<Verification>)> for SEIPDv2 {
    fn produce(&self) -> Result<Vec<(String, Data, Option<Expectation>)>> {
        Ok(vec![
            ("AES128-EAX".into(),
             crate::data::file("v6/test-vectors/v6skesk-aes128-eax.pgp").unwrap().into(),
             None),
            ("AES128-OCB".into(),
             crate::data::file("v6/test-vectors/v6skesk-aes128-ocb.pgp").unwrap().into(),
             Some(Ok("Implementations MUST implement OCB according to RFC9580.".into()))),
            ("AES128-GCM".into(),
             crate::data::file("v6/test-vectors/v6skesk-aes128-gcm.pgp").unwrap().into(),
             None),
        ])
    }

    fn consume(&self, pgp: &Sop, artifact: &Data)
               -> SopResult<(Data, Vec<Verification>)>
    {
        pgp.sop()
            .decrypt()
            .with_password(Self::PASSWORD)
            .ciphertext(artifact)
    }

    fn check_consumer(&self, _: &Data,
                      (artifact, _verifications): &(Data, Vec<Verification>),
                      _expextation: &Option<Expectation>)
                      -> Result<()>
    {
        ensure!(&artifact[..] == Self::MESSAGE,
            CheckError::HardFailure(format!("Expected {:?}, got {:?}",
                                            Self::MESSAGE, artifact)));
        Ok(())
    }
}
