use anyhow::ensure;

use sequoia_openpgp as openpgp;
use openpgp::types::AEADAlgorithm;
use crate::{
    sop::{Sop, SopResult, Verification}, tests::{
        CheckError, ConsumptionTest, Expectation, TestMatrix, MESSAGE, PASSWORD
    },
    Data,
    data,
    Result
};

enum S2KUsage {
    AEAD(AEADAlgorithm),
    CFB,
    MalleableCFB,
    MalleableCFBWithLength,
    LegacyCFB,
}

impl std::fmt::Display for S2KUsage {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        use self::S2KUsage::*;
        match self {
            AEAD(algorithm) => write!(f, "AEAD ({})", algorithm),
            CFB => write!(f, "CFB"),
            MalleableCFB => write!(f, "MalleableCFB"),
            MalleableCFBWithLength => write!(f, "MalleableCFB (with length octet)"),
            LegacyCFB => write!(f, "LegacyCFB"),
        }
    }
}

impl std::fmt::Debug for S2KUsage {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        use self::S2KUsage::*;
        match self {
            AEAD(algorithm) => {
                let mut str = format!("{:?}", algorithm);
                str.make_ascii_lowercase();
                write!(f, "253-{}", str)
            },
            CFB => write!(f, "254"),
            MalleableCFB => write!(f, "255"),
            MalleableCFBWithLength => write!(f, "255-with-length"),
            LegacyCFB => write!(f, "implicit"),
        }
    }
}

enum S2KType {
    Argon2,
    Iterated,
    Salted,
    Simple,
    MD5,
}

impl std::fmt::Display for S2KType {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        use self::S2KType::*;
        write!(f, "{}", match self {
            Argon2 => "Argon2 S2K",
            Iterated => "Iterated and Salted S2K",
            Salted => "Salted S2K",
            Simple => "Simple S2K",
            MD5 => "MD5 (no S2K specifier)",
        })
    }
}

impl std::fmt::Debug for S2KType {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        use self::S2KType::*;
        write!(f, "{}", match self {
            Argon2 => "argon2",
            Iterated => "iterated",
            Salted => "salted",
            Simple => "simple",
            MD5 => "md5",
        })
    }
}

/// Tests support for encrypted keys using various encryption methods.
pub struct EncryptedKeys {
}

impl EncryptedKeys {
    pub fn new() -> Result<EncryptedKeys> {
        Ok(EncryptedKeys {
        })
    }
}

impl crate::plan::Runnable<TestMatrix> for EncryptedKeys {
    fn title(&self) -> String {
        "Encrypted keys".into()
    }

    fn description(&self) -> String {
        format!(
            "Tests support for decrypting keys using various encryption methods. \
             Tries to decrypt a message using an encrypted key. \
             All keys are encrypted using AES256, except for the LegacyCFB keys, \
             which are encrypted using AES128. \
             The password is {:?} for all keys.",
            crate::tests::PASSWORD)
    }

    fn artifacts(&self) -> Vec<(String, Data)> {
        vec![
            ("Message encrypted with v6 key".into(), data::file("encrypted-key/v6-message.pgp").unwrap().into()),
            ("Message encrypted with v5 key".into(), data::file("encrypted-key/v5-message.pgp").unwrap().into()),
            ("Message encrypted with v4 key".into(), data::file("encrypted-key/v4-message.pgp").unwrap().into()),
        ]
    }

    fn tags(&self) -> std::collections::BTreeSet<&'static str> {
        ["v6"].iter().cloned().collect()
    }

    fn run(&self, matrix: TestMatrix, implementations: &[crate::Sop])
           -> Result<TestMatrix> {
        ConsumptionTest::run(self, matrix, implementations)
    }
}

impl ConsumptionTest<(Data, Data), (Data, Vec<Verification>)> for EncryptedKeys {
    fn produce(&self) -> Result<Vec<(String, (Data, Data), Option<Expectation>)>> {
        let make_test = |version, s2k_usage, s2k_type, expectation| {
            (
                format!("V{version} key encrypted with {s2k_usage}\nKey derived using {s2k_type}").into(),
                (
                    Data::from(data::file(&format!("encrypted-key/v{version}-key-s2k-usage-{s2k_usage:?}-{s2k_type:?}.pgp")).unwrap()),
                    Data::from(data::file(&format!("encrypted-key/v{version}-message.pgp")).unwrap())
                ),
                expectation
            )
        };
        use { S2KUsage::*, AEADAlgorithm::*, S2KType::* };
        Ok(vec![
            make_test(6, AEAD(EAX), Argon2, None),
            make_test(6, AEAD(OCB), Argon2, Some(Ok("Implementations MUST implement OCB and SHOULD implement Argon2 according to RFC9580.".into()))),
            make_test(6, AEAD(GCM), Argon2, None),
            make_test(6, AEAD(OCB), Iterated, Some(Ok("Interoperability concern.".into()))),
            make_test(6, CFB, Argon2, Some(Err("MUST reject Argon2 without AEAD according to RFC9580.".into()))),
            make_test(6, CFB, Iterated, Some(Ok("Interoperability concern.".into()))),
            make_test(6, CFB, Salted, None),
            make_test(6, CFB, Simple, Some(Err("Should be used only for reading in backwards compatibility mode.".into()))),
            make_test(6, MalleableCFB, Iterated, Some(Err("Version 6 secret keys using this format MUST be rejected according to RFC9580.".into()))),
            make_test(6, MalleableCFBWithLength, Iterated, Some(Err("Version 6 secret keys using this format MUST be rejected according to RFC9580.".into()))),
            make_test(6, LegacyCFB, MD5, Some(Err("Version 6 secret keys using this format MUST be rejected according to RFC9580.".into()))),
            make_test(5, AEAD(OCB), Iterated, None),
            make_test(5, CFB, Iterated, None),
            make_test(4, AEAD(OCB), Argon2, Some(Ok("Implementations MUST implement OCB and SHOULD implement Argon2 according to RFC9580.".into()))),
            make_test(4, AEAD(OCB), Iterated, Some(Ok("Interoperability concern.".into()))),
            make_test(4, CFB, Argon2, Some(Err("MUST reject Argon2 without AEAD according to RFC9580.".into()))),
            make_test(4, CFB, Iterated, Some(Ok("Interoperability concern.".into()))),
            make_test(4, CFB, Salted, None),
            make_test(4, CFB, Simple, None),
            make_test(4, MalleableCFB, Iterated, None),
            make_test(4, LegacyCFB, MD5, None),
        ])
    }

    fn consume(&self, pgp: &Sop, (key, message): &(Data, Data))
               -> SopResult<(Data, Vec<Verification>)>
    {
        pgp.sop()
            .decrypt()
            .key(key)
            .with_key_password(PASSWORD)
            .ciphertext(message)
    }

    fn check_consumer(&self, _: &(Data, Data),
                      (artifact, _verifications): &(Data, Vec<Verification>),
                      _expectation: &Option<Expectation>)
                      -> Result<()>
    {
        ensure!(&artifact[..] == MESSAGE,
            CheckError::HardFailure(format!("Expected {:?}, got {:?}",
                                            MESSAGE, artifact)));
        Ok(())
    }
}
