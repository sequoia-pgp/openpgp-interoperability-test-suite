use std::io::Write;

use anyhow::ensure;

use sequoia_openpgp as openpgp;
use openpgp::{
    Packet,
    PacketPile,
    crypto::hash::{Hash, Digest},
    packet::{Any, Signature},
    parse::Parse,
    serialize::{
        MarshalInto,
        stream::*,
    },
};

use crate::{
    Data,
    Result,
    data,
    sop::{Sop, SopResult, Verification},
    tests::{
        ConsumptionTest,
        Expectation,
        TestMatrix,
        CheckError,
    },
};

fn corrupt_hash_digest(p: Packet) -> Result<Packet> {
    if let Packet::Signature(sig) = &p {
        let mut buf = p.to_vec()?;
        let buf_len = buf.len();
        let mpi_len = sig.mpis().serialized_len();
        buf[buf_len - mpi_len - 2] ^= 0xff;
        buf[buf_len - mpi_len - 1] ^= 0xff;
        Packet::from_bytes(&buf)
    } else {
        Ok(p)
    }
}

/// Explores whether implementations set the digest prefix correctly.
pub struct Production {
}

impl Production {
    pub fn new() -> Result<Production> {
        Ok(Production {})
    }
}

impl crate::plan::Runnable<TestMatrix> for Production {
    fn title(&self) -> String {
        "Signature digest prefix, production".into()
    }

    fn description(&self) -> String {
        "<p>Explores whether implementations set the digest prefix correctly. \
         To that end, we ask implementations to make a detached signature \
         and check whether the digest prefix is correct.</p>"
            .into()
    }

    fn run(&self, matrix: TestMatrix, implementations: &[crate::Sop])
           -> Result<TestMatrix> {
        ConsumptionTest::run(self, matrix, implementations)
    }
}

impl ConsumptionTest<Data, Data> for Production {
    fn produce(&self)
               -> Result<Vec<(String, Data, Option<Expectation>)>>
    {
        Ok(vec![
            ("Checking produced prefix".into(),
             data::certificate("bob-secret.pgp").into(),
             Some(Ok("Digest prefix MUST be set correctly".into()))),
        ])
    }

    fn consume(&self, pgp: &Sop, artifact: &Data)
               -> SopResult<Data>
    {
        pgp.sop()
            .sign()
            .key(artifact)
            .data(crate::tests::MESSAGE)
    }

    fn check_consumer(&self,
                      _: &Data,
                      result: &Data,
                      _: &Option<Expectation>)
                      -> Result<()> {
        let p = Packet::from_bytes(result)?;
        let s: Signature = p.downcast()
            .map_err(|p| anyhow::anyhow!("Expected a signature packet, \
                                          got {:?}", p))?;
        let mut h = s.hash_algo().context()?;
        h.update(crate::tests::MESSAGE);
        s.hash(&mut h);
        let d = h.into_digest()?;
        ensure!(s.digest_prefix() == &d[..2],
                CheckError::HardFailure(format!(
                    "Digest prefix mismatch. Got: {:?}, want: {:?}.",
                    s.digest_prefix(), &d[..2])));

        Ok(())
    }
}

/// Explores whether implementations consider signatures with invalid
/// digest prefixes invalid.
pub struct Consumption {
    bob_corrupted: Data,
}

impl Consumption {
    pub fn new() -> Result<Consumption> {
        let bob_corrupted =
            PacketPile::from_bytes(data::certificate("bob.pgp"))?
            .into_children()
            .map(|p| corrupt_hash_digest(p).unwrap())
            .collect::<PacketPile>()
            .to_vec()?
            .into();
        Ok(Consumption {
            bob_corrupted,
        })
    }
}

impl crate::plan::Runnable<TestMatrix> for Consumption {
    fn title(&self) -> String {
        "Signature digest prefix, consumption".into()
    }

    fn description(&self) -> String {
        "<p>Explores whether implementations consider signatures with \
         invalid digest prefixes invalid.  There are two checks:</p>\
         <ol><li>We make a detached signature, corrupt the digest prefix, \
         and ask implementations to verify it.</li>\
         <li>We ask implementations to verify a signature using a cert where \
         we corrupted all digest prefixes in the binding signatures.</li></ol>"
            .into()
    }

    fn artifacts(&self) -> Vec<(String, Data)> {
        vec![
            ("Bob's cert".into(), data::certificate("bob.pgp").into()),
            ("Bob's cert with corrupted digest prefixes".into(),
             self.bob_corrupted.clone()),
        ]
    }

    fn tags(&self) -> std::collections::BTreeSet<&'static str> {
        ["verify-only"].iter().cloned().collect()
    }

    fn run(&self, matrix: TestMatrix, implementations: &[crate::Sop])
           -> Result<TestMatrix> {
        ConsumptionTest::run(self, matrix, implementations)
    }
}

enum Variant {
    CheckCorruptedSig,
    CheckCorruptedCert,
}
use Variant::*;

impl ConsumptionTest<(Data, Variant), (Data, Vec<Verification>)> for Consumption {
    fn produce(&self)
               -> Result<Vec<(String, (Data, Variant), Option<Expectation>)>>
    {
        let cert =
            openpgp::Cert::from_bytes(data::certificate("bob-secret.pgp"))?;
        let primary = cert.primary_key().key().clone();
        let primary_signer =
            primary.clone().parts_into_secret()?.into_keypair()?;

        let mut sig = Vec::new();
        let message = Message::new(&mut sig);
        let mut signer = Signer::new(message, primary_signer)
            .detached()
            .build()?;
        signer.write_all(crate::tests::MESSAGE)?;
        signer.finalize()?;
        let sig = Packet::from_bytes(&sig)?;

        Ok(vec![
            ("Sig w/corrupted prefix".into(),
             (corrupt_hash_digest(sig.clone())?.to_vec()?.into(), CheckCorruptedSig),
             None),
            ("Cert w/corrupted prefixes".into(),
             (sig.to_vec()?.into(), CheckCorruptedCert),
             None),
        ])
    }

    fn consume(&self, pgp: &Sop, (artifact, variant): &(Data, Variant))
               -> SopResult<(Data, Vec<Verification>)>
    {
        match variant {
            CheckCorruptedSig => {
                pgp.sop()
                    .verify()
                    .cert(data::certificate("bob.pgp"))
                    .signatures(artifact)
                    .data(crate::tests::MESSAGE)
                    .map(|verifications| (Default::default(), verifications))
            },
            CheckCorruptedCert => {
                pgp.sop()
                    .verify()
                    .cert(&self.bob_corrupted)
                    .signatures(artifact)
                    .data(crate::tests::MESSAGE)
                    .map(|verifications| (Default::default(), verifications))
            },
        }
    }

    fn check_consumer(&self,
                      (_, variant): &(Data, Variant),
                      (_, verifications): &(Data, Vec<Verification>),
                      _: &Option<Expectation>)
                      -> Result<()> {
        match variant {
            CheckCorruptedSig => {
                let _ = verifications; // XXX
                Ok(())
            },
            CheckCorruptedCert => {
                let _ = verifications; // XXX
                Ok(())
            },
        }
    }
}

