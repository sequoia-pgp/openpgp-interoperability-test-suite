use std::io::Write;

use sequoia_openpgp as openpgp;
use openpgp::types::CompressionAlgorithm;
use openpgp::parse::Parse;

use crate::{
    Data,
    Result,
    data,
    sop::{Sop, SopResult, Verification},
    tests::TestPlan,
    tests::{
        Expectation,
        TestMatrix,
        ConsumptionTest,
    },
};

/// Tests support for compression algorithms.
struct CompressionSupport {
}

impl CompressionSupport {
    pub fn new() -> Result<CompressionSupport> {
        Ok(CompressionSupport {
        })
    }
}

impl crate::plan::Runnable<TestMatrix> for CompressionSupport {
    fn title(&self) -> String {
        "Compression Algorithm support".into()
    }

    fn description(&self) -> String {
        "This tests support for the different compression algorithms \
         using Sequoia to generate the artifacts.".into()
    }

    fn artifacts(&self) -> Vec<(String, Data)> {
        vec![("Key".into(), data::certificate("bob-secret.pgp").into())]
    }

    fn run(&self, matrix: TestMatrix, implementations: &[crate::Sop])
           -> Result<TestMatrix> {
        ConsumptionTest::run(self, matrix, implementations)
    }
}

impl ConsumptionTest<Data, (Data, Vec<Verification>)> for CompressionSupport {
    fn produce(&self) -> Result<Vec<(String, Data, Option<Expectation>)>> {
        use openpgp::serialize::stream::Encryptor2 as Encryptor;
        use openpgp::serialize::stream::*;

        let cert =
            openpgp::Cert::from_bytes(data::certificate("bob.pgp"))?;
        let mut t = Vec::new();

        use CompressionAlgorithm::*;
        for &c in &[Uncompressed, Zip, Zlib, BZip2] {
            let expectation = match c {
                Uncompressed =>
                    None,
                Zip =>
                    Some(Ok("SHOULD be able to decompress ZIP.".into())),
                Zlib =>
                    Some(Ok("Zlib SHOULD be supported.".into())),
                _ =>
                    None,
            };

            let recipient: Recipient =
                cert.keys().with_policy(super::P, None)
                .for_transport_encryption()
                .nth(0).unwrap().key().into();

            let mut b = Vec::new();

            {
                let stack = Message::new(&mut b);
                let stack = Armorer::new(stack).build()?;
                let stack =
                    Encryptor::for_recipients(stack, vec![recipient]).build()?;
                let stack = Compressor::new(stack).algo(c).build()?;
                let mut stack = LiteralWriter::new(stack).build()?;

                write!(stack, "Compressed using {}.", c)?;
                stack.finalize()?;
            }

            t.push((c.to_string(), b.into(), expectation));
        }

        Ok(t)
    }

    fn consume(&self, pgp: &Sop, artifact: &Data)
               -> SopResult<(Data, Vec<Verification>)> {
        pgp.sop()
            .decrypt()
            .key(data::certificate("bob-secret.pgp"))
            .ciphertext(artifact)
    }
}

pub fn schedule(plan: &mut TestPlan) -> Result<()> {
    plan.add_section("Compression Algorithms");
    plan.add(Box::new(CompressionSupport::new()?));
    Ok(())
}
