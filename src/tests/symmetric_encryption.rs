use std::io::Write;

use anyhow::Context;
use sequoia_openpgp as openpgp;
use openpgp::{
    PacketPile,
    packet,
    packet::prelude::*,
    parse::Parse,
    serialize::{Serialize, SerializeInto},
    types::{SymmetricAlgorithm, AEADAlgorithm},
};
use crate::{
    Data,
    Result,
    data,
    plan::Runnable,
    sop::{Sop, SopResult, Verification},
    tests::TestPlan,
    tests::{
        Expectation,
        TestMatrix,
        ConsumptionTest,
        asymmetric_encryption::EncryptDecryptRoundtrip,
    },
};

mod encryption_support;
mod password_interop;
mod s2ks;
mod argon2;
mod seipdv2;
mod sed;
mod encrypted_keys;

/// Tests support for the consumption (i.e. decryption) of symmetric
/// encryption algorithms.
struct SymmetricDecryptionSupport {
    title: String,
    description: String,
    aead: Option<AEADAlgorithm>,
}

impl SymmetricDecryptionSupport {
    pub fn new(title: &str, description: &str) -> Result<SymmetricDecryptionSupport> {
        Ok(SymmetricDecryptionSupport {
            title: title.into(),
            description: description.into(),
            aead: None,
        })
    }

    pub fn with_aead(title: &str, description: &str,
                     aead: AEADAlgorithm)
                     -> Result<SymmetricDecryptionSupport> {
        Ok(SymmetricDecryptionSupport {
            title: title.into(),
            description: description.into(),
            aead: Some(aead),
        })
    }

    fn fallback(recipient: &openpgp::serialize::stream::Recipient,
                cipher: SymmetricAlgorithm, aead: Option<AEADAlgorithm>)
                -> Result<Data>
    {
        data::file(&match aead {
            Some(aead_algo) => format!("messages/{:X}.{:?}-{:?}.pgp", recipient.keyid(), cipher, aead_algo),
            None => format!("messages/{:X}.{:?}.pgp", recipient.keyid(), cipher)
        }).map(Into::into).context(anyhow::anyhow!(
            "Unsupported symmetric algorithm: {:?}-{:?}", cipher, aead))
    }
}

impl crate::plan::Runnable<TestMatrix> for SymmetricDecryptionSupport {
    fn title(&self) -> String {
        self.title.clone()
    }

    fn description(&self) -> String {
        self.description.clone()
    }

    fn artifacts(&self) -> Vec<(String, Data)> {
        vec![("Key".into(), data::certificate("bob-secret.pgp").into())]
    }

    fn run(&self, matrix: TestMatrix, implementations: &[crate::Sop])
           -> Result<TestMatrix> {
        ConsumptionTest::run(self, matrix, implementations)
    }
}

impl ConsumptionTest<Data, (Data, Vec<Verification>)> for SymmetricDecryptionSupport {
    fn produce(&self) -> Result<Vec<(String, Data, Option<Expectation>)>> {
        use openpgp::serialize::stream::Encryptor2 as Encryptor;
        use openpgp::serialize::stream::*;

        let cert =
            openpgp::Cert::from_bytes(data::certificate("bob.pgp"))?;
        let mut t = Vec::new();

        for cipher in SymmetricAlgorithm::variants() {
            use SymmetricAlgorithm::*;
            use AEADAlgorithm::*;
            let expectation = match (cipher, self.aead) {
                (IDEA | TripleDES | CAST5 | Blowfish, Some(_)) => // EAX, OCB and GCM can only use block ciphers with 16-octet blocks in OpenPGP.
                    Some(Err("Algorithm can't be used with AEAD.".into())),
                (AES128, None) =>
                    Some(Ok("Implementations MUST implement AES-128.".into())),
                (AES128, Some(OCB)) =>
                    Some(Ok("Implementations MUST implement AES-128 and OCB.".into())),
                (AES256, None) =>
                    Some(Ok("Implementations SHOULD implement AES-256.".into())),
                (AES256, Some(OCB)) =>
                    Some(Ok("Implementations SHOULD implement AES-256 and MUST implement OCB.".into())),
                _ => None,
            };

            let mut b = Vec::new();

            let recipient: Recipient =
                cert.keys().with_policy(super::P, None)
                .for_transport_encryption()
                .nth(0).unwrap().key().into();
            let msg: Data = if let Some(aead_algo) = self.aead {
                format!("Encrypted using {:?}-{:?}.", cipher, aead_algo)
            } else {
                format!("Encrypted using {:?}.", cipher)
            }.into_bytes().into();
            let stack = Message::new(&mut b);
            let stack = Armorer::new(stack).build()?;
            let mut encryptor = Encryptor::for_recipients(stack, vec![recipient])
                .symmetric_algo(cipher);
            if let Some(aead_algo) = self.aead {
                encryptor = encryptor.aead_algo(aead_algo)
            }
            let result = match encryptor.build()
                .and_then(|stack| {
                    let mut stack = LiteralWriter::new(stack).build()?;
                    stack.write_all(&msg)?;
                    stack.finalize()
                })
            {
                Ok(_) => b.into(),
                Err(_) => {
                    let recipient: Recipient =
                        cert.keys().with_policy(super::P, None)
                        .for_transport_encryption()
                        .nth(0).unwrap().key().into();
                    // Cipher is not supported by Sequoia, look
                    // for a fallback.
                    let fallback = Self::fallback(&recipient, cipher, self.aead);
                    if fallback.is_err() {
                        // XXX: Missing fallback message.
                        continue;
                    }
                    fallback.unwrap()
                },
            };

            t.push(
                (format!("{:?}", cipher), result, expectation));
        }

        if self.aead == None {
            // Unencrypted SEIP packet.
            let recipient =
                cert.keys().with_policy(super::P, None)
                .for_transport_encryption()
                .nth(0).unwrap().key();
            let msg =
                "NOT ENCRYPTED".to_string().into_bytes().into_boxed_slice();

            let mut b = Vec::new();
            {
                let stack = Message::new(&mut b);
                let mut stack = Armorer::new(stack).build()?;

                // PKESK packet with a fake session key.
                let session_key = vec![0, 1, 2, 3, 4, 5, 6, 7,
                                       0, 1, 2, 3, 4, 5, 6, 7].into();
                let pkesk =
                    PKESK3::for_recipient(SymmetricAlgorithm::Unencrypted,
                                          &session_key, recipient)?;
                Packet::from(pkesk).serialize(&mut stack)?;

                // Fake a SEIP container and continue as usual.
                let mut stack = ArbitraryWriter::new(stack, Tag::SEIP)?;
                stack.write(&[1]).unwrap(); // SEIP Version.
                let mut stack = LiteralWriter::new(stack).build()?;
                stack.write_all(&msg)?;

                // Fake MDC packet.
                let mut stack = stack.finalize_one()?.unwrap();
                let sum = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9,
                           0, 1, 2, 3, 4, 5, 6, 7, 8, 9,];
                let mdc = MDC::new(sum.clone(), sum);
                Packet::from(mdc).serialize(&mut stack)?;

                stack.finalize()?;
            }

            t.push(
                ("Unencrypted".into(), b.into(),
                 Some(Err("\"Unencrypted cipher\" MUST NOT be used".into()))));
        }

        Ok(t)
    }

    fn consume(&self, pgp: &Sop, artifact: &Data)
               -> SopResult<(Data, Vec<Verification>)> {
        pgp.sop()
            .decrypt()
            .key(data::certificate("bob-secret.pgp"))
            .ciphertext(artifact)
    }
}

/// Tests support for symmetrically encrypted integrity protected packets.
struct SEIPSupport {
}

impl SEIPSupport {
    pub fn new() -> Result<SEIPSupport> {
        Ok(SEIPSupport {
        })
    }
}

impl crate::plan::Runnable<TestMatrix> for SEIPSupport {
    fn title(&self) -> String {
        "SEIP packet support".into()
    }

    fn description(&self) -> String {
        "This tests support for the Symmetrically Encrypted Integrity \
         Protected Data Packet (Tag 18) and verifies that modifications to \
         the ciphertext are detected.  To avoid creating a decryption \
         oracle, implementations must respond with a uniform error message \
         to tampering.".into()
    }

    fn artifacts(&self) -> Vec<(String, Data)> {
        vec![("Key".into(), data::certificate("bob-secret.pgp").into())]
    }

    fn run(&self, matrix: TestMatrix, implementations: &[crate::Sop])
           -> Result<TestMatrix> {
        ConsumptionTest::run(self, matrix, implementations)
    }
}

impl ConsumptionTest<Data, (Data, Vec<Verification>)> for SEIPSupport {
    fn produce(&self) -> Result<Vec<(String, Data, Option<Expectation>)>> {
        use openpgp::serialize::stream::Encryptor2 as Encryptor;
        use openpgp::serialize::stream::*;

        // The tests.
        let mut t = Vec::new();
        // Makes tests.
        let make =
            |test: &str, message: &[u8], expectation: Option<Expectation>|
            -> Result<(String, Data, Option<Expectation>)>
        {
            let mut buf = Vec::new();
            {
                use openpgp::armor;
                let mut w =
                    armor::Writer::new(&mut buf, armor::Kind::Message)?;
                w.write_all(message)?;
                w.finalize()?;
            }
            Ok((test.into(), buf.into(), expectation))
        };

        // Use the RSA key to increase compatibility.
        let cert =
            openpgp::Cert::from_bytes(data::certificate("bob.pgp"))?;
        let recipient: Recipient =
            cert.keys().with_policy(super::P, None)
            .for_transport_encryption()
            .nth(0).unwrap().key().into();

        let mut buf = Vec::new();
        let message = Message::new(&mut buf);
        let message = Encryptor::for_recipients(message, vec![recipient])
            .symmetric_algo(SymmetricAlgorithm::AES256)
            .build()?;
        let mut message = LiteralWriter::new(message).build()?;
        message.write_all(b"Encrypted using SEIP + MDC.")?;
        message.finalize()?;

        // The base case as-is.
        t.push(make("Base case", &buf,
                    Some(Ok("SEIP is a MUST according to RFC4880.".into())))?);

        // Shave off the MDC packet.
        let mut packets = PacketPile::from_bytes(&buf)?;
        if let Some(Packet::SEIP(packet::SEIP::V1(seip))) = packets.path_ref_mut(&[1]) {
            let tampered = if let Body::Unprocessed(ciphertext) = seip.body() {
                Body::Unprocessed(ciphertext[..ciphertext.len() - 22].into())
            } else {
                panic!("Unexpected packet body");
                // XXX: panic!("Unexpected packet body: {:?}", seip.body());
            };
            seip.set_body(tampered);
        } else {
            panic!("Unexpected packet at [1]: {:?}", packets.path_ref(&[1]));
        }
        t.push(make("Missing MDC", &packets.to_vec()?,
                    Some(Err("Missing MDC must abort processing.".into())))?);

        // Downgrade to SED packet.
        let mut packets = PacketPile::from_bytes(&buf)?;
        let mut sed = Unknown::new(Tag::SED,
                                   anyhow::anyhow!("SED not supported"));
        if let Some(Packet::SEIP(packet::SEIP::V1(seip))) = packets.path_ref(&[1]) {
            if let Body::Unprocessed(ciphertext) = seip.body() {
                sed.set_body(ciphertext[..ciphertext.len() - 22].into());
            } else {
                panic!("Unexpected packet body");
                // XXX: panic!("Unexpected packet body: {:?}", seip.body());
            }
        } else {
            panic!("Unexpected packet at [1]: {:?}", packets.path_ref(&[1]));
        }
        packets.replace(&[1], 1, vec![sed.into()])?;
        t.push(make("Downgrade to SED", &packets.to_vec()?,
                    Some(Err("Security concern: Downgrade must be prevented."
                             .into())))?);

        // Tamper with the literal data.
        let mut packets = PacketPile::from_bytes(&buf)?;
        if let Some(Packet::SEIP(packet::SEIP::V1(seip))) = packets.path_ref_mut(&[1]) {
            let tampered = if let Body::Unprocessed(ciphertext) = seip.body() {
                let mut body = ciphertext.clone();
                let l = body.len();
                body[l - 23] ^= 0x01;
                Body::Unprocessed(body)
            } else {
                panic!("Unexpected packet body");
                // XXX: panic!("Unexpected packet body: {:?}", seip.body());
            };
            seip.set_body(tampered);
        } else {
            panic!("Unexpected packet at [1]: {:?}", packets.path_ref(&[1]));
        }
        t.push(make("Tampered ciphertext", &packets.to_vec()?,
                    Some(Err("Security concern: Tampering must be prevented."
                             .into())))?);

        // Tamper with the MDC.
        let mut packets = PacketPile::from_bytes(&buf)?;
        if let Some(Packet::SEIP(packet::SEIP::V1(seip))) = packets.path_ref_mut(&[1]) {
            let tampered = if let Body::Unprocessed(ciphertext) = seip.body() {
                let mut body = ciphertext.clone();
                let l = body.len();
                body[l - 1] ^= 0x01;
                Body::Unprocessed(body)
            } else {
                panic!("Unexpected packet body");
                // XXX: panic!("Unexpected packet body: {:?}", seip.body());
            };
            seip.set_body(tampered);
        } else {
            panic!("Unexpected packet at [1]: {:?}", packets.path_ref(&[1]));
        }
        t.push(make("Tampered MDC", &packets.to_vec()?,
                    Some(Err("Security concern: Tampering must be prevented."
                             .into())))?);

        // Truncated MDC.
        let mut packets = PacketPile::from_bytes(&buf)?;
        if let Some(Packet::SEIP(packet::SEIP::V1(seip))) = packets.path_ref_mut(&[1]) {
            let tampered = if let Body::Unprocessed(ciphertext) = seip.body() {
                let mut body = ciphertext.clone();
                let l = body.len();
                body.truncate(l - 1);
                Body::Unprocessed(body)
            } else {
                panic!("Unexpected packet body");
                // XXX: panic!("Unexpected packet body: {:?}", seip.body());
            };
            seip.set_body(tampered);
        } else {
            panic!("Unexpected packet at [1]: {:?}", packets.path_ref(&[1]));
        }
        t.push(make("Truncated MDC", &packets.to_vec()?,
                    Some(Err("Security concern: Tampering must be prevented."
                             .into())))?);

        // MDC with bad CTB.
        let mut packets = PacketPile::from_bytes(&buf)?;
        if let Some(Packet::SEIP(packet::SEIP::V1(seip))) = packets.path_ref_mut(&[1]) {
            let tampered = if let Body::Unprocessed(ciphertext) = seip.body() {
                let mut body = ciphertext.clone();
                let l = body.len();
                body[l - 22] ^= 0x01;
                Body::Unprocessed(body)
            } else {
                panic!("Unexpected packet body");
                // XXX: panic!("Unexpected packet body: {:?}", seip.body());
            };
            seip.set_body(tampered);
        } else {
            panic!("Unexpected packet at [1]: {:?}", packets.path_ref(&[1]));
        }
        t.push(make("MDC with bad CTB", &packets.to_vec()?,
                    Some(Err("Security concern: Tampering must be prevented."
                             .into())))?);

        // MDC with bad length.
        let mut packets = PacketPile::from_bytes(&buf)?;
        if let Some(Packet::SEIP(packet::SEIP::V1(seip))) = packets.path_ref_mut(&[1]) {
            let tampered = if let Body::Unprocessed(ciphertext) = seip.body() {
                let mut body = ciphertext.clone();
                let l = body.len();
                body[l - 21] ^= 0x01;
                Body::Unprocessed(body)
            } else {
                panic!("Unexpected packet body");
                // XXX: panic!("Unexpected packet body: {:?}", seip.body());
            };
            seip.set_body(tampered);
        } else {
            panic!("Unexpected packet at [1]: {:?}", packets.path_ref(&[1]));
        }
        t.push(make("MDC with bad length", &packets.to_vec()?,
                    Some(Err("Security concern: Tampering must be prevented."
                             .into())))?);

        Ok(t)
    }

    fn consume(&self, pgp: &Sop, artifact: &Data)
               -> SopResult<(Data, Vec<Verification>)> {
        pgp.sop()
            .decrypt()
            .key(data::certificate("bob-secret.pgp"))
            .ciphertext(artifact)
    }
}

pub fn schedule(plan: &mut TestPlan) -> Result<()> {
    use openpgp::types::SymmetricAlgorithm::*;
    use openpgp::types::AEADAlgorithm::*;
    use openpgp::types::*;

    plan.add_section("Symmetric Encryption");
    plan.add(Box::new(SymmetricDecryptionSupport::new(
        &"Symmetric Encryption Algorithm support (SEIPDv1)",
        &"This tests support for the consumption (i.e. decryption) of \
         different symmetric encryption algorithms \
         using Sequoia to generate the artifacts."
    )?));
    for &aead_algo in &[EAX, OCB, GCM] {
        plan.add(
            SymmetricDecryptionSupport::with_aead(
                &format!("Symmetric Encryption Algorithm support, {aead_algo} (SEIPDv2)"),
                "This tests support for the consumption (i.e. decryption) of \
                 different symmetric encryption algorithms \
                 using Sequoia to generate the artifacts.",
                aead_algo)?
                .with_tags(&["v6"]));
    }

    plan.add(Box::new(encryption_support::SymmetricEncryptionSupport::new(
        &"Symmetric Encryption Algorithm support, production side (SEIPDv1)",
        "This tests support for the production (i.e. encryption) of \
         different symmetric encryption algorithms.  To that end, \
         the preferred symmetric algorithms are set to only include \
         the algorithm to be tested, and the features subpacket \
         is set to [SEIPDv1]."
    )?));
    for &aead_algo in &[EAX, OCB, GCM] {
        plan.add(
            encryption_support::SymmetricEncryptionSupport::with_aead(
                &format!("Symmetric Encryption Algorithm support, production side, {aead_algo} (SEIPDv2)"),
                "This tests support for the production (i.e. encryption) of \
                 different symmetric encryption algorithms.  To that end, \
                 the preferred AEAD ciphersuites are set to only include \
                 the algorithms to be tested, and the features subpacket \
                 is set to [SEIPDv2].",
                aead_algo,
                Features::empty().set_seipdv2())?
                .with_tags(&["v6"]));
    }
    #[allow(deprecated)]
    for &aead_algo in &[EAX, OCB] {
        plan.add(
            encryption_support::SymmetricEncryptionSupport::with_aead(
                &format!("Symmetric Encryption Algorithm support, production side, {aead_algo} (AEADED)"),
                "This tests support for the production (i.e. encryption) of \
                 different symmetric encryption algorithms.  To that end, \
                 the preferred symmetric and AEAD algorithm preferences \
                 are set to only include the algorithms to be tested, \
                 and the features subpacket is set to [AEAD].",
                aead_algo,
                Features::empty().set_aead())?
                .with_tags(&["v5"]));
    }

    for cipher in SymmetricAlgorithm::variants() {
        plan.add(Box::new(
            EncryptDecryptRoundtrip::with_cipher(
                &format!("Encrypt-Decrypt roundtrip with key 'Bob', {:?}",
                         cipher),
                &format!("Encrypt-Decrypt roundtrip using the 'Bob' key from \
                          draft-bre-openpgp-samples-00, modified with the \
                          symmetric algorithm preference [{:?}], \
                          and features subpacket [SEIPDv1].", cipher),
                openpgp::Cert::from_bytes(data::certificate("bob-secret.pgp"))?,
                crate::tests::MESSAGE.to_vec().into(), cipher, None,
                Features::empty().set_seipdv1())?));
    }

    for &aead_algo in &[EAX, OCB, GCM] {
        plan.add(
            EncryptDecryptRoundtrip::with_cipher(
                &format!("Encrypt-Decrypt roundtrip with key 'Bob', {:?} (SEIPDv2)",
                         aead_algo),
                &format!("Encrypt-Decrypt roundtrip using the 'Bob' key from \
                          draft-bre-openpgp-samples-00, modified with the \
                          AEAD ciphersuite algorithm preference [AES128-{:?}], \
                          and features subpacket [SEIPDv2].", aead_algo),
                openpgp::Cert::from_bytes(data::certificate("bob-secret.pgp"))?,
                crate::tests::MESSAGE.to_vec().into(), AES128,
                Some(aead_algo),
                Features::empty().set_seipdv2())?
                .with_tags(&["v6"]));
    }

    #[allow(deprecated)]
    for &aead_algo in &[EAX, OCB] {
        plan.add(
            EncryptDecryptRoundtrip::with_cipher(
                &format!("Encrypt-Decrypt roundtrip with key 'Bob', {:?} (AEADED)",
                         aead_algo),
                &format!("Encrypt-Decrypt roundtrip using the 'Bob' key from \
                          draft-bre-openpgp-samples-00, modified with the \
                          symmetric algorithm preference [AES128], \
                          AEAD algorithm preference [{:?}], \
                          and features subpacket [AEAD].", aead_algo),
                openpgp::Cert::from_bytes(data::certificate("bob-secret.pgp"))?,
                crate::tests::MESSAGE.to_vec().into(), AES128,
                Some(aead_algo),
                Features::empty().set_aead())?
                .with_tags(&["v5"]));
    }

    plan.add(Box::new(password_interop::PasswordEncryptionInterop::new()?));
    plan.add(Box::new(s2ks::S2KSupport::new()?));
    plan.add(Box::new(argon2::Argon2::new()?));
    plan.add(Box::new(SEIPSupport::new()?));
    plan.add(Box::new(seipdv2::SEIPDv2::new()?));
    plan.add(Box::new(sed::SED::new()?));
    plan.add(Box::new(encrypted_keys::EncryptedKeys::new()?));

    Ok(())
}
