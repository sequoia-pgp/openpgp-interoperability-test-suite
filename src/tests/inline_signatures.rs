use crate::{
    Result,
    data,
    plan::Runnable,
    tests::TestPlan,
};

mod roundtrip;

pub fn schedule(plan: &mut TestPlan) -> Result<()> {
    plan.add_section("Inline Signatures");
    plan.add(
        roundtrip::InlineSignVerifyRoundtrip::new(
            "Inline Sign-Verify roundtrip with minimal key from RFC9580",
            "Inline Sign-Verify roundtrip with minimal key from \
             Appendix A.3 of RFC9580.",
            data::file("v6/test-vectors/v6-minimal-secret.key").unwrap().into(),
            data::file("v6/test-vectors/v6-minimal-cert.key").unwrap().into(),
            None, None,
            Some(Ok("Interoperability concern.".into())))?
            .with_tags(&["v6"]));
    plan.add(
        roundtrip::InlineSignVerifyRoundtrip::new(
            "Inline Sign with minimal key from RFC9580 and key 'Alice', \
             verify with key from RFC9580",
            "Inline Sign-Verify roundtrip test with multiple keys: \
             the data is signed with the minimal key from Appendix A.3 \
             of RFC9580 and the 'Alice' key from draft-bre-openpgp-samples-00, \
             then verified using the key from RFC9580",
            data::file("v6/test-vectors/v6-minimal-secret.key").unwrap().into(),
            data::file("v6/test-vectors/v6-minimal-cert.key").unwrap().into(),
            None, None,
            Some(Ok("Interoperability concern.".into())))?
            .add_signer(data::certificate("alice-secret.pgp"))
            .with_tags(&["v6"]));
    plan.add(
        roundtrip::InlineSignVerifyRoundtrip::new(
            "Inline Sign with minimal key from RFC9580 and key 'Alice', \
             verify with key 'Alice'",
            "Inline Sign-Verify roundtrip test with multiple keys: \
             the data is signed with the minimal key from Appendix A.3 \
             of RFC9580 and the 'Alice' key from draft-bre-openpgp-samples-00, \
             then verified using the key from RFC9580",
            data::file("v6/test-vectors/v6-minimal-secret.key").unwrap(),
            data::certificate("alice.pgp"),
            None, None,
            Some(Ok("Interoperability concern.".into())))?
            .add_signer(data::certificate("alice-secret.pgp"))
            .with_tags(&["v6"]));
    plan.add(Box::new(
        roundtrip::InlineSignVerifyRoundtrip::new(
            "Inline Sign-Verify roundtrip with key 'Alice'",
            "Inline Sign-Verify roundtrip using the 'Alice' key from \
             draft-bre-openpgp-samples-00.",
            data::certificate("alice-secret.pgp"),
            data::certificate("alice.pgp"),
            None, None,
            Some(Ok("Interoperability concern.".into())))?));
    plan.add(Box::new(
        roundtrip::InlineSignVerifyRoundtrip::new(
            "Sign+Encrypt-Decrypt+Verify roundtrip with key 'Alice'",
            "The signature is created using the 'Alice' key from \
             draft-bre-openpgp-samples-00, and the message is \
             encrypted for 'Bob'.",
            data::certificate("alice-secret.pgp"),
            data::certificate("alice.pgp"),
            data::certificate("bob-secret.pgp"),
            data::certificate("bob.pgp"),
            Some(Ok("Interoperability concern.".into())))?));

    plan.add(Box::new(
        roundtrip::InlineSignVerifyRoundtrip::new(
            "Inline Sign-Verify roundtrip with key 'Bob'",
            "Inline Sign-Verify roundtrip using the 'Bob' key from \
             draft-bre-openpgp-samples-00.",
            data::certificate("bob-secret.pgp"),
            data::certificate("bob.pgp"),
            None, None,
            Some(Ok("Interoperability concern.".into())))?));
    plan.add(Box::new(
        roundtrip::InlineSignVerifyRoundtrip::new(
            "Sign+Encrypt-Decrypt+Verify roundtrip with key 'Bob'",
            "The signature is created using the 'Bob' key from \
             draft-bre-openpgp-samples-00, and the message is \
             encrypted for 'Bob'.",
            data::certificate("bob-secret.pgp"),
            data::certificate("bob.pgp"),
            data::certificate("bob-secret.pgp"),
            data::certificate("bob.pgp"),
            Some(Ok("Interoperability concern.".into())))?));

    plan.add(Box::new(
        roundtrip::InlineSignVerifyRoundtrip::new(
            "Sign-Verify roundtrip with keys 'Alice' and 'Bob'",
            "Sign-Verify roundtrip using both the 'Alice' and \
             the 'Bob' key from draft-bre-openpgp-samples-00.",
            data::certificate("alice-secret.pgp"),
            data::certificate("alice.pgp"),
            None, None,
            Some(Ok("Interoperability concern.".into())))?
            .add_signer(data::certificate("bob-secret.pgp"))
            .add_verifier(data::certificate("bob.pgp"))));

    plan.add(Box::new(
        roundtrip::InlineSignVerifyRoundtrip::new(
            "Sign+Encrypt-Decrypt+Verify roundtrip with keys 'Alice' and 'Bob'",
            "The signatures are created using both the 'Alice' and \
             the 'Bob' key from draft-bre-openpgp-samples-00, \
             and the message is encrypted for 'Bob'.",
            data::certificate("alice-secret.pgp"),
            data::certificate("alice.pgp"),
            data::certificate("bob-secret.pgp"),
            data::certificate("bob.pgp"),
            Some(Ok("Interoperability concern.".into())))?
            .add_signer(data::certificate("bob-secret.pgp"))
            .add_verifier(data::certificate("bob.pgp"))));

    plan.add(Box::new(
        roundtrip::InlineSignVerifyRoundtrip::new(
            "Inline Sign-Verify roundtrip with key 'Carol'",
            "Inline Sign-Verify roundtrip using the 'Carol' key from \
             draft-bre-openpgp-samples-00.",
            data::certificate("carol-secret.pgp"),
            data::certificate("carol.pgp"),
            None, None,
            Some(Err("Implementations MUST NOT sign or verify using DSA keys according to RFC9580.".into())))?));
    plan.add(Box::new(
        roundtrip::InlineSignVerifyRoundtrip::new(
            "Sign+Encrypt-Decrypt+Verify roundtrip with key 'Carol'",
            "The signature is created using the 'Carol' key from \
             draft-bre-openpgp-samples-00, and the message is \
             encrypted for 'Bob'.",
            data::certificate("carol-secret.pgp"),
            data::certificate("carol.pgp"),
            data::certificate("bob-secret.pgp"),
            data::certificate("bob.pgp"),
            Some(Err("Implementations MUST NOT sign or verify using DSA keys according to RFC9580.".into())))?));

    plan.add(
        roundtrip::InlineSignVerifyRoundtrip::new(
            "Inline Sign-Verify roundtrip with key 'John'",
            "This is an OpenPGP v3 key.",
            data::certificate("john-secret.pgp"),
            data::certificate("john.pgp"),
            None, None,
            None)?
            .with_tags(&["v3"]));
    plan.add(
        roundtrip::InlineSignVerifyRoundtrip::new(
            "Sign+Encrypt-Decrypt+Verify roundtrip with key 'John'",
            "The signature is created using the v3 'John' key,
             and the message is encrypted for 'Bob'.",
            data::certificate("john-secret.pgp"),
            data::certificate("john.pgp"),
            data::certificate("bob-secret.pgp"),
            data::certificate("bob.pgp"),
            None)?
            .with_tags(&["v3"]));

    plan.add(
        roundtrip::InlineSignVerifyRoundtrip::new(
            "Inline Sign-Verify roundtrip with key 'Emma'",
            "This is an OpenPGP v5 key.",
            data::file("v5/v5-sample-1-sec.asc").unwrap(),
            data::file("v5/v5-sample-1-pub.asc").unwrap(),
            None, None,
            None)?
            .with_tags(&["v5"]));
    plan.add(
        roundtrip::InlineSignVerifyRoundtrip::new(
            "Sign+Encrypt-Decrypt+Verify roundtrip with key 'Emma'",
            "The signature is created using the v5 'Emma' key,
             and the message is encrypted for 'Bob'.",
            data::file("v5/v5-sample-1-sec.asc").unwrap(),
            data::file("v5/v5-sample-1-pub.asc").unwrap(),
            data::certificate("bob-secret.pgp"),
            data::certificate("bob.pgp"),
            None)?
            .with_tags(&["v5"]));

    plan.add(
        roundtrip::InlineSignVerifyRoundtrip::new(
            "Inline Sign-Verify roundtrip with a PQC key",
            "Inline Sign-Verify roundtrip with a PQC key from \
             Appendix A.1 of draft-ietf-openpgp-pqc.",
            data::file("pqc/pqc-secret.pgp").unwrap().into(),
            data::file("pqc/pqc-public.pgp").unwrap().into(),
            None, None,
            Some(Ok("Interoperability concern.".into())))?
            .with_tags(&["v6", "pqc", "draft"]));
    plan.add(Box::new(
        roundtrip::InlineSignVerifyRoundtrip::new(
            "Sign+Encrypt-Decrypt+Verify roundtrip with a PQC key",
            "Sign+Encrypt-Decrypt+Verify roundtrip with a PQC key from \
             Appendix A.1 of draft-ietf-openpgp-pqc.",
            data::file("pqc/pqc-secret.pgp").unwrap(),
            data::file("pqc/pqc-public.pgp").unwrap(),
            data::file("pqc/pqc-secret.pgp").unwrap(),
            data::file("pqc/pqc-public.pgp").unwrap(),
            Some(Ok("Interoperability concern.".into())))?)
            .with_tags(&["v6", "pqc", "draft"]));

    plan.add(
        roundtrip::InlineSignVerifyRoundtrip::cleartext(
            "Cleartext Signature Sign-Verify roundtrip with minimal key from RFC9580",
            "Inline Sign-Verify roundtrip with minimal key from \
             Appendix A.3 of RFC9580.",
            data::file("v6/test-vectors/v6-minimal-secret.key").unwrap().into(),
            data::file("v6/test-vectors/v6-minimal-cert.key").unwrap().into(),
            Some(Ok("Interoperability concern.".into())))?
            .with_tags(&["v6"]));
    plan.add(
        roundtrip::InlineSignVerifyRoundtrip::cleartext(
            "Cleartext Sign with minimal key from RFC9580 and key 'Alice', \
             verify with key from RFC9580",
            "Inline Sign-Verify roundtrip test with multiple keys: \
             the data is signed with the minimal key from Appendix A.3 \
             of RFC9580 and the 'Alice' key from draft-bre-openpgp-samples-00, \
             then verified using the key from RFC9580",
            data::file("v6/test-vectors/v6-minimal-secret.key").unwrap().into(),
            data::file("v6/test-vectors/v6-minimal-cert.key").unwrap().into(),
            Some(Ok("Interoperability concern.".into())))?
            .add_signer(data::certificate("alice-secret.pgp"))
            .with_tags(&["v6"]));
    plan.add(
        roundtrip::InlineSignVerifyRoundtrip::cleartext(
            "Cleartext Sign with minimal key from RFC9580 and key 'Alice', \
             verify with key 'Alice'",
            "Inline Sign-Verify roundtrip test with multiple keys: \
             the data is signed with the minimal key from Appendix A.3 \
             of RFC9580 and the 'Alice' key from draft-bre-openpgp-samples-00, \
             then verified using the key from RFC9580",
            data::file("v6/test-vectors/v6-minimal-secret.key").unwrap(),
            data::certificate("alice.pgp"),
            Some(Ok("Interoperability concern.".into())))?
            .add_signer(data::certificate("alice-secret.pgp"))
            .with_tags(&["v6"]));

    plan.add(Box::new(
        roundtrip::InlineSignVerifyRoundtrip::cleartext(
            "Cleartext Signature Sign-Verify roundtrip with key 'Bob'",
            "Cleartext Signature Sign-Verify roundtrip using the 'Bob' key from \
             draft-bre-openpgp-samples-00.",
            data::certificate("bob-secret.pgp"),
            data::certificate("bob.pgp"),
            Some(Ok("Interoperability concern.".into())))?));

    plan.add(
        roundtrip::InlineSignVerifyRoundtrip::cleartext(
            "Cleartext Signature Sign-Verify roundtrip with a PQC key",
            "Cleartext Signature Sign-Verify roundtrip with a PQC key from \
             Appendix A.1 of draft-ietf-openpgp-pqc.",
            data::file("pqc/pqc-secret.pgp").unwrap().into(),
            data::file("pqc/pqc-public.pgp").unwrap().into(),
            Some(Ok("Interoperability concern.".into())))?
            .with_tags(&["v6", "pqc", "draft"]));

    plan.add(Box::new(
        roundtrip::InlineSignVerifyRoundtrip::cleartext(
            "Problematic Cleartext Signature Sign-Verify roundtrip",
            "Cleartext Signature Sign-Verify roundtrip using the 'Bob' key from \
             draft-bre-openpgp-samples-00.  This test uses a particularly \
             problematic text which requires dash-escaping and has trailing \
             newlines.",
            data::certificate("bob-secret.pgp"),
            data::certificate("bob.pgp"),
            Some(Ok("Interoperability concern.".into())))?
            .with_message(super::TRICKY_GROCERY_LIST)));

    Ok(())
}
