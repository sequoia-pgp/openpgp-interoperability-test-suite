use anyhow::ensure;

use sequoia_openpgp as openpgp;
use openpgp::{
    Packet,
    packet::signature::SignatureBuilder,
    parse::Parse,
    types::{KeyFlags, SignatureType},
};

use crate::{
    Data,
    Result,
    data,
    sop::{Sop, SopResult, Verification},
    tests::{
        ConsumptionTest,
        Expectation,
        TestMatrix,
        CheckError,
    },
};

/// Explores a certificate corner case where a certificate includes
/// its primary key as subkey.
pub struct ImMyOwnGrandpa {
}

impl ImMyOwnGrandpa {
    pub fn new() -> Result<ImMyOwnGrandpa> {
        Ok(ImMyOwnGrandpa {
        })
    }

    fn message(&self) -> &'static [u8] {
        crate::tests::MESSAGE
    }
}

impl crate::plan::Runnable<TestMatrix> for ImMyOwnGrandpa {
    fn title(&self) -> String {
        "I'm My Own Grandpa".into()
    }

    fn description(&self) -> String {
        "<p>Explores a certificate corner case where a certificate \
         includes its primary key as subkey.  This is an oddball, \
         supporting it is not necessary.</p>\
         \
         <p>A certificate is constructed by taking Bob's subkey and \
         using it as primary key as well as subkey.  The test encrypts \
         a short message, and tries to decrypt is using Bob's key.</p>"
            .into()
    }

    fn artifacts(&self) -> Vec<(String, Data)> {
        vec![("Key".into(), data::certificate("bob-secret.pgp").into())]
    }

    fn run(&self, matrix: TestMatrix, implementations: &[crate::Sop])
           -> Result<TestMatrix> {
        ConsumptionTest::run(self, matrix, implementations)
    }
}

impl ConsumptionTest<Data, (Data, Vec<Verification>)> for ImMyOwnGrandpa {
    fn produce(&self) -> Result<Vec<(String, Data, Option<Expectation>)>> {
        let bob =
            openpgp::Cert::from_bytes(data::certificate("bob-secret.pgp"))?;
        let uid =
            bob.userids().nth(0).unwrap().userid().clone();
        let uidb =
            bob.userids().nth(0).unwrap().self_signatures()
                .nth(0).unwrap().clone();
        let key = bob.keys().subkeys().nth(0).unwrap().key().clone();
        let mut signer =
            key.clone().parts_into_secret()?.into_keypair()?;
        let sub_key = key.clone().take_secret().0;
        let primary_key = sub_key.clone().role_into_primary();

        let cert = openpgp::Cert::from_packets(
            vec![primary_key.clone().into()].into_iter())?;

        use super::make_test as make;
        Ok(vec![
            make("I'm My Own Grandpa",
                 vec![Packet::from(primary_key.clone()),
                      uid.clone().into(),
                      uid.bind(
                          &mut signer,
                          &cert,
                          uidb.into())?.into(),
                      sub_key.clone().into(),
                      sub_key.bind(
                          &mut signer,
                          &cert,
                          SignatureBuilder::new(SignatureType::SubkeyBinding)
                              .set_signature_creation_time(
                                  sub_key.creation_time())?
                              .set_key_flags(KeyFlags::empty()
                                             .set_storage_encryption()
                                             .set_transport_encryption())?
                      )?.into(),
                 ],
                 None)?,
        ])
    }

    fn consume(&self, pgp: &Sop, artifact: &Data)
               -> SopResult<(Data, Vec<Verification>)> {
        pgp.sop()
            .encrypt()
            .cert(artifact)
            .plaintext(self.message())
            .context("encryption failed")
            .and_then(|ciphertext| {
                pgp.sop()
                    .decrypt()
                    .key(data::certificate("bob-secret.pgp"))
                    .ciphertext(&ciphertext)
                    .context("decryption failed")
            })
    }

    fn check_consumer(&self, _artifact: &Data,
                      (plaintext, _sigs): &(Data, Vec<Verification>),
                      _expectation: &Option<Expectation>)
                      -> Result<()> {
        ensure!(&plaintext[..] == self.message(),
            CheckError::HardFailure(format!("Expected {:?}, got {:?}",
                                            self.message(), plaintext)));
        Ok(())
    }
}
