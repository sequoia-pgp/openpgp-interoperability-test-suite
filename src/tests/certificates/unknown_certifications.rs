use std::{
    io::Write,
};

use sequoia_openpgp as openpgp;
use openpgp::{
    Packet,
    packet::{Any, Signature, Tag},
    packet::signature::{
        Signature4,
        subpacket::{Subpacket, SubpacketArea, SubpacketValue},
    },
    parse::Parse,
    serialize::{Marshal, SerializeInto},
    types::*,
};

use crate::{
    Data,
    Result,
    data,
    sop::{Sop, SopResult, Verification},
    tests::{
        ConsumptionTest,
        Expectation,
        TestMatrix,
    },
};

/// Explores how robust the certificate canonicalization is to
/// unknown certifications.
pub struct UnknownCertifications {
    signature: Vec<u8>,
}

impl UnknownCertifications {
    pub fn new() -> Result<UnknownCertifications> {
        use openpgp::{
            armor::Kind,
            serialize::stream::*,
        };

        // Get the primary signer.
        let bob =
            openpgp::Cert::from_bytes(data::certificate("bob-secret.pgp"))?;
        let primary_signer = bob.primary_key().key().clone()
            .parts_into_secret()?.into_keypair()?;

        let mut signature = Vec::new();
        let message = Message::new(&mut signature);
        let message = Armorer::new(message).kind(Kind::Signature).build()?;
        let mut signer = Signer::new(message, primary_signer)
            .detached().build()?;
        signer.write_all(crate::tests::MESSAGE)?;
        signer.finalize()?;

        Ok(UnknownCertifications {
            signature,
        })
    }
}

impl crate::plan::Runnable<TestMatrix> for UnknownCertifications {
    fn title(&self) -> String {
        "Unknown certifications".into()
    }

    fn description(&self) -> String {
        format!("\
<p>Explores how robust the certificate canonicalization is in the
presence of unknown certifications.</p>

<p>The test verifies a signature with a certificate containing a
certification that is unknown in some way (unknown signature version,
unknown public key algorithm, unknown hash algorithm).  The signature
is made using the primary key over the message <code>{}</code>.  The
unknown certifiation is not involved in any way, besides being present
in the certificate.</p>
",
                String::from_utf8_lossy(crate::tests::MESSAGE),
        )
    }

    fn artifacts(&self) -> Vec<(String, Data)> {
        vec![("Signature".into(), self.signature.clone().into())]
    }

    fn tags(&self) -> std::collections::BTreeSet<&'static str> {
        ["verify-only", "forward-compat"].iter().cloned().collect()
    }

    fn run(&self, matrix: TestMatrix, implementations: &[crate::Sop])
           -> Result<TestMatrix> {
        ConsumptionTest::run(self, matrix, implementations)
    }
}

impl ConsumptionTest<Data, Vec<Verification>> for UnknownCertifications {
    fn produce(&self) -> Result<Vec<(String, Data, Option<Expectation>)>> {
        let packets =
            openpgp::PacketPile::from_bytes(data::certificate("bob.pgp"))?
            .into_children().collect::<Vec<_>>();
        assert_eq!(packets.len(), 5);
        let primary = &packets[0];
        assert_eq!(primary.kind(), Some(Tag::PublicKey));
        let uid = &packets[1];
        assert_eq!(uid.kind(), Some(Tag::UserID));
        let uidb = &packets[2];
        assert_eq!(uidb.kind(), Some(Tag::Signature));
        let subkey = &packets[3];
        assert_eq!(subkey.kind(), Some(Tag::PublicSubkey));
        let subkeyb = &packets[4];
        assert_eq!(subkeyb.kind(), Some(Tag::Signature));

        let uidb_sig: &Signature = uidb.downcast_ref().unwrap();
        let mut hashed_area = SubpacketArea::default();
        hashed_area.add(
            Subpacket::new(
                SubpacketValue::SignatureCreationTime(
                    uidb_sig.signature_creation_time().unwrap().try_into()?),
                false)?)?;
        hashed_area.add(
            Subpacket::new(
                SubpacketValue::Issuer("AAAA BBBB CCCC DDDD".parse()?),
                false)?)?;
        hashed_area.add(
            Subpacket::new(
                SubpacketValue::IssuerFingerprint(
                    "AAAA BBBB CCCC DDDD AAAA BBBB CCCC DDDD AAAA BBBB".parse()?),
                false)?)?;

        let certification_unknown_hash: Packet = {
            let sig4: &Signature = uidb.downcast_ref().unwrap();
            Signature4::new(
                sig4.typ(),
                sig4.pk_algo(),
                HashAlgorithm::Unknown(99),
                hashed_area.clone(),
                Default::default(),
                sig4.digest_prefix().clone(),
                sig4.mpis().clone()).into()
        };
        let certification_unknown_pk: Packet = {
            let sig4: &Signature = uidb.downcast_ref().unwrap();
            Signature4::new(
                sig4.typ(),
                PublicKeyAlgorithm::Unknown(99),
                sig4.hash_algo(),
                hashed_area.clone(),
                Default::default(),
                sig4.digest_prefix().clone(),
                sig4.mpis().clone()).into()
        };
        let mut certification_unknown_version =
            certification_unknown_hash.to_vec()?;
        certification_unknown_version[3] = 23;
        let certification_unknown_version =
            Packet::from_bytes(&certification_unknown_version)?;

        let make =
            |test: &str, packets: &[&Packet], expectation: Option<Expectation>|
            -> Result<(String, Data, Option<Expectation>)>
        {
            let mut buf = Vec::new();
            {
                use openpgp::armor;
                let mut w =
                    armor::Writer::new(&mut buf, armor::Kind::PublicKey)?;
                for p in packets {
                    p.serialize(&mut w)?;
                }
                w.finalize()?;
            }
            Ok((test.into(), buf.into(), expectation))
        };

        Ok(vec![
            // Base case.
            make("Bob's cert".into(),
                 &[primary, uid, uidb, subkey, subkeyb],
                 Some(Ok("Base case.".into())))?,

            make("Unknown hash algorithm".into(),
                 &[primary, uid, uidb, &certification_unknown_hash,
                   subkey, subkeyb],
                 Some(Ok("Unknown versions must be ignored.".into())))?,

            make("Unknown public key algorithm".into(),
                 &[primary, uid, uidb, &certification_unknown_pk,
                   subkey, subkeyb],
                 Some(Ok("Unknown versions must be ignored.".into())))?,

            make("Unknown packet version".into(),
                 &[primary, uid, uidb, &certification_unknown_version,
                   subkey, subkeyb],
                 Some(Ok("Unknown versions must be ignored.".into())))?,
        ])
    }

    fn consume(&self, pgp: &Sop, artifact: &Data)
               -> SopResult<Vec<Verification>> {
        pgp.sop()
            .verify()
            .cert(artifact)
            .signatures(&self.signature)
            .data(crate::tests::MESSAGE)
    }
}
