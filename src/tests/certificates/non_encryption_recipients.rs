use std::io::Write;

use anyhow::ensure;

use sequoia_openpgp as openpgp;
use openpgp::{
    cert::prelude::*,
    packet::{Key, key::*, signature::{*, subpacket::*}},
    policy::StandardPolicy,
    serialize::SerializeInto,
    types::*,
};
use crate::{
    Data,
    Result,
    sop::{Sop, SopResult, Verification},
    tests::{
        Expectation,
        TestMatrix,
        ConsumptionTest,
        CheckError,
    },
};

/// Tests whether implementations are willing to decrypt messages
/// encrypted for a subkey that has not been marked as
/// encryption-capable.
pub struct NonEncryptionRecipients {
    cert: Cert,
    cert_data: Data,
}

impl NonEncryptionRecipients {
    pub fn new() -> Result<NonEncryptionRecipients> {
        let (cert, _) = CertBuilder::new()
            .set_cipher_suite(CipherSuite::RSA2k)
            .add_userid("Nick Comte <comte@example.org>")
            .add_transport_encryption_subkey()
            .add_storage_encryption_subkey()
            .add_signing_subkey()
            .add_authentication_subkey()
            .add_subkey_with(
                KeyFlags::empty(), None, None,
                SignatureBuilder::new(SignatureType::SubkeyBinding))?
            .add_subkey_with(
                KeyFlags::empty().set(8), None, None,
                SignatureBuilder::new(SignatureType::SubkeyBinding))?
            .add_subkey_with(
                KeyFlags::empty().set_transport_encryption(), None, None,
                SignatureBuilder::new(SignatureType::SubkeyBinding)
                // Add a critical notation!
                    .set_notation("critical@policy.example.org",
                                  b"and you don't know it yet",
                                  NotationDataFlags::empty(), true)?)?
            .generate()?;
        let cert_data = cert.as_tsk().to_vec()?.into();
        Ok(NonEncryptionRecipients {
            cert_data,
            cert,
        })
    }
}

impl crate::plan::Runnable<TestMatrix> for NonEncryptionRecipients {
    fn title(&self) -> String {
        "Non-encryption recipients".into()
    }

    fn description(&self) -> String {
        "<p>
             Tests whether implementations are willing to decrypt
             messages encrypted for a subkey that has not been marked
             as encryption-capable.
         </p>
        <p>
            The key has a couple of subkeys with varying properties,
            all RSA keys.  The test encrypts a message using each
            subkey and the primary key, and tests whether
            implementations are willing to decrypt the message.
        </p>".into()
    }

    fn artifacts(&self) -> Vec<(String, Data)> {
        vec![("TSK".into(), self.cert_data.clone())]
    }

    fn run(&self, matrix: TestMatrix, implementations: &[crate::Sop])
           -> Result<TestMatrix> {
        ConsumptionTest::run(self, matrix, implementations)
    }
}

impl ConsumptionTest<Data, (Data, Vec<Verification>)> for NonEncryptionRecipients {
    fn produce(&self) -> Result<Vec<(String, Data, Option<Expectation>)>> {
        use openpgp::serialize::stream::Encryptor2 as Encryptor;
        use openpgp::serialize::stream::*;

        // The tests.
        let mut t = Vec::new();

        assert_eq!(self.cert.keys().count(), 8);

        let mut p = StandardPolicy::new();
        p.good_critical_notations(&["critical@policy.example.org"]);

        fn make<S>(t: &mut Vec<(String, Data, Option<Expectation>)>,
                   cert: &Cert,
                   selector: S,
                   comment: &str,
                   expectation: Option<Expectation>)
                   -> Result<()>
        where
            S: Fn(&Cert) -> Key<PublicParts, UnspecifiedRole>,
        {
            let mut buf = Vec::new();
            let message = Message::new(&mut buf);
            let message = Armorer::new(message).build()?;
            let recipient = selector(cert);
            let message = Encryptor::for_recipients(
                message, Some(Recipient::from(&recipient)))
                .symmetric_algo(SymmetricAlgorithm::AES256)
                .build()?;
            let mut message = LiteralWriter::new(message).build()?;
            message.write_all(crate::tests::MESSAGE)?;
            message.finalize()?;
            t.push((comment.into(),
                    buf.into(),
                    expectation));
            Ok(())
        }

        make(&mut t, &self.cert,
             |c: &Cert| c.keys().with_policy(&p, None)
             .for_transport_encryption()
             .filter(|k| k.binding_signature()
                     .notation("critical@policy.example.org").next().is_none())
             .next().unwrap().key().clone(),
             "Base case, transport encryption",
             Some(Ok("Interoperability concern".into())))?;
        make(&mut t, &self.cert,
             |c: &Cert| c.keys().with_policy(&p, None)
             .for_storage_encryption().next().unwrap().key().clone(),
             "Base case, storage encryption",
             Some(Ok("Interoperability concern".into())))?;
        make(&mut t, &self.cert,
             |c: &Cert| c.keys().nth(0).unwrap().key().clone(),
             "Encrypted to primary",
             Some(Err("Not marked encryption-capable".into())))?;
        make(&mut t, &self.cert,
             |c: &Cert| c.keys().with_policy(&p, None)
             .for_signing().next().unwrap().key().clone(),
             "Encrypted to signing subkey",
             Some(Err("Not marked encryption-capable".into())))?;
        make(&mut t, &self.cert,
             |c: &Cert| c.keys().with_policy(&p, None)
             .for_authentication().next().unwrap().key().clone(),
             "Encrypted to auth subkey",
             Some(Err("Not marked encryption-capable".into())))?;
        make(&mut t, &self.cert,
             |c: &Cert| c.keys().with_policy(&p, None)
             .filter(|k| k.binding_signature()
                     .key_flags().map(|f| f == KeyFlags::empty())
                     .unwrap_or(false))
             .next().unwrap().key().clone(),
             "Encrypted to subkey with empty flags",
             Some(Err("Not marked encryption-capable".into())))?;
        make(&mut t, &self.cert,
             |c: &Cert| c.keys().with_policy(&p, None)
             .key_flags(KeyFlags::empty().set(8))
             .next().unwrap().key().clone(),
             "Encrypted to subkey with unknown flags",
             Some(Err("Not marked encryption-capable".into())))?;
        make(&mut t, &self.cert,
             |c: &Cert| c.keys().with_policy(&p, None)
             .for_transport_encryption()
             .filter(|k| k.binding_signature()
                     .notation("critical@policy.example.org").next().is_some())
             .next().unwrap().key().clone(),
             "Encrypted to subkey w/unknown notation",
             Some(Err("Not marked encryption-capable".into())))?;

        Ok(t)
    }

    fn consume(&self, pgp: &Sop, artifact: &Data)
               -> SopResult<(Data, Vec<Verification>)> {
        pgp.sop()
            .decrypt()
            .key(&self.cert_data)
            .ciphertext(artifact)
    }

    fn check_consumer(&self, _artifact: &Data,
                      (plaintext, _sigs): &(Data, Vec<Verification>),
                      _expectation: &Option<Expectation>)
                      -> Result<()> {
        ensure!(&plaintext[..] == crate::tests::MESSAGE,
            CheckError::HardFailure(format!(
                "Expected {:?}, got {:?}",
                crate::tests::MESSAGE, plaintext)));
        Ok(())
    }

}
