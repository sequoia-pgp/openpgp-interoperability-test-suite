use std::io::Write as _;
use std::fmt::Write as _;
use anyhow::{
    ensure,
    bail,
};

use sequoia_openpgp::{
    Cert,
    armor::Kind,
    parse::Parse,
    policy::StandardPolicy,
    serialize::stream::Encryptor2 as Encryptor,
    serialize::stream::*,
};

use crate::{
    Data,
    Result,
    data,
    sop::{Sop, SopResult, Verification},
    tests::{
        ConsumptionTest,
        Expectation,
        TestMatrix,
        CheckError,
    },
};

/// Applies a series of textual transformations to armored data.
fn mangle_armor<D>(data: D)
                   -> Result<Vec<(String, Data, Option<Expectation>)>>
where D: AsRef<[u8]>,
{
    mangle(data, false)
}

/// Applies a series of textual transformations to the given message
/// using the cleartext signature framework.
fn mangle_csf_message<D>(data: D)
                   -> Result<Vec<(String, Data, Option<Expectation>)>>
where D: AsRef<[u8]>,
{
    mangle(data, true)
}

/// Applies a series of textual transformations to armored data.
fn mangle<D>(data: D, csf: bool)
             -> Result<Vec<(String, Data, Option<Expectation>)>>
where D: AsRef<[u8]>,
{
    let data = data.as_ref();
    // Data as Vec<u8>.
    let data = data.to_vec();
    // Data as String.
    let datas =
        String::from_utf8(data.to_vec()).unwrap();
    // Data as Vec<&str>.
    let datal = datas.lines().collect::<Vec<_>>();
    // Position of the empty line separating header and body.
    let empty_line = datal.iter().position(|l| *l == "").unwrap();
    // Position of the armor headers (in the case of a CSF message,
    // the armor headers of the signature, not the headers of the
    // cleartext, those are always at position 1).
    let armor_headers_line = datal.iter().rposition(|l| (*l).starts_with("-----BEGIN")).unwrap() + 1;

    fn quote<'a, S: AsRef<str>>(l: impl Iterator<Item = S> + 'a,
                                prefix: &'a str)
                                -> impl Iterator<Item = String> + 'a {
        l.map(move |l| format!("{}{}", prefix, l.as_ref()))
    }

    fn join<S: AsRef<str>>(l: impl Iterator<Item = S>, suffix: &str) -> Data
    {
        let l = l.map(|l| l.as_ref().to_string()).collect::<Vec<_>>();
        l.join(suffix).into_bytes().into()
    }

    /// Marks transformations as applicable for...
    enum Applicable {
        ForBoth,
        ForArmor,
        ForCSF,
    }
    use Applicable::*;

    Ok(vec![
        ("Not mangled".into(),
         data.clone().into(),
         ForBoth,
         Some(Ok("Base case".into()))),

        ("\\r\\n line endings".into(),
         join(datal.iter(), "\r\n"),
         ForBoth,
         Some(Ok("Interoperability concern".into()))),

        ("Blank line with space".into(),
         {
             let mut l = datal.clone();
             l[empty_line] = " ";
             join(l.iter(), "\n")
         },
         ForBoth,
         Some(Ok("Interoperability concern".into()))),

        ("Blank line with ' \\t\\r'".into(),
         {
             let mut l = datal.clone();
             l[empty_line] = " \t\r";
             join(l.iter(), "\n")
         },
         ForBoth,
         Some(Ok("Interoperability concern".into()))),

        ("Blank line with ' \\t\\r\\v\\f'".into(),
         {
             let mut l = datal.clone();
             l[empty_line] = " \t\r\x0b\x0c";
             join(l.iter(), "\n")
         },
         ForBoth,
         None),

        ("Cleartext with extra Hash header key".into(),
         {
             let mut l = datal.clone();
             l.insert(1, "Hash: SHA256");
             join(l.iter(), "\n")
         },
         ForCSF,
         None),

        ("Cleartext with extra Hash header key, two hashes (SHA256 and SHA512)".into(),
         {
             let mut l = datal.clone();
             l.insert(1, "Hash: SHA256, SHA512");
             join(l.iter(), "\n")
         },
         ForCSF,
         None),

        ("Cleartext with extra Hash header key, two hashes (SHA256 and RipeMD160)".into(),
         {
             let mut l = datal.clone();
             l.insert(1, "Hash: SHA256, RipeMD160");
             join(l.iter(), "\n")
         },
         ForCSF,
         None),

        ("Cleartext with extra Hash header key, unknown hash".into(),
         {
             let mut l = datal.clone();
             l.insert(1, "Hash: UnknownHash256");
             join(l.iter(), "\n")
         },
         ForCSF,
         None),

        ("Cleartext with extra SaltedHash header key".into(),
         {
             let mut l = datal.clone();
             l.insert(1, "SaltedHash: SHA256:kJn9ODTIN54nxrZgzTQDkA");
             join(l.iter(), "\n")
         },
         ForCSF,
         None),

        ("Cleartext with unknown header key".into(),
         {
             let mut l = datal.clone();
             l.insert(1, "Hello: this is totally part of the signed text");
             join(l.iter(), "\n")
         },
         ForCSF,
         Some(Err("Unknown headers should be rejected".into()))),

        ("Version header key".into(),
         {
             let mut l = datal.clone();
             l.insert(armor_headers_line, "Version: FooPGP 2000");
             join(l.iter(), "\n")
         },
         ForBoth,
         Some(Ok("Interoperability concern".into()))),

        ("Version header key w/o value".into(),
         {
             let mut l = datal.clone();
             l.insert(armor_headers_line, "Version: ");
             join(l.iter(), "\n")
         },
         ForBoth,
         Some(Ok("Interoperability concern".into()))),

        ("Version header key w/o space".into(),
         {
             let mut l = datal.clone();
             l.insert(armor_headers_line, "Version:FooPGP 2000");
             join(l.iter(), "\n")
         },
         ForBoth,
         None),

        ("Version header key w/o space, value".into(),
         {
             let mut l = datal.clone();
             l.insert(armor_headers_line, "Version:");
             join(l.iter(), "\n")
         },
         ForBoth,
         None),

        ("MessageID header key".into(),
         {
             let mut l = datal.clone();
             l.insert(armor_headers_line, "MessageID: vtB!m)V#q@!~LM>\\=^od\"a]R5]iejb2W");
             join(l.iter(), "\n")
         },
         ForBoth,
         None),

        ("Too long MessageID header key".into(),
         {
             let mut l = datal.clone();
             l.insert(armor_headers_line, "MessageID: ! \"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~");
             join(l.iter(), "\n")
         },
         ForBoth,
         None),

        ("Charset header key".into(),
         {
             let mut l = datal.clone();
             l.insert(armor_headers_line, "Charset: UTF-8");
             join(l.iter(), "\n")
         },
         ForBoth,
         None),

        ("Charset header key w/unknown".into(),
         {
             let mut l = datal.clone();
             l.insert(armor_headers_line, "Charset: ThisIsNotACharset");
             join(l.iter(), "\n")
         },
         ForBoth,
         None),

        ("Unknown header key".into(),
         {
             let mut l = datal.clone();
             l.insert(armor_headers_line, "Unknown: blabla");
             join(l.iter(), "\n")
         },
         ForBoth,
         Some(Ok("Interoperability concern".into()))),

        ("Very long header key".into(),
         {
             let mut comment = "Comment: ".to_string();
             for _ in 0..64 {
                 write!(comment, "0123456789abcde").unwrap();
             }
             let mut l = datal.clone();
             l.insert(armor_headers_line, &comment);
             join(l.iter(), "\n")
         },
         ForBoth,
         Some(Ok("Interoperability concern".into()))),

        ("No checksum".into(),
         {
             let mut l = datal.clone();
             l.remove(datal.len() - 2);
             join(l.iter(), "\n")
         },
         ForBoth,
         Some(Ok("Interoperability concern".into()))),

        ("No newlines in body".into(),
         join(datal.iter().take(3).map(|l| l.to_string()).chain(
             Some(datal[3..datal.len() - 2].join(""))).chain(
             datal[datal.len() - 2..].iter().map(|l| l.to_string())),
              "\n"),
         ForArmor,
         None),

        ("Spurious spaces in body".into(),
         {
             let mut b = data.clone();
             b.insert(b.len() - 77 - 0 * 65, b' ');
             b.insert(b.len() - 77 - 1 * 65, b'\n');
             b.insert(b.len() - 77 - 2 * 65, b'\t');
             b.insert(b.len() - 77 - 3 * 65, 0x0b);
             b.into()
         },
         ForBoth,
         None),

        ("Leading space".into(),
         join(quote(datal.iter(), " "), "\n"),
         ForBoth,
         None),

        ("Leading space, ends trimmed".into(),
         join(quote(datal.iter(), " ").map(|l| l.trim_end().to_string()),
              "\n"),
         ForBoth,
         None),

        ("Trailing space".into(),
         join(datal.iter(), " \n"),
         ForBoth,
         None),

        ("Double spaced".into(),
         join(datal.iter(), "\n\n"),
         ForBoth,
         None),

        ("Newlines replaced by spaces".into(),
         datas.replace("\n", " ").into_bytes().into(),
         ForArmor,
         None),

        ("Quoted with '> '".into(),
         join(quote(datal.iter(), "> "), "\n"),
         ForBoth,
         None),

        ("Quoted with '> ', ends trimmed".into(),
         join(quote(datal.iter(), "> ").map(|l| l.trim_end().to_string()),
              "\n"),
         ForBoth,
         None),

        ("Quoted with '] } >>> '".into(),
         join(quote(datal.iter(), "] } >>> "), "\n"),
         ForBoth,
         None),

        ("Missing '-' in header".into(),
         datas.clone().replacen("-", "", 1).into_bytes().into(),
         ForBoth,
         None),

        ("Unicode hyphens '‐'".into(),
         datas.clone().replace("-", "‐").into_bytes().into(),
         ForBoth,
         None),

        ("No hyphens".into(),
         datas.clone().replace("-", "").into_bytes().into(),
         ForArmor,
         None),

        ("Quoted-printable '=' -> '=3D'".into(),
         datas.clone().replace("=", "=3D").into_bytes().into(),
         ForBoth,
         None),

        ("Dash-escaped frames".into(),
         datas.clone().replace("-----B", "- -----B")
         .replace("-----E", "- -----E").into_bytes().into(),
         ForBoth,
         None),

        ("Don't dash-escape 'From '".into(),
         datas.clone().replace("- From ", "From ").into_bytes().into(),
         ForCSF,
         None),

        ("Don't dash-escape '- '".into(),
         datas.clone().replace("- - ", "- ").into_bytes().into(),
         ForCSF,
         None),

        ("Missing header".into(),
         {
             let mut l = datal.clone();
             l.remove(0);
             join(l.iter(), "\n")
         },
         ForBoth,
         None),

        ("Missing blank line".into(),
         {
             let mut l = datal.clone();
             l.remove(empty_line);
             join(l.iter(), "\n")
         },
         ForBoth,
         None),

        ("Missing blank line, no headers".into(),
         {
             let mut l = datal.clone();
             l.drain(1..empty_line+1);
             join(l.iter(), "\n")
         },
         ForBoth,
         None),

        ("Missing footer".into(),
         {
             let mut l = datal.clone();
             l.remove(datal.len() - 1);
             join(l.iter(), "\n")
         },
         ForBoth,
         None),

        ("Bare base64 body".into(),
         {
             let mut l = datal.clone();
             l.remove(datal.len() - 1); // Footer.
             l.remove(datal.len() - 2); // Checksum.
             l.remove(0); // Header.
             l.remove(0); // Comment.
             l.remove(0); // Separator.
             join(l.iter(), "\n")
         },
         ForArmor,
         None),

        ("Bad checksum".into(),
         {
             let mut l = datal.clone();
             l[datal.len() - 2] = "=AAAA".into();
             join(l.iter(), "\n")
         },
         ForBoth,
         None),

        ("Malformed checksum".into(),
         {
             let mut l = datal.clone();
             l[datal.len() - 2] = "=AAA".into();
             join(l.iter(), "\n")
         },
         ForBoth,
         None),

        ("Leading blank line".into(),
         ("\n".to_owned() + &datas).into_bytes().into(),
         ForBoth,
         None),

        ("Leading line with ' \\t\\r'".into(),
         (" \t\r\n".to_owned() + &datas).into_bytes().into(),
         ForBoth,
         None),

        ("Leading line with ' \\t\\r\\v\\f'".into(),
         (" \t\r\x0b\x0c\n".to_owned() + &datas).into_bytes().into(),
         ForBoth,
         None),

        ("Leading line with text".into(),
         ("hello\n".to_owned() + &datas).into_bytes().into(),
         ForBoth,
         match csf {
            true => Some(Err("Leading text should be rejected as it may be confused as being part of the signed message".into())),
            false => None,
         }),

        ("Trailing blank line".into(),
         (datas.clone() + "\n").into_bytes().into(),
         ForBoth,
         None),

        ("Trailing line with ' \\t\\r'".into(),
         (datas.clone() + " \t\r\n").into_bytes().into(),
         ForBoth,
         None),

        ("Trailing line with ' \\t\\r\\v\\f'".into(),
         (datas.clone() + " \t\r\x0b\x0c\n").into_bytes().into(),
         ForBoth,
         None),

        ("Trailing line with text".into(),
         (datas.clone() + "hello\n").into_bytes().into(),
         ForBoth,
         match csf {
            true => Some(Err("Trailing text should be rejected as it may be confused as being part of the signed message".into())),
            false => None,
         }),
    ].into_iter().filter_map(|(desc, data, applicable, expectation)| {
        match applicable {
            ForBoth => Some((desc, data, expectation)),
            ForCSF if csf => Some((desc, data, expectation)),
            ForArmor if ! csf => Some((desc, data, expectation)),
            _ => None,
        }
    }).collect())
}

/// Explores how robust the ASCII Armor reader is when reading KEYs.
pub struct MangledArmoredKey {
}

impl MangledArmoredKey {
    pub fn new() -> Result<MangledArmoredKey> {
        Ok(MangledArmoredKey {
        })
    }

    fn message(&self) -> &'static [u8] {
        crate::tests::MESSAGE
    }
}

impl crate::plan::Runnable<TestMatrix> for MangledArmoredKey {
    fn title(&self) -> String {
        "Mangled ASCII Armored Key".into()
    }

    fn description(&self) -> String {
      format!(
          "<p>ASCII Armor is supposed to protect OpenPGP data in \
          transit, but unfortunately it can be a source of brittleness \
          if the Armor parser isn't sufficiently robust.</p>\
          \
          <p>This test mangles Bob's ASCII Armored key, and \
          tries to decrypt the text <code>{}</code>.</p>",
          String::from_utf8(self.message().into()).unwrap())
    }

    fn artifacts(&self) -> Vec<(String, Data)> {
        vec![("Certificate".into(), data::certificate("bob.pgp").into())]
    }

    fn run(&self, matrix: TestMatrix, implementations: &[crate::Sop])
           -> Result<TestMatrix> {
        ConsumptionTest::run(self, matrix, implementations)
    }
}

impl ConsumptionTest<Data, (Data, Vec<Verification>)> for MangledArmoredKey {
    fn produce(&self) -> Result<Vec<(String, Data, Option<Expectation>)>> {
        mangle_armor(data::certificate("bob-secret.pgp"))
    }

    fn consume(&self, pgp: &Sop, artifact: &Data)
               -> SopResult<(Data, Vec<Verification>)> {
        pgp.sop()
            .encrypt()
            .cert(data::certificate("bob.pgp"))
            .plaintext(self.message())
            .context("Encryption failed")
            .and_then(|ciphertext| {
                pgp.sop()
                    .decrypt()
                    .key(artifact)
                    .ciphertext(&ciphertext)
                    .context("Decryption failed")
            })
    }

    fn check_consumer(&self, _: &Data, (artifact, _): &(Data, Vec<Verification>),
                      _expextation: &Option<Expectation>)
                      -> Result<()> {
        ensure!(&artifact[..] == self.message(),
            CheckError::HardFailure(format!("Expected {:?}, got {:?}",
                                            self.message(), artifact)));
        Ok(())
    }
}

/// Explores how robust the ASCII Armor reader is when reading CERTs.
pub struct MangledArmoredCert {
}

impl MangledArmoredCert {
    pub fn new() -> Result<MangledArmoredCert> {
        Ok(MangledArmoredCert {
        })
    }

    fn message(&self) -> &'static [u8] {
        crate::tests::MESSAGE
    }
}

impl crate::plan::Runnable<TestMatrix> for MangledArmoredCert {
    fn title(&self) -> String {
        "Mangled ASCII Armor".into()
    }

    fn description(&self) -> String {
      format!(
          "<p>ASCII Armor is supposed to protect OpenPGP data in \
          transit, but unfortunately it can be a source of brittleness \
          if the Armor parser isn't sufficiently robust.</p>\
          \
          <p>This test mangles Bob's ASCII Armored certificate, and \
          tries to encrypt the text <code>{}</code>.</p>",
          String::from_utf8(self.message().into()).unwrap())
    }

    fn artifacts(&self) -> Vec<(String, Data)> {
        vec![("Key".into(), data::certificate("bob-secret.pgp").into())]
    }

    fn run(&self, matrix: TestMatrix, implementations: &[crate::Sop])
           -> Result<TestMatrix> {
        ConsumptionTest::run(self, matrix, implementations)
    }
}

impl ConsumptionTest<Data, (Data, Vec<Verification>)> for MangledArmoredCert {
    fn produce(&self) -> Result<Vec<(String, Data, Option<Expectation>)>> {
        mangle_armor(data::certificate("bob.pgp"))
    }

    fn consume(&self, pgp: &Sop, artifact: &Data)
               -> SopResult<(Data, Vec<Verification>)> {
        pgp.sop()
            .encrypt()
            .cert(artifact)
            .plaintext(self.message())
            .context("Encryption failed")
            .and_then(|ciphertext| {
                pgp.sop()
                    .decrypt()
                    .key(data::certificate("bob-secret.pgp"))
                    .ciphertext(&ciphertext)
                    .context("Decryption failed")
            })
    }

    fn check_consumer(&self, _: &Data, (artifact, _): &(Data, Vec<Verification>),
                      _expextation: &Option<Expectation>)
                      -> Result<()> {
        ensure!(&artifact[..] == self.message(),
            CheckError::HardFailure(format!("Expected {:?}, got {:?}",
                                            self.message(), artifact)));
        Ok(())
    }
}

/// Explores how robust the ASCII Armor reader is when reading
/// CIPHERTEXTs.
pub struct MangledArmoredCiphertext {
}

impl MangledArmoredCiphertext {
    pub fn new() -> Result<MangledArmoredCiphertext> {
        Ok(MangledArmoredCiphertext {
        })
    }

    fn message(&self) -> &'static [u8] {
        crate::tests::MESSAGE
    }
}

impl crate::plan::Runnable<TestMatrix> for MangledArmoredCiphertext {
    fn title(&self) -> String {
        "Mangled ASCII Armored Ciphertexts".into()
    }

    fn description(&self) -> String {
      format!(
          "<p>ASCII Armor is supposed to protect OpenPGP data in \
          transit, but unfortunately it can be a source of brittleness \
          if the Armor parser isn't sufficiently robust.</p>\
          \
          <p>This test mangles the ASCII Armored ciphertext decrypting \
          to the text <code>{}</code>.</p>",
          String::from_utf8(self.message().into()).unwrap())
    }

    fn artifacts(&self) -> Vec<(String, Data)> {
        vec![("Key".into(), data::certificate("bob-secret.pgp").into())]
    }

    fn run(&self, matrix: TestMatrix, implementations: &[crate::Sop])
           -> Result<TestMatrix> {
        ConsumptionTest::run(self, matrix, implementations)
    }
}

impl ConsumptionTest<Data, (Data, Vec<Verification>)> for MangledArmoredCiphertext {
    fn produce(&self) -> Result<Vec<(String, Data, Option<Expectation>)>> {
        let cert = Cert::from_bytes(data::certificate("bob.pgp"))?;
        let p = &StandardPolicy::new();
        let recipients =
            cert.keys().with_policy(p, None).supported().alive().revoked(false)
            .for_transport_encryption();
        let mut sink = Vec::new();
        let message = Message::new(&mut sink);
        let message = Armorer::new(message).build()?;
        let message = Encryptor::for_recipients(message, recipients).build()?;
        let mut message = LiteralWriter::new(message).build()?;
        message.write_all(self.message())?;
        message.finalize()?;
        mangle_armor(sink)
    }

    fn consume(&self, pgp: &Sop, artifact: &Data)
               -> SopResult<(Data, Vec<Verification>)> {
        pgp.sop()
            .decrypt()
            .key(data::certificate("bob-secret.pgp"))
            .ciphertext(artifact)
            .context("Decryption failed")
    }

    fn check_consumer(&self, _: &Data, (artifact, _): &(Data, Vec<Verification>),
                      _expextation: &Option<Expectation>)
                      -> Result<()> {
        ensure!(&artifact[..] == self.message(),
            CheckError::HardFailure(format!("Expected {:?}, got {:?}",
                                            self.message(), artifact)));
        Ok(())
    }
}

/// Explores how robust the ASCII Armor reader is when reading
/// SIGNATUREs.
pub struct MangledArmoredSignature {
}

impl MangledArmoredSignature {
    pub fn new() -> Result<MangledArmoredSignature> {
        Ok(MangledArmoredSignature {
        })
    }

    fn message(&self) -> &'static [u8] {
        crate::tests::MESSAGE
    }
}

impl crate::plan::Runnable<TestMatrix> for MangledArmoredSignature {
    fn title(&self) -> String {
        "Mangled ASCII Armored Signatures".into()
    }

    fn description(&self) -> String {
      format!(
          "<p>ASCII Armor is supposed to protect OpenPGP data in \
          transit, but unfortunately it can be a source of brittleness \
          if the Armor parser isn't sufficiently robust.</p>\
          \
          <p>This test mangles the ASCII Armored signature over \
          the text <code>{}</code>.</p>",
          String::from_utf8(self.message().into()).unwrap())
    }

    fn artifacts(&self) -> Vec<(String, Data)> {
        vec![("Certificate".into(), data::certificate("bob.pgp").into())]
    }

    fn run(&self, matrix: TestMatrix, implementations: &[crate::Sop])
           -> Result<TestMatrix> {
        ConsumptionTest::run(self, matrix, implementations)
    }
}

impl ConsumptionTest<Data, Vec<Verification>> for MangledArmoredSignature {
    fn produce(&self) -> Result<Vec<(String, Data, Option<Expectation>)>> {
        let cert = Cert::from_bytes(data::certificate("bob-secret.pgp"))?;
        let p = &StandardPolicy::new();
        let signer =
            cert.keys().with_policy(p, None).supported().alive().revoked(false)
            .secret()
            .for_signing()
            .nth(0).unwrap()
            .key().clone().into_keypair()?;
        let mut sink = Vec::new();
        let message = Message::new(&mut sink);
        let message = Armorer::new(message).kind(Kind::Signature).build()?;
        let mut message = Signer::new(message, signer).detached().build()?;
        message.write_all(self.message())?;
        message.finalize()?;
        mangle_armor(sink)
    }

    fn consume(&self, pgp: &Sop, artifact: &Data)
               -> SopResult<Vec<Verification>> {
        pgp.sop()
            .verify()
            .cert(data::certificate("bob.pgp"))
            .signatures(artifact)
            .data(self.message())
            .context("Verification failed")
    }
}

/// Explores how robust the ASCII Armor reader is when verifying
/// messages using the Cleartext Signature Framework.
pub struct MangledCleartextSignedMessage {
    signer: Cert,
}

impl MangledCleartextSignedMessage {
    pub fn new() -> Result<MangledCleartextSignedMessage> {
        Ok(MangledCleartextSignedMessage {
            signer: Cert::from_bytes(data::certificate("bob-secret.pgp"))?,
        })
    }

    fn message(&self) -> &'static [u8] {
        crate::tests::TRICKY_GROCERY_LIST
    }
}

impl crate::plan::Runnable<TestMatrix> for MangledCleartextSignedMessage {
    fn title(&self) -> String {
        "Mangled message using the Cleartext Signature Framework ".into()
    }

    fn description(&self) -> String {
      format!(
          "<p>ASCII Armor is supposed to protect OpenPGP data in \
          transit, but unfortunately it can be a source of brittleness \
          if the Armor parser isn't sufficiently robust.</p>\
          \
          <p>This test mangles the message using the Cleartext Signature
          Framework.  The message is <code>{}</code>.</p>",
          String::from_utf8(self.message().into()).unwrap())
    }

    fn artifacts(&self) -> Vec<(String, Data)> {
        vec![("Certificate".into(), data::certificate("bob.pgp").into())]
    }

    fn run(&self, matrix: TestMatrix, implementations: &[crate::Sop])
           -> Result<TestMatrix> {
        ConsumptionTest::run(self, matrix, implementations)
    }
}

impl ConsumptionTest<Data, (Data, Vec<Verification>)> for MangledCleartextSignedMessage {
    fn produce(&self) -> Result<Vec<(String, Data, Option<Expectation>)>> {
        let p = &StandardPolicy::new();
        let signer =
            self.signer.keys().with_policy(p, None).supported().alive()
            .revoked(false)
            .secret()
            .for_signing()
            .nth(0).unwrap()
            .key().clone().into_keypair()?;
        let mut sink = Vec::new();
        let message = Message::new(&mut sink);
        let mut message = Signer::new(message, signer).cleartext().build()?;
        message.write_all(self.message())?;
        message.finalize()?;
        mangle_csf_message(sink)
    }

    fn consume(&self, pgp: &Sop, artifact: &Data)
               -> SopResult<(Data, Vec<Verification>)> {
        pgp.sop()
            .inline_verify()
            .cert(data::certificate("bob.pgp"))
            .message(artifact)
            .context("Verification failed")
    }

    fn check_consumer(&self, _: &Data,
                      (artifact, verifications): &(Data, Vec<Verification>),
                      _expextation: &Option<Expectation>)
                      -> Result<()> {
        match verifications.len() {
            0 =>
                // This is not a hard failure, as the test expectations
                // should indicate whether a valid signature is expected.
                bail!(CheckError::SoftFailure(
                    "Expected a verification line, but got none".into())),
            1 => {
                let v = &verifications[0];
                ensure!(self.signer.fingerprint() == v.cert,
                    CheckError::HardFailure(format!(
                        "Bad cert fingerprint, expected {}, got {}",
                        self.signer.fingerprint(), v.cert)));

                // XXX test more?
            },
            n => bail!(CheckError::HardFailure(format!(
                "Expected exactly one verification line, but got {}", n))),
        }

        ensure!(crate::tests::csf_eq(artifact, self.message()),
            CheckError::HardFailure(format!(
                "Bad recovered CSF message, expected {:?}, got {:?}",
                std::str::from_utf8(self.message()),
                std::str::from_utf8(artifact))));

        Ok(())
    }
}
