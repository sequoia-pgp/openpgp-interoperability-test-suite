use std::io::Write;

use sequoia_openpgp as openpgp;
use openpgp::{
    armor,
    packet::{Tag, Packet},
    parse::Parse,
    serialize::{
        stream::*,
    },
};

use crate::{
    Data,
    Result,
    data,
    sop::{Sop, SopResult, Verification},
    tests::{
        ConsumptionTest,
        Expectation,
        TestMatrix,
    },
};

/// Explores whether signatures and certs with suboptimal length
/// encoding are accepted when verifying detached signatures.
pub struct LengthEncoding {
    sig: Data,
}

impl LengthEncoding {
    pub fn new() -> Result<LengthEncoding> {
        let cert =
            openpgp::Cert::from_bytes(data::certificate("bob-secret.pgp"))?;
        let signer =
            cert.with_policy(crate::tests::P, None)?
            .keys().secret().for_signing().next().unwrap()
            .key().clone().into_keypair()?;

        let mut buf = Vec::new();
        {
            let message = Message::new(&mut buf);
            let message = Armorer::new(message)
                .kind(armor::Kind::Signature)
                .build()?;
            let mut signer = Signer::new(message, signer)
                .detached()
                .build()?;
            signer.write_all(crate::tests::MESSAGE)?;
            signer.finalize()?;
        }

        Ok(LengthEncoding {
            sig: buf.into(),
        })
    }
}

impl crate::plan::Runnable<TestMatrix> for LengthEncoding {
    fn title(&self) -> String {
        "Suboptimal length encoding".into()
    }

    fn description(&self) -> String {
        format!(

        "<p>Explores whether signatures and certs with suboptimal \
         length encoding are accepted when verifying detached \
         signatures.</p>\
         \
         <p>Each vector consists of a cert and a detached signature \
         over the message {:?}.  We modify either the cert or the \
         message to use packets with suboptimal length encoding.  \
         The base case uses the unmodified cert and signature.</p>\
         \
         <p>The labels indicate which artifact is modified, and how.  \
         <b>L</b> denotes that a legacy CTB and length encoding scheme \
         is used.  The number denotes the length of the encoded \
         length.</p>
         ",
         String::from_utf8_lossy(crate::tests::MESSAGE))
    }

    fn artifacts(&self) -> Vec<(String, Data)> {
        vec![
            ("Bob's certificate".into(), data::certificate("bob.pgp").into()),
            ("Bob's signature".into(), self.sig.clone()),
        ]
    }

    fn tags(&self) -> std::collections::BTreeSet<&'static str> {
        ["verify-only"].iter().cloned().collect()
    }

    fn run(&self, matrix: TestMatrix, implementations: &[crate::Sop])
           -> Result<TestMatrix> {
        ConsumptionTest::run(self, matrix, implementations)
    }
}

impl ConsumptionTest<Mode, Vec<Verification>> for LengthEncoding {
    fn produce(&self) -> Result<Vec<(String, Mode, Option<Expectation>)>>
    {
        let bob = Data::from(data::certificate("bob.pgp"));

        #[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
        enum BL {
            OneOctet,
            TwoOctets,
            FourOctets,
        }
        fn frob_header(packets: &[u8], new_ctb: bool, length: BL) -> Result<Data> {
            let pp = openpgp::PacketPile::from_bytes(packets)?;
            let mut w = armor::Writer::new(
                Vec::new(),
                if pp.path_ref(&[0]).unwrap().tag() == Tag::Signature {
                    armor::Kind::Signature
                } else {
                    armor::Kind::PublicKey
                })?;

            for p in pp.children() {
                use openpgp::serialize::MarshalInto;
                let tag = p.tag();
                let body = match p {
                    Packet::Signature(p) => p.to_vec()?,
                    Packet::PublicKey(p) => p.to_vec()?,
                    Packet::PublicSubkey(p) => p.to_vec()?,
                    Packet::UserID(p) => p.to_vec()?,
                    _ => unreachable!("unexpected packet in cert"),
                };
                let len = body.len();

                if new_ctb {
                    // Compute required length.
                    let required_length = match body.len() {
                        0 ..= 191 => BL::OneOctet,
                        192 ..= 8383 => BL::TwoOctets,
                        _ => BL::FourOctets,
                    };

                    let required_length =
                        if required_length == BL::OneOctet
                        && length == BL::TwoOctets {
                            // Using two octets to denote a length
                            // that can be encoded in one octet is not
                            // representable on the wire.  We must use
                            // one octet.
                            BL::OneOctet
                        } else {
                            length.max(required_length)
                        };

                    // Write CTB.
                    w.write_all(&[0b1100_0000u8 | u8::from(tag)])?;

                    // Write length.
                    match required_length {
                        BL::OneOctet => {
                            assert!(len <= 191);
                            w.write_all(&[len as u8])?;
                        },
                        BL::TwoOctets => {
                            assert!(len <= 8383);
                            let v = len - 192;
                            let v = v + (192 << 8);
                            w.write_all(&(v as u16).to_be_bytes())?;
                        },
                        BL::FourOctets => {
                            w.write_all(&[0xff])?;
                            w.write_all(&(len as u32).to_be_bytes())?;
                        },
                    }
                } else {
                    // Compute required length.
                    let required_length = length.max(match body.len() {
                        0 ..= 0xFF => BL::OneOctet,
                        0x1_00 ..= 0xFF_FF => BL::TwoOctets,
                        _ => BL::FourOctets,
                    });

                    let length_type = match required_length {
                        BL::OneOctet => 0,
                        BL::TwoOctets => 1,
                        BL::FourOctets => 2,
                    };

                    // Write CTB.
                    w.write_all(
                        &[0b1000_0000u8 | (u8::from(tag) << 2) | length_type])?;

                    // Write length.
                    match required_length {
                        BL::OneOctet => {
                            assert!(len <= 0xFF);
                            w.write_all(&[len as u8])?;
                        },
                        BL::TwoOctets => {
                            assert!(len <= 0xFF_FF);
                            w.write_all(&(len as u16).to_be_bytes())?;
                        },
                        BL::FourOctets => {
                            w.write_all(&(len as u32).to_be_bytes())?;
                        },
                    }
                }

                // Write Packet.
                w.write_all(&body)?;
            }

            Ok(w.finalize()?.into())
        }

        Ok(vec![
            ("Cert, Signature".into(),
             Mode::FrobbingCert {
                 cert: bob.clone(),
                 sig: self.sig.clone(),
             },
             Some(Ok("Base case".into()))),
            ("Cert_2, Signature".into(),
             Mode::FrobbingCert {
                 cert: frob_header(&bob, true, BL::TwoOctets)?,
                 sig: self.sig.clone(),
             },
             Some(Ok("Interop concern".into()))),
            ("Cert_L2, Signature".into(),
             Mode::FrobbingCert {
                 cert: frob_header(&bob, false, BL::TwoOctets)?,
                 sig: self.sig.clone(),
             },
             Some(Ok("Interop concern".into()))),
            ("Cert_5, Signature".into(),
             Mode::FrobbingCert {
                 cert: frob_header(&bob, true, BL::FourOctets)?,
                 sig: self.sig.clone(),
             },
             Some(Ok("Interop concern".into()))),
            ("Cert_L4, Signature".into(),
             Mode::FrobbingCert {
                 cert: frob_header(&bob, false, BL::FourOctets)?,
                 sig: self.sig.clone(),
             },
             Some(Ok("Interop concern".into()))),
            ("Cert, Signature_2".into(),
             Mode::FrobbingSignature {
                 sig: frob_header(&self.sig, true, BL::TwoOctets)?,
                 cert: bob.clone(),
             },
             Some(Ok("Interop concern".into()))),
            ("Cert, Signature_L2".into(),
             Mode::FrobbingSignature {
                 sig: frob_header(&self.sig, false, BL::TwoOctets)?,
                 cert: bob.clone(),
             },
             Some(Ok("Interop concern".into()))),
            ("Cert, Signature_5".into(),
             Mode::FrobbingSignature {
                 sig: frob_header(&self.sig, true, BL::FourOctets)?,
                 cert: bob.clone(),
             },
             Some(Ok("Interop concern".into()))),
            ("Cert, Signature_L4".into(),
             Mode::FrobbingSignature {
                 sig: frob_header(&self.sig, false, BL::FourOctets)?,
                 cert: bob.clone(),
             },
             Some(Ok("Interop concern".into()))),
        ])
    }

    fn consume(&self,
               pgp: &Sop,
               artifact: &Mode)
               -> SopResult<Vec<Verification>> {
        pgp.sop()
            .verify()
            .cert(artifact.cert())
            .signatures(artifact.sig())
            .data(crate::tests::MESSAGE)
    }
}

enum Mode {
    FrobbingCert {
        cert: Data,
        sig: Data,
    },
    FrobbingSignature {
        cert: Data,
        sig: Data,
    },
}

impl Mode {
    fn cert(&self) -> &[u8] {
        match self {
            Mode::FrobbingCert { cert, .. } => cert,
            Mode::FrobbingSignature { cert, .. } => cert,
        }
    }

    fn sig(&self) -> &[u8] {
        match self {
            Mode::FrobbingCert { sig, .. } => sig,
            Mode::FrobbingSignature { sig, .. } => sig,
        }
    }
}

impl crate::tests::AsData for Mode {
    fn as_data(&self) -> &[u8] {
        // When rendering the report, we want to include the artifact
        // that we changed for the user to inspect.
        match self {
            Mode::FrobbingCert { .. } => self.cert(),
            Mode::FrobbingSignature { .. } => self.sig(),
        }
    }
}
