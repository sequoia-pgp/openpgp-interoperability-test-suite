use std::io::Write;

use anyhow::ensure;

use sequoia_openpgp as openpgp;
use openpgp::{
    cert::Cert,
    parse::Parse,
    policy::StandardPolicy,
    serialize::stream::Encryptor2 as Encryptor,
    serialize::stream::{*, padding::*},
};

use crate::{
    Data,
    Result,
    data,
    sop::{Sop, SopResult, Verification},
    tests::{
        Expectation,
        TestMatrix,
        ConsumptionTest,
        CheckError,
    },
};

/// Tests whether excess bytes in a packet are correctly consumed.
pub struct PacketConsumption {
}

impl PacketConsumption {
    pub fn new() -> Result<PacketConsumption> {
        Ok(PacketConsumption {
        })
    }

    fn message(&self) -> &'static [u8] {
        crate::tests::MESSAGE
    }

    fn make(&self, policy: fn(u64) -> u64) -> Result<Data> {
        let p = &StandardPolicy::new();
        let cert = Cert::from_bytes(data::certificate("bob.pgp"))?;
        let recipients = cert.keys()
            .with_policy(p, None)
            .alive()
            .revoked(false)
            .for_transport_encryption();

        let mut sink = Vec::new();
        {
            let message = Message::new(&mut sink);
            let message = Armorer::new(message)
                .kind(openpgp::armor::Kind::Message)
                .build()?;
            let message = Encryptor::for_recipients(message, recipients)
                .build()?;
            let message = Padder::new(message)
                .with_policy(policy)
                .build()?;
            let mut message = LiteralWriter::new(message)
                .build()?;
            message.write_all(self.message())?;
            message.finalize()?;
        }

        Ok(sink.into())
    }
}

impl crate::plan::Runnable<TestMatrix> for PacketConsumption {
    fn title(&self) -> String {
        "Packet excess consumption".into()
    }

    fn description(&self) -> String {
        format!(
            "<p>Tests whether excess bytes in a packet are correctly
            consumed.  The compressed data packet presents the unique
            opportunity to test whether the packet parser actually
            consumes (i.e. advances the read cursor) all the bytes
            specified in a packet header, even though they are not
            consumed by the underlying decompression
            algorithm.</p>\
            \
            <p>The plaintext message is the string <code>{}</code>,
            padded by the specified number of bytes using excess data
            in the compression stream.</p>",
            String::from_utf8(self.message().into()).unwrap())
    }

    fn artifacts(&self) -> Vec<(String, Data)> {
        vec![("Key".into(), data::certificate("bob-secret.pgp").into())]
    }

    fn run(&self, matrix: TestMatrix, implementations: &[crate::Sop])
           -> Result<TestMatrix> {
        ConsumptionTest::run(self, matrix, implementations)
    }
}

impl ConsumptionTest<Data, (Data, Vec<Verification>)> for PacketConsumption {
    fn produce(&self) -> Result<Vec<(String, Data, Option<Expectation>)>> {
        Ok(vec![
            ("Base case".into(), self.make(|n| n)?,
             Some(Ok("Base case".into()))),
            ("+1 byte".into(), self.make(|n| n + 1)?,
             Some(Ok("Excess data must be discarded".into()))),
            ("+10 bytes".into(), self.make(|n| n + 10)?,
             Some(Ok("Excess data must be discarded".into()))),
            ("+100 bytes".into(), self.make(|n| n + 100)?,
             Some(Ok("Excess data must be discarded".into()))),
            ("+1_000 bytes".into(), self.make(|n| n + 1_000)?,
             Some(Ok("Excess data must be discarded".into()))),
            ("+10_000 bytes".into(), self.make(|n| n + 10_000)?,
             Some(Ok("Excess data must be discarded".into()))),
            ("+100_000 bytes".into(), self.make(|n| n + 100_000)?,
             Some(Ok("Excess data must be discarded".into()))),
            ("+1_000_000 bytes".into(), self.make(|n| n + 1_000_000)?,
             Some(Ok("Excess data must be discarded".into()))),
            ("+10_000_000 bytes".into(), self.make(|n| n + 10_000_000)?,
             Some(Ok("Excess data must be discarded".into()))),
        ])
    }

    fn consume(&self, pgp: &Sop, artifact: &Data)
               -> SopResult<(Data, Vec<Verification>)> {
        pgp.sop()
            .decrypt()
            .key(data::certificate("bob-secret.pgp"))
            .ciphertext(artifact)
    }

    fn check_consumer(&self, _: &Data,
                      (artifact, _verifications): &(Data, Vec<Verification>),
                      _expextation: &Option<Expectation>)
                      -> Result<()>
    {
        ensure!(&artifact[..] == self.message(),
            CheckError::HardFailure(format!("Expected {:?}, got {:?}",
                                            self.message(), artifact)));
        Ok(())
    }
}
