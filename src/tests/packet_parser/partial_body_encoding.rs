use std::io::Write;

use anyhow::ensure;

use sequoia_openpgp as openpgp;
use openpgp::{
    cert::Cert,
    packet::{
        header::*,
        key,
        Key,
        Tag,
    },
    parse::Parse,
    policy::StandardPolicy,
    serialize::{
        Marshal,
        stream::Encryptor2 as Encryptor,
        stream::*,
    },
};

use crate::{
    Data,
    Result,
    data,
    sop::{Sop, SopResult, Verification},
    tests::{
        Expectation,
        TestMatrix,
        ConsumptionTest,
        CheckError,
    },
};

/// States the expected result of a test.
type StaticExpectation = std::result::Result<&'static str, &'static str>;

/// Tests whether Partial Body Encoding is supported and how corner
/// cases are handled.
pub struct PartialBodyEncoding {
    pattern: Vec<u8>,
}

impl PartialBodyEncoding {
    const LITERAL_HEADER: &'static [u8] = &[
        b'b', // data format
        0, // filename length
        0, 0, 0, 0, // timestamp
    ];

    pub fn new() -> Result<PartialBodyEncoding> {
        let mut pattern = vec![0; 4096];
        pattern[..Self::LITERAL_HEADER.len()]
            .copy_from_slice(Self::LITERAL_HEADER);
        pattern.iter_mut()
            .skip(Self::LITERAL_HEADER.len())
            .enumerate()
            .for_each(|(i, p)| *p = (i & 0x7f).try_into().unwrap());

        Ok(PartialBodyEncoding {
            pattern,
        })
    }

    const TESTS: &'static [(usize,
                            &'static [BodyLength],
                            Option<StaticExpectation>)] = &[
        (1024, &[BodyLength::Full(1024)],
         Some(Ok("Base case: no chunking"))),
        (1024, &[BodyLength::Indeterminate],
         Some(Ok("Base case: indeterminate length"))),
        (1024, &[BodyLength::Partial(512), BodyLength::Full(512)],
         Some(Ok("Base case: p512, f512"))),
        (512, &[BodyLength::Partial(512), BodyLength::Full(0)],
         Some(Ok("Interoperability concern."))),
        (513, &[BodyLength::Partial(512), BodyLength::Full(1)],
         Some(Ok("Interoperability concern."))),
        (513, &[BodyLength::Partial(512), BodyLength::Partial(1), BodyLength::Full(0)],
         Some(Ok("Interoperability concern."))),
        (514, &[BodyLength::Partial(512), BodyLength::Partial(2), BodyLength::Full(0)],
         Some(Ok("Interoperability concern."))),
        (512, &[BodyLength::Partial(256), BodyLength::Full(256)],
         Some(Err("Short first chunk."))),
        (512, &[BodyLength::Partial(1), BodyLength::Full(511)],
         Some(Err("Very short first chunk."))),
        (512, &[BodyLength::Partial(512)],
         Some(Err("No terminating chunk."))),
        (512, &[BodyLength::Partial(512), BodyLength::Full(0), BodyLength::Full(0)],
         Some(Err("Two terminating chunks."))),
        (513, &[BodyLength::Partial(512), BodyLength::Full(1), BodyLength::Full(0)],
         Some(Err("Two terminating chunks."))),
        (514, &[BodyLength::Partial(512), BodyLength::Full(1), BodyLength::Full(1)],
         Some(Err("Two terminating chunks."))),
        (512, &[BodyLength::Full(512), BodyLength::Full(0)],
         Some(Err("Two fixed size chunks."))),
        (512, &[BodyLength::Full(256), BodyLength::Full(256)],
         Some(Err("Two fixed size chunks."))),
        (512, &[BodyLength::Full(256), BodyLength::Partial(128), BodyLength::Full(128)],
         Some(Err("Two fixed size chunks."))),
        (512, &[BodyLength::Partial(512), BodyLength::Full(1)],
         Some(Err("Short terminating chunk."))),
        (512, &[BodyLength::Partial(512), BodyLength::Full(22)],
         Some(Err("Short terminating chunk."))),
        (511, &[BodyLength::Partial(512), BodyLength::Full(0)],
         Some(Err("Short first chunk."))),
        (512 - 22, &[BodyLength::Partial(512), BodyLength::Full(0)],
         Some(Err("Very short first chunk."))),
        (512 - 22, &[BodyLength::Partial(4096), BodyLength::Full(0)],
         Some(Err("Very short big first chunk."))),
        (512 - 22, &[BodyLength::Partial(1_073_741_824), BodyLength::Full(0)],
         Some(Err("Very short huge first chunk."))),
        (513, &[BodyLength::Partial(512), BodyLength::Full(0)],
         Some(Err("Data after terminating chunk."))),
    ];

    fn make(&self, recipient: &Key<key::PublicParts, key::UnspecifiedRole>,
            length: usize, chunks: &[BodyLength],
            expectation: Option<StaticExpectation>)
            -> Result<(String, (Data, usize), Option<Expectation>)>
    {
        // The length always includes the header fields.
        assert!(length >= Self::LITERAL_HEADER.len());
        assert!(length <= self.pattern.len());
        let mut pattern = &self.pattern[..length];

        assert!(! chunks.is_empty());

        let mut sink = Vec::new();
        let message = Message::new(&mut sink);
        let message = Armorer::new(message)
            .kind(openpgp::armor::Kind::Message)
            .build()?;
        let mut message = Encryptor::for_recipients(message, Some(recipient))
            .build()?;

        // First, write the header.
        if let BodyLength::Indeterminate = chunks[0] {
            // Old CTB, no chunking.
            assert_eq!(chunks.len(), 1);

            CTBOld::new(Tag::Literal, BodyLength::Indeterminate)?
                .serialize(&mut message)?;
            message.write_all(pattern)?;
        } else {
            CTBNew::new(Tag::Literal).serialize(&mut message)?;

            // How much data is still left to write.

            // Then, write the chunks.
            for chunk in chunks.iter() {
                // Write partial body length.
                chunk.serialize(&mut message)?;

                match chunk {
                    BodyLength::Full(l) | BodyLength::Partial(l)=> {
                        let l = (*l).min(pattern.len() as u32) as usize;
                        message.write_all(&pattern[..l])?;
                        pattern = &pattern[l..];
                    },
                    BodyLength::Indeterminate =>
                        panic!("Must not use indeterminate length \
                                with new CTB"),
                }
            }

            // Then, write whatever is left.
            message.write_all(pattern)?;
        }

        message.finalize()?;

        Ok((format!("{} bytes, {}", length, {
                let v: Vec<_> = chunks.iter().map(|c| match c {
                    BodyLength::Full(l) => format!("f{}", l),
                    BodyLength::Partial(l) => format!("p{}", l),
                    BodyLength::Indeterminate => "i".into(),
                }).collect();
                v.join(" ")
            }),
            (sink.into(), length),
            expectation.map(|r| r.map(Into::into).map_err(Into::into))))
    }
}

impl crate::plan::Runnable<TestMatrix> for PartialBodyEncoding {
    fn title(&self) -> String {
        "Tests support for partial body encoding".into()
    }

    fn description(&self) -> String {
        "<p>Tests support for partial body encoding by constructing a
         message encrypted for Bob that uses different encodings for
         the literal data packet.  The message consists of bytes
         encoding their position in the message modulo 128.</p>

         <p>The spec mandates that if partial body encoding is used,
         the first chunk must be at least 512 bytes long, and the
         packet must be terminated by a fixed (or full) chunk (which
         may be of size zero).</p>

         <p>The test vector's label indicate the length of the data
         written and the chunking, where 'fX' indicates a fixed (or
         full) length of X bytes, 'pX' indicates a partial chunk of X
         bytes, and 'i' indicates a chunk of indeterminate length.</p>

         <p>In some vectors there is less or more data written to the
         packet than indicated by the chunks.  Of particular interest
         is whether we can use that to peek into the memory after the
         literal data packet.  A relatively safe peek ahead is 22
         bytes, the size of the MDC packet.</p>".into()
    }

    fn artifacts(&self) -> Vec<(String, Data)> {
        vec![("Key".into(), data::certificate("bob-secret.pgp").into())]
    }

    fn run(&self, matrix: TestMatrix, implementations: &[crate::Sop])
           -> Result<TestMatrix> {
        ConsumptionTest::run(self, matrix, implementations)
    }
}

impl ConsumptionTest<(Data, usize), (Data, Vec<Verification>)> for PartialBodyEncoding {
    fn produce(&self) -> Result<Vec<(String, (Data, usize), Option<Expectation>)>> {
        let p = &StandardPolicy::new();
        let cert = Cert::from_bytes(data::certificate("bob.pgp"))?;
        let recipient = cert.keys()
            .with_policy(p, None)
            .alive()
            .revoked(false)
            .for_transport_encryption()
            .next().unwrap().key();

        Self::TESTS.iter()
            .map(|(length, chunks, expectation)|
                 self.make(&recipient, *length, *chunks, *expectation))
            .collect()
    }

    fn consume(&self, pgp: &Sop, (data, _): &(Data, usize))
               -> SopResult<(Data, Vec<Verification>)> {
        pgp.sop()
            .decrypt()
            .key(data::certificate("bob-secret.pgp"))
            .ciphertext(data)
    }

    fn check_consumer(&self, (_, length): &(Data, usize),
                      (result, _sigs): &(Data, Vec<Verification>),
                      expectation: &Option<Expectation>) -> Result<()> {
        if expectation.as_ref().map(|r| r.is_err()).unwrap_or(true) {
            // Either we have no expectation, or we expect an error.
            // We cannot really test anything here then.
            return Ok(());
        }

        let net_length = length - Self::LITERAL_HEADER.len();
        ensure!(result.len() == net_length,
            CheckError::HardFailure(format!(
                "Expected net length of {} bytes, got {}",
                net_length, result.len())));

        // XX test content

        Ok(())
    }
}
