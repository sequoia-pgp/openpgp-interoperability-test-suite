use crate::{
    Data,
    Result,
    data,
    sop::{Sop, SopResult, Verification},
    tests::{
        ConsumptionTest,
        Expectation,
        TestMatrix,
    },
};

/// Explores whether SHA-1 signatures over colliding files are
/// considered valid.
pub struct Shattered {
}

impl Shattered {
    pub fn new() -> Result<Shattered> {
        Ok(Shattered {
        })
    }
}

impl crate::plan::Runnable<TestMatrix> for Shattered {
    fn title(&self) -> String {
        "Signature over the shattered collision".into()
    }

    fn description(&self) -> String {
        "<p>This tests whether detached signatures using SHA-1 over \
         the collision from the paper <i>The first collision for full \
         SHA-1</i> are considered valid.</p>\
         \
         <p>The first test establishes a baseline.  It is a SHA-1 signature \
         over the text <code>Hello World :)</code></p>" .into() }

    fn artifacts(&self) -> Vec<(String, Data)> {
        vec![("Certificate".into(), data::certificate("bob.pgp").into())]
    }

    fn tags(&self) -> std::collections::BTreeSet<&'static str> {
        ["verify-only"].iter().cloned().collect()
    }

    fn run(&self, matrix: TestMatrix, implementations: &[crate::Sop])
           -> Result<TestMatrix> {
        ConsumptionTest::run(self, matrix, implementations)
    }
}

impl ConsumptionTest<(Data, Data), Vec<Verification>> for Shattered {
    fn produce(&self)
               -> Result<Vec<(String, (Data, Data), Option<Expectation>)>>
    {
        Ok(vec![
            ("Baseline".into(),
             (data::message("shattered-baseline.asc").into(),
              crate::tests::MESSAGE.into()),
             Some(Err(
                 "Data signatures using SHA-1 should be considered invalid"
                     .into()))),
            ("SIG-1 over PDF-1".into(),
             (data::message("shattered-1.pdf.asc").into(),
              data::message("shattered-1.pdf").into()),
             Some(Err("Attack must be mitigated".into()))),
            ("SIG-1 over PDF-2".into(),
             (data::message("shattered-1.pdf.asc").into(),
              data::message("shattered-2.pdf").into()),
             Some(Err("Attack must be mitigated".into()))),
            ("SIG-2 over PDF-1".into(),
             (data::message("shattered-2.pdf.asc").into(),
              data::message("shattered-1.pdf").into()),
             Some(Err("Attack must be mitigated".into()))),
            ("SIG-2 over PDF-2".into(),
             (data::message("shattered-2.pdf.asc").into(),
              data::message("shattered-2.pdf").into()),
             Some(Err("Attack must be mitigated".into()))),
        ])

    }

    fn consume(&self, pgp: &Sop, (sig, data): &(Data, Data))
               -> SopResult<Vec<Verification>> {
        pgp.sop()
            .verify()
            .cert(data::certificate("bob.pgp"))
            .signatures(sig)
            .data(data)
    }
}
