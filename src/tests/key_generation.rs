use std::collections::HashSet;

use anyhow::ensure;

use sequoia_openpgp as openpgp;
use openpgp::parse::Parse;

use crate::{
    Data,
    Result,
    sop::{Sop, SopWithProfile, SopResult, Verification},
    tests::TestPlan,
    tests::{
        TestMatrix,
        RoundtripTest,
        CheckError,
        Expectation,
    },
};

pub struct GenerateThenEncryptDecryptRoundtrip {
    title: String,
    userids: HashSet<String>,
}

impl GenerateThenEncryptDecryptRoundtrip {
    pub fn new(title: &str, userids: &[&str])
               -> GenerateThenEncryptDecryptRoundtrip {
        GenerateThenEncryptDecryptRoundtrip {
            title: title.into(),
            userids: userids.iter().map(|u| u.to_string()).collect(),
        }
    }
}

impl crate::plan::Runnable<TestMatrix> for GenerateThenEncryptDecryptRoundtrip {
    fn title(&self) -> String {
        self.title.clone()
    }

    fn description(&self) -> String {
        "This models key generation, distribution, and encrypted
        message exchange.  Generates a default key with the producer
        <i>P</i>, then extracts the certificate from the key and uses
        it to encrypt a message using the consumer <i>C</i>, and
        finally <i>P</i> to decrypt the message.".into()
    }

    fn tags(&self) -> std::collections::BTreeSet<&'static str> {
        ["v6", "pqc"].iter().cloned().collect()
    }

    fn run(&self, matrix: TestMatrix, implementations: &[crate::Sop]) -> Result<TestMatrix> {
        RoundtripTest::run(self, matrix, implementations)
    }
}

impl RoundtripTest<(Data, Data), (Data, Vec<Verification>)>
    for GenerateThenEncryptDecryptRoundtrip
{
    fn producer_profiles(&self, sop: &Sop) -> Vec<String> {
        sop.list_profiles("generate-key")
    }

    fn produce(&self, pgp: &SopWithProfile) -> SopResult<(Data, Data)> {
        let userids = self.userids.iter().map(|s| &s[..]).collect::<Vec<_>>();

        let extract_cert = |key: Data| -> SopResult<(Data, Data)> {
            pgp.sop()
                .extract_cert()
                .key(&key)
                .map(|cert| (cert, key))
        };

        let key = pgp.sop()
            .generate_key()
            .userids(userids.iter().cloned());

        if pgp.profile().starts_with("draft-ietf-openpgp-persistent-symmetric-keys") {
            key.map(|key| (key.clone(), key))
        } else {
            key.and_then(extract_cert)
        }
    }

    fn check_producer(&self, (artifact, _): &(Data, Data)) -> Result<()> {
        let tpk = match openpgp::Cert::from_bytes(&artifact) {
            Ok(c) => c,
            Err(e) => if let Some(openpgp::Error::UnsupportedCert2(_, _))
                = e.downcast_ref::<openpgp::Error>() {
                    // We don't understand the certificate, skip
                    // sanity checks.
                    return Ok(());
                } else {
                    return Err(e);
                },
        };
        let userids: HashSet<String> = tpk.userids()
            .map(|uidb| {
                String::from_utf8_lossy(uidb.userid().value()).to_string()
            })
            .collect();

        let missing: Vec<&str> = self.userids.difference(&userids)
            .map(|s| &s[..]).collect();
        ensure!(missing.is_empty(),
            CheckError::HardFailure(format!("Missing userids: {:?}",
                                            missing)));

        let additional: Vec<&str> = userids.difference(&self.userids)
            .map(|s| &s[..]).collect();
        ensure!(additional.is_empty(),
            CheckError::HardFailure(format!("Additional userids: {:?}",
                                            additional)));

        Ok(())
    }

    fn consume(&self,
               producer: &Sop,
               consumer: &Sop,
               (cert, key): &(Data, Data))
               -> SopResult<(Data, Vec<Verification>)> {
        consumer.sop()
            .encrypt()
            .cert(cert)
            .plaintext(crate::tests::MESSAGE)
            .and_then(|ciphertext| {
                producer.sop()
                    .decrypt()
                    .key(key)
                    .ciphertext(&ciphertext)
            })
    }

    fn expectation(&self, profile: &String) -> Option<Expectation> {
        if self.userids.len() != 0 {
            return Some(Ok("Interoperability concern.".into()))
        }
        if profile.contains("rfc9580") || profile.contains("crypto-refresh") {
            return Some(Ok("Interoperability concern.".into()))
        }
        None
    }
}

pub fn schedule(plan: &mut TestPlan) -> Result<()> {
    plan.add_section("Key Generation");

    plan.add(Box::new(
        GenerateThenEncryptDecryptRoundtrip::new(
            "Default key generation, encrypt-decrypt roundtrip",
            &["Bernadette <b@example.org>"])));
    plan.add(Box::new(
        GenerateThenEncryptDecryptRoundtrip::new(
            "Default key generation, encrypt-decrypt roundtrip, 2 UIDs",
            &["Bernadette <b@example.org>", "Soo <s@example.org>"])));
    plan.add(Box::new(
        GenerateThenEncryptDecryptRoundtrip::new(
            "Default key generation, encrypt-decrypt roundtrip, no UIDs",
            &[])));
    Ok(())
}
