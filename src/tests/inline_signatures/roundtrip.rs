use std::collections::BTreeSet;
use std::io::Read;

use anyhow::{
    ensure,
    bail,
};

use sequoia_openpgp as openpgp;
use openpgp::{
    cert::prelude::*,
    crypto::*,
    packet::prelude::*,
    parse::{PacketParser, Parse, stream::*},
    types::*,
};

use crate::{
    Data,
    Result,
    sop::{Sop, SopWithProfile, SopResult, SignAs, Verification},
    tests::{
        Expectation,
        TestMatrix,
        P,
        RoundtripTest,
        CheckError,
    },
};

/// Roundtrip tests check whether consume(produce(x)) yields x.
pub struct InlineSignVerifyRoundtrip {
    title: String,
    description: String,
    message: &'static [u8],
    signer_keys: Vec<Vec<u8>>,
    signer_certs: Vec<Vec<u8>>,
    cleartext: bool,
    recipient_key: Option<Vec<u8>>,
    recipient_cert: Option<Vec<u8>>,
    expectation: Option<Expectation>,
}

impl InlineSignVerifyRoundtrip {
    pub fn new<'r, R>(title: &str, description: &str,
                      signer_key: &[u8],
                      signer_cert: &[u8],
                      recipient_key: R,
                      recipient_cert: R,
                      expectation: Option<Expectation>)
                      -> Result<InlineSignVerifyRoundtrip>
    where
        R: Into<Option<&'r [u8]>>,
    {
        Ok(InlineSignVerifyRoundtrip {
            title: title.into(),
            description: description.into(),
            message: crate::tests::MESSAGE,
            signer_certs: vec![signer_cert.into()],
            signer_keys: vec![signer_key.into()],
            cleartext: false,
            recipient_key: recipient_key.into().map(|c| c.into()),
            recipient_cert: recipient_cert.into().map(|c| c.into()),
            expectation,
        })
    }

    pub fn cleartext(title: &str, description: &str,
                     signer_key: &[u8],
                     signer_cert: &[u8],
                     expectation: Option<Expectation>)
                     -> Result<InlineSignVerifyRoundtrip>
    {
        Ok(InlineSignVerifyRoundtrip {
            title: title.into(),
            description: description.into(),
            message: crate::tests::MESSAGE,
            signer_certs: vec![signer_cert.into()],
            signer_keys: vec![signer_key.into()],
            cleartext: true,
            recipient_key: None,
            recipient_cert: None,
            expectation,
        })
    }

    pub fn add_signer(mut self, signer_key: &[u8]) -> Self {
        self.signer_keys.push(signer_key.into());
        self
    }

    pub fn add_verifier(mut self, verifier_cert: &[u8]) -> Self {
        self.signer_certs.push(verifier_cert.into());
        self
    }

    pub fn with_message(mut self, message: &'static [u8]) -> Self {
        self.message = message;
        self
    }
}

impl crate::plan::Runnable<TestMatrix> for InlineSignVerifyRoundtrip {
    fn title(&self) -> String {
        self.title.clone()
    }

    fn description(&self) -> String {
        self.description.clone()
    }

    fn artifacts(&self) -> Vec<(String, Data)> {
        let mut artifacts: Vec<(String, Data)> =
            self.signer_keys.iter().map(|key| {
                ("Signer Key".into(), key.clone().into())
            }).chain(self.signer_certs.iter().map(|cert| {
                ("Signer Cert".into(), cert.clone().into())
            })).collect();

        if let Some(a) = &self.recipient_key {
            artifacts.push(("Recipient Key".into(), a.clone().into()));
        }
        if let Some(a) = &self.recipient_cert {
            artifacts.push(("Recipient Cert".into(), a.clone().into()));
        }
        artifacts
    }

    fn run(&self, matrix: TestMatrix, implementations: &[crate::Sop]) -> Result<TestMatrix> {
        RoundtripTest::run(self, matrix, implementations)
    }
}

impl RoundtripTest<Data, (Data, Vec<Verification>)> for InlineSignVerifyRoundtrip {
    fn produce(&self, pgp: &SopWithProfile)
               -> SopResult<Data> {
        assert_eq!(self.recipient_cert.is_none(), self.recipient_key.is_none());
        assert!(!self.cleartext || self.recipient_cert.is_none());

        if let Some(recipient) = &self.recipient_cert {
            let mut encrypt = pgp.sop()
                .encrypt();
            for key in &self.signer_keys {
                encrypt = encrypt.signer_key(&key);
            }
            encrypt
                .cert(recipient)
                .plaintext(self.message)
        } else {
            let mut inline_sign = pgp.sop()
                .inline_sign()
                .as_(if self.cleartext {
                    SignAs::Clearsigned
                } else {
                    SignAs::Binary
                });
            for key in &self.signer_keys {
                inline_sign = inline_sign.key(&key);
            }
            inline_sign
                .data(self.message)
        }
    }

    fn check_producer(&self, artifact: &Data) -> Result<()> {
        /// Given a verification result, produce the signature type.
        fn typ(r: &VerificationResult) -> SignatureType {
            use VerificationError::*;
            match r {
                Ok(v) => v.sig.typ(),
                Err(MalformedSignature { sig, .. }) => sig.typ(),
                Err(MissingKey { sig, .. }) => sig.typ(),
                Err(UnboundKey { sig, .. }) => sig.typ(),
                Err(BadKey { sig, .. }) => sig.typ(),
                Err(BadSignature { sig, .. }) => sig.typ(),
            }
        }

        struct Helper<'a> {
            key: Option<Cert>,
            cleartext: bool,
            signer_keys_count: usize,
            unsupported_signature: &'a mut bool,
            unsupported_encryption: &'a mut bool,
        }
        impl VerificationHelper for Helper<'_> {
            fn inspect(&mut self, pp: &PacketParser<'_>) -> Result<()> {
                if let Packet::Unknown(u) = &pp.packet {
                    *self.unsupported_signature |=
                        u.tag() == Tag::OnePassSig
                        || u.tag() == Tag::Signature;
                    *self.unsupported_encryption |=
                        u.tag() == Tag::PKESK
                        || u.tag() == Tag::SKESK
                        || u.tag() == Tag::SEIP
                        || u.tag() == Tag::AED;
                }
                Ok(())
            }

            fn get_certs(&mut self, _ids: &[openpgp::KeyHandle])
                         -> Result<Vec<Cert>>
            {
                Ok(Vec::new())
            }
            fn check(&mut self, structure: MessageStructure)
                     -> Result<()>
            {
                let mut saw_signatures = 0;
                for (i, layer) in structure.into_iter().enumerate() {
                    match layer {
                        MessageLayer::Encryption {
                            ..
                        } if i == 0 && self.key.is_some() => (),
                        MessageLayer::Compression {
                            ..
                        } if (i == 1 && self.key.is_some())
                            || (i == 0 && ! self.key.is_some()) => (),
                        MessageLayer::SignatureGroup { ref results } => {
                            saw_signatures += results.len();
                            if self.cleartext {
                                ensure!(results.iter().all(|r| typ(r) == SignatureType::Text),
                                    CheckError::HardFailure("Cleartext signature framework must use \
                                                             text signatures".into()));
                            }
                            ()
                        },
                        _ => bail!(CheckError::HardFailure(
                            "Unexpected message structure".into())),
                    }
                }

                ensure!(saw_signatures == self.signer_keys_count,
                        CheckError::HardFailure(format!(
                            "Saw {} signatures, expected {}",
                            saw_signatures, self.signer_keys_count)));

                Ok(())
            }
        }
        impl DecryptionHelper for Helper<'_> {
            fn decrypt<D>(&mut self, pkesks: &[PKESK], _skesks: &[SKESK],
                          _sym_algo: Option<SymmetricAlgorithm>,
                          mut decrypt: D) -> Result<Option<openpgp::Fingerprint>>
            where D: FnMut(Option<SymmetricAlgorithm>, &SessionKey) -> bool
            {
                if let Some(key) = &self.key {
                    let mut keypair = key.with_policy(P, None)?
                        .keys().for_transport_encryption()
                        .next().ok_or_else(
                            || anyhow::anyhow!("no encryption key"))?
                        .key().clone().parts_into_secret()?.into_keypair()?;

                    pkesks[0].decrypt(&mut keypair, None)
                        .map(|(algo, session_key)| decrypt(algo, &session_key));
                }
                Ok(None)
            }
        }

        let mut unsupported_signature = false;
        let mut unsupported_encryption = false;
        let h = Helper {
            key: self.recipient_key.as_ref()
                .and_then(|k| Cert::from_bytes(k)
                          .map_err(|e| { unsupported_encryption = true; e })
                          .ok()),
                  cleartext: self.cleartext,
            signer_keys_count: self.signer_keys.len(),
            unsupported_signature: &mut unsupported_signature,
            unsupported_encryption: &mut unsupported_encryption,
        };

        let mut content = Vec::new();
        if self.recipient_key.is_some() {
            match (|| {
                let mut v = DecryptorBuilder::from_bytes(&artifact[..])?
                    .with_policy(P, None, h)?;
                v.read_to_end(&mut content)?;
                Ok(())
            })() {
                Ok(()) => (),
                Err(_) if unsupported_signature || unsupported_encryption => (),
                Err(e) => return Err(e),
            }
        } else {
            match (|| {
                let mut v = VerifierBuilder::from_bytes(&artifact[..])?
                    .with_policy(P, None, h)?;
                v.read_to_end(&mut content)?;
                Ok(())
            })() {
                Ok(()) => (),
                Err(_) if unsupported_signature => (),
                Err(e) => return Err(e),
            }
        }

        if self.cleartext {
            ensure!(unsupported_signature
                    || crate::tests::csf_eq(&content[..], self.message),
                CheckError::HardFailure(format!(
                    "Bad recovered CSF message, expected {:?}, got {:?}",
                    std::str::from_utf8(self.message),
                    std::str::from_utf8(&content))));
        } else {
            ensure!(unsupported_signature
                    || unsupported_encryption
                    || &content[..] == self.message,
                CheckError::HardFailure(format!(
                    "Bad message, expected {:?}, got {:?}",
                    std::str::from_utf8(self.message),
                    std::str::from_utf8(&content))));
        }

        Ok(())
    }

    fn consume(&self,
               _producer: &Sop,
               consumer: &Sop,
               artifact: &Data)
               -> SopResult<(Data, Vec<Verification>)> {
        let sop = consumer.sop();
        if let Some(recipient) = &self.recipient_key {
            let mut decrypt = sop
                .decrypt()
                .key(recipient);

            for cert in &self.signer_certs {
                decrypt = decrypt.verify_cert(cert);
            }

            decrypt.ciphertext(artifact)
        } else {
            let mut verify = sop
                .inline_verify();
            for cert in &self.signer_certs {
                verify = verify.cert(cert);
            }

            verify.message(artifact)
        }
    }

    fn check_consumer(&self, _artifact: &Data,
                      (result, verifications): &(Data, Vec<Verification>))
                      -> Result<()>
    {
        match verifications.len() {
            0 =>
                // This is not a hard failure, as the test expectations
                // should indicate whether a valid signature is expected.
                bail!(CheckError::SoftFailure(
                    "Expected a verification line, but got none".into())),
            _ => {
                let expect = self.signer_certs.iter()
                    .map(|c| Cert::from_bytes(c).map(|c| c.fingerprint()))
                    .filter_map(|f| f.ok())
                    .collect::<BTreeSet<_>>();
                let got = verifications.iter().map(|v| v.cert.clone())
                    .collect::<BTreeSet<_>>();

                if expect.len() == self.signer_certs.len() {
                    // We computed all the fingerprints.
                    ensure!(expect == got,
                            CheckError::HardFailure(format!(
                                "Bad verifications, expected {:?}, got {:?}",
                                expect, got)));
                } else {
                    // We failed to compute some fingerprints.
                    ensure!(self.signer_certs.len() == got.len(),
                            CheckError::HardFailure(format!(
                                "Bad verifications, expected {} fingerprints, got {}",
                                self.signer_certs.len(), got.len())));
                }

                // XXX test more?
            },
        }

        if self.cleartext {
            ensure!(crate::tests::csf_eq(result, self.message),
                CheckError::HardFailure(format!(
                    "Bad recovered CSF message, expected {:?}, got {:?}",
                    std::str::from_utf8(self.message),
                    std::str::from_utf8(result))));
        } else {
            ensure!(&result[..] == self.message,
                CheckError::HardFailure(format!(
                    "Bad message, expected {:?}, got {:?}",
                    std::str::from_utf8(self.message),
                    std::str::from_utf8(result))));
        }

        Ok(())
    }

    fn expectation(&self, _: &String) -> Option<Expectation> {
        self.expectation.clone()
    }
}
