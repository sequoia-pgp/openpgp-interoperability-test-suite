use std::collections::HashMap;

use anyhow::Context;

use crate::{
    Result,
};

/// Maximum size of artifacts included in the results.
pub const MAXIMUM_ARTIFACT_SIZE: usize = 50_000;

/// Known drafts that are expected to be implemented, for example
/// because they have since been published as an RFC.
const KNOWN_DRAFTS: [&str; 2] = [
    "draft-ietf-openpgp-crypto-refresh",
    "draft-koch-eddsa"
];

pub fn is_unknown_draft(profile: &str) -> bool {
    profile.starts_with("draft-") &&
        !KNOWN_DRAFTS.iter().any(|known_draft| profile.starts_with(known_draft))
}

pub fn is_prerelease(summary: &str) -> bool {
    summary.contains("-") ||
        (summary.contains("+") && !summary.ends_with("+"))
}

/// Test suite configuration.
#[derive(Clone, Debug, serde::Serialize, serde::Deserialize)]
pub struct Config {
    drivers: Vec<Driver>,
    #[serde(default)]
    rlimits: std::collections::HashMap<String, u64>,
}

/// A driver configuration.
#[derive(Clone, Debug, serde::Serialize, serde::Deserialize)]
struct Driver {
    #[serde(default)]
    id: Option<String>,
    path: String,
    #[serde(default)]
    env: std::collections::HashMap<String, String>,
}

impl Config {
    pub fn set_rlimits(&self) -> Result<()> {
        for (key, &value) in self.rlimits.iter() {
            match key.as_str() {
                "DATA" => rlimit::Resource::DATA.set(value, value)?,
                _ => return
                    Err(anyhow::anyhow!("Unknown limit {:?}", key)),
            }
        }
        Ok(())
    }

    pub fn implementations(&self, env_override: HashMap<String, String>)
                           -> Result<Vec<crate::Sop>>
    {
        let mut r: Vec<crate::Sop> = Vec::new();
        for d in self.drivers.iter() {
            let mut env = d.env.clone();
            for (k, v) in env_override.iter() {
                env.insert(k.into(), v.into());
            }

            let sop =
                crate::sop::Sop::with_env_and_id(&d.path, env, d.id.clone())
                .with_context(|| format!(
                    "Creating sop backend: {} (id: {:?})",
                    d.path, d.id))?;

            r.push(sop);
        }
        Ok(r)
    }
}
