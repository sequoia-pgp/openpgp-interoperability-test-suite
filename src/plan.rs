use std::{
    collections::BTreeSet,
};
use rayon::prelude::*;

use crate::{
    Config,
    tests::TestMatrix,
    Result,
    progress_bar::ProgressBarHandle,
};

/// A run-able test or benchmark.
pub trait Runnable<T: AResult + 'static>: Sync {
    fn title(&self) -> String;
    fn description(&self) -> String;
    fn artifacts(&self) -> Vec<(String, crate::Data)> {
        Vec::with_capacity(0)
    }

    /// Returns the set of tags associated with this test or
    /// benchmark.
    fn tags(&self) -> BTreeSet<&'static str> {
        BTreeSet::new()
    }

    /// Adds the given tags to this test.
    fn with_tags(self, additional_tags: &[&'static str]) -> Box<dyn Runnable<T>>
    where
        Self: Sized,
        Self: 'static,
    {
        Box::new(WithTags {
            additional_tags: additional_tags.iter().cloned().collect(),
            test: Box::new(self),
        })
    }

    fn prepare(&self, implementations: &[crate::Sop]) -> TestMatrix {
        TestMatrix::new(
            self.title(),
            self.tags(),
            self.description(),
            self.artifacts(),
            implementations.iter().map(|i| i.version().unwrap()).collect(),
        )
    }
    fn run(&self, matrix: TestMatrix, implementations: &[crate::Sop]) -> Result<T>;
}

/// A result of a single run-able test or benchmark.
pub trait AResult: Sync {
    /// Changes the title.
    fn with_title(self, title: String) -> Self;

    /// Changes the description.
    fn with_description(self, description: String) -> Self;

    /// Changes the artifacts.
    fn with_artifacts(self, artifacts: Vec<(String, crate::Data)>) -> Self;

    /// Changes the tags.
    fn with_tags(self, tags: BTreeSet<&'static str>) -> Self;
}

/// A collection of scheduled tests.
///
/// We first collect all the tests we want to run in a plan, then
/// execute them in parallel, producing a structure describing the
/// results of the tests.
pub struct Plan<'a, T: AResult + Sync + 'static> {
    toc: Vec<(String, Vec<Box<dyn Runnable<T>>>)>,
    configuration: &'a Config,
}

impl<'a, T: AResult + Sync + Send> Plan<'a, T>
{
    pub fn new(configuration: &'a Config) -> Plan<'a, T> {
        Plan {
            toc: Default::default(),
            configuration,
        }
    }

    pub fn add_section(&mut self, title: &str) {
        self.toc.push((title.into(), Vec::new()));
    }

    pub fn add(&mut self, test: Box<dyn Runnable<T>>) {
        if let Some((_, entries)) = self.toc.iter_mut().last() {
            entries.push(test);
        } else {
            panic!("No section added")
        }
    }

    /// Retains only the tests specified by the predicate.
    ///
    /// Prunes empty sections.
    pub fn retain_tests<F>(&mut self, mut f: F)
    where F: FnMut(&dyn Runnable<T>) -> bool,
    {
        for (_section, tests) in &mut self.toc {
            tests.retain(|t| f(t.as_ref()));
        }
        self.toc.retain(|(_section, tests)| ! tests.is_empty());
    }

    pub fn run(&self, implementations: &[crate::Sop])
               -> Result<Results<T>>
    {
        let pb = ProgressBarHandle::new(
            self.toc.iter().map(|(_, tests)| tests.len() as u64).sum::<u64>());

        let results =
            self.toc.par_iter().map(|(section, tests)|
                                      -> Result<(String, Vec<T>)> {
                Ok((section.into(),
                 tests.par_iter().map(
                     |test| {
                         pb.start_test(test.title());
                         let matrix = test.prepare(implementations);
                         let r = test.run(matrix, implementations);
                         pb.end_test();
                         r
                     }).collect::<Result<Vec<T>>>()?))
            }).collect::<Result<Vec<(String, Vec<T>)>>>()?;

        Ok(Results {
            version: env!("VERGEN_SEMVER").to_string(),
            commit: env!("VERGEN_SHA_SHORT").to_string(),
            timestamp: chrono::offset::Utc::now(),
            configuration: self.configuration.clone(),
            results: map_to_section_results(results),
            implementations:
            implementations.iter().map(|i| i.version())
                .collect::<Result<_>>()?,
        })
    }

    pub fn run_sequential(&self, implementations: &[crate::Sop])
               -> Result<Results<T>>
    {
        let pb = ProgressBarHandle::new(
            self.toc.iter().map(|(_, tests)| tests.len() as u64).sum::<u64>());

        let results =
            self.toc.iter().map(|(section, tests)|
                                  -> Result<(String, Vec<T>)> {
                Ok((section.into(),
                 tests.iter().map(
                     |test| {
                         pb.start_test(test.title());
                         let matrix = test.prepare(implementations);
                         let r = test.run(matrix, implementations);
                         pb.end_test();
                         r
                     }).collect::<Result<Vec<T>>>()?))
            }).collect::<Result<Vec<(String, Vec<T>)>>>()?;

        Ok(Results {
            version: env!("VERGEN_SEMVER").to_string(),
            commit: env!("VERGEN_SHA_SHORT").to_string(),
            timestamp: chrono::offset::Utc::now(),
            configuration: self.configuration.clone(),
            results: map_to_section_results(results),
            implementations:
            implementations.iter().map(|i| i.version())
                .collect::<Result<_>>()?,
        })
    }
}

/// Result of executing a Plan<T>.
#[derive(Debug, serde::Deserialize, serde::Serialize)]
pub struct Results<T>
{
    pub version: String,
    pub commit: String,
    pub timestamp: chrono::DateTime<chrono::offset::Utc>,
    pub configuration: Config,
    pub results: Vec<SectionResult<T>>,
    pub implementations: Vec<crate::sop::Version>,
}

#[derive(Debug, serde::Deserialize, serde::Serialize)]
pub struct SectionResult<T> {
    pub section: String,
    pub results: Vec<T>,
}

fn map_to_section_results<T>(tuples: Vec<(String, Vec<T>)>) -> Vec<SectionResult<T>> {
    tuples
        .into_iter()
        .map(|(section, results)| SectionResult{section, results})
        .collect()
}

/// Adds tags to an existing test.
struct WithTags<T> {
    additional_tags: BTreeSet<&'static str>,
    test: Box<dyn Runnable<T>>,
}

impl<T: AResult + 'static> Runnable<T> for WithTags<T> {
    fn title(&self) -> String {
        self.test.title()
    }

    fn description(&self) -> String {
        self.test.description()
    }

    fn artifacts(&self) -> Vec<(String, crate::Data)> {
        self.test.artifacts()
    }

    fn tags(&self) -> BTreeSet<&'static str> {
        self.test.tags().union(&self.additional_tags).cloned().collect()
    }

    fn run(&self, matrix: TestMatrix, implementations: &[crate::Sop]) -> Result<T> {
        self.test.run(matrix.with_tags(self.tags()), implementations)
    }
}
